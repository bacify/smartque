<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('is_logged_in'))
{
    function is_logged_in() {
	    $CI =& get_instance();
	    // We need to use $CI->session instead of $this->session
	    $user = $CI->session->userdata('user_data');
	    if (!isset($user)) { return false; } else { return true; }

	} 

}