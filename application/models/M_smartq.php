<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class M_smartq extends CI_Model {



	public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }


             public function get_setting($condition=null)
        {
            if($condition!=null)
                $this->db->where($condition);
            
                $query = $this->db->get('smart_setting');
                return $query->result();
        }
        public function update_setting($condition,$value)
        {
                $this->db->where($condition);
                                
                $query=$this->db->update('smart_setting',$value);
                return ($this->db->affected_rows() > 0);
        } 
             public function get_loket_alias($condition=null)
        {
            if($condition!=null)
                $this->db->where($condition);

                $query = $this->db->get('s_loket_alias');
                return $query->result();
        } 
         public function update_loket_alias($condition,$value)
        {
            if($condition!=null)
                $this->db->where($condition);

               
            $query=$this->db->update('s_loket_alias',$value);
            return ($this->db->affected_rows() > 0);
               
        }    
        public function get_layanan($condition=null)
        {
            if($condition!=null)
                $this->db->where($condition);

                $query = $this->db->get('s_layanan');
                return $query->result();
        }
        public function get_used_layanan($condition=null)
        {
            if($condition!=null)
                $this->db->where($condition);

                $this->db->where_in('id_layanan','select DISTINCT(layanan_id) from tiket_queue',false);

                $this->db->select('s_layanan.id_layanan,s_layanan.nama_layanan');

                $this->db->order_by('id_layanan','asc');
                $query = $this->db->get('s_layanan');
                return $query->result();
        }
        public function update_layanan($condition,$value)
        {
            if($condition!=null)
                $this->db->where($condition);

               
            $query=$this->db->update('s_layanan',$value);
            return ($this->db->affected_rows() > 0);
               
        }

         public function get_display_counter()
        {
                $query = $this->db->get('s_display_counter');
                return $query->result();
        }

         public function cek_login($value=null)
        {
                if($value!=''){
                       $this->db->or_where($value);                        
                }
                $query = $this->db->get('smart_setting');
                return $query->result();
        }
        public function insert_login_details($value)
        {
                
                $query = $this->db->insert('login_details', $value);
                $insert_id = $this->db->insert_id();
                return $insert_id;
        } 
        
         public function update_login_details($condition,$value=null)
        {
                $this->db->where($condition);
                if($value==null)
                    $this->db->set('last_activity','NOW()',FALSE);
                else
                   $this->db->set($value); 
                $query=$this->db->update('login_details');
                return ($this->db->affected_rows() > 0);
        }
        public function fetch_loket_last_activity($loket=null){
                if($loket!=null)
                 $this->db->where($loket);   

                 $this->db->order_by('last_activity','DESC');   
                 $this->db->limit('1');   

            $query = $this->db->get('login_details');
            return $query->result();
        }
            public function get_tiket_counter($condition=null)
        {
            if($condition!=null)
                $this->db->where($condition);

                $query = $this->db->get('s_tiket_counter');
                return $query->result();
        }    
       
        public function get_loket_status($condition=null)
        {
            if($condition!=null)
                $this->db->where($condition);

                $this->db->select('*, 
                                    TIMESTAMPDIFF(SECOND,updated,CURRENT_TIMESTAMP()) as diff, 
                                    if(counter="-","00:00:00",SEC_TO_TIME(TIMESTAMPDIFF(SECOND,updated,CURRENT_TIMESTAMP()))) as tim ');
                $this->db->order_by('updated','DESC');
                $query = $this->db->get('s_loket_status');
                return $query->result();
        }
         public function get_call_status($condition=null)
        {
            if($condition!=null)
                $this->db->where($condition);

                $this->db->select('s_call_status.loket_id,counter,s_loket_alias.alias_name,counter as counter_alias,s_call_status.updated,s_call_status.status,calling');
                $this->db->join('s_loket_alias','s_loket_alias.loket_id=s_call_status.loket_id');                
                $query = $this->db->get('s_call_status');
                return $query->result();
        } 
        public function update_call_status($condition=null,$value)
        {
            if($condition!=null)
                $this->db->where($condition);

                $query=$this->db->update('s_call_status',$value);
                return ($this->db->affected_rows() > 0);                
        }
        public function get_queue($condition=null)
        {
            if($condition!=null)
                $this->db->where($condition);

                $query = $this->db->get('s_layanan_counter');
                return $query->result();
        }  
        public function get_loket_last_update($condition=null)
        {
            if($condition!=null)
                $this->db->where($condition);

            $this->db->select_max('updated', 'last_update');

                $query = $this->db->get('s_loket_status');
                return $query->result();
        }
         public function get_tiket_last_update($condition=null)
        {
            if($condition!=null)
                $this->db->where($condition);

            $this->db->select_max('updated', 'last_update');

                $query = $this->db->get('s_tiket_counter');
                return $query->result();
        }
         public function get_queue_last_update($condition=null)
        {
            if($condition!=null)
                $this->db->where($condition);

                $this->db->select_max('updated', 'last_update');

                $query = $this->db->get('s_layanan_counter');
                return $query->result();
        }

         public function update_queue($condition,$value)
        {
                
                $this->db->where($condition);                
                $this->db->set($value); 
                $query=$this->db->update('s_layanan_counter');
                return ($this->db->affected_rows() > 0);
        }
        public function update_loket($condition,$value)
        {
                $this->db->where($condition);
                
                $this->db->set($value); 
                $query=$this->db->update('s_loket_status');
                return ($this->db->affected_rows() > 0);
        } 
        
        public function get_saved_queue($condition=null)
        {
               if($condition!=null)
                $this->db->where($condition);

                               

                $query = $this->db->get('s_accepted');
                return $query->result();
        }
         public function save_queue($value)
        {
                
                $query = $this->db->insert('s_accepted', $value);
                $insert_id = $this->db->insert_id();
                return $insert_id;
        }
        public function update_saved_queue($condition)
        {
                $this->db->where($condition);
                
                $this->db->set('finish_date','NOW()',false); 
                $query=$this->db->update('s_accepted');
                return ($this->db->affected_rows() > 0);
        }  
         public function get_passed_queue($condition=null)
        {
               if($condition!=null)
                $this->db->where($condition);

                               

                $query = $this->db->get('s_passed');
                return $query->result();
        } 


        public function save_media($value)
        {
                
                $query = $this->db->insert('s_media', $value);
                $insert_id = $this->db->insert_id();
                return $insert_id;
        }
         public function get_media($condition=null)
        {
               if($condition!=null)
                $this->db->where($condition);

                               

                $query = $this->db->get('s_media');
                return $query->result();
        }
          public function delete_media($condition){

                
                $this->db->where($condition);
                
              
                $query = $this->db->delete('s_media');

               return ($this->db->affected_rows() > 0);
        }


         public function get_current_tiket($condition=null)
        {
               if($condition!=null)
                $this->db->where($condition);

                $this->db->select('layanan_id,nama_layanan,tiket_alias,counter, concat(tiket_alias,counter) as current_tiket');
                $this->db->join('s_layanan','s_layanan.id_layanan=s_layanan_counter.layanan_id');              

                $query = $this->db->get('s_layanan_counter');
                return $query->result();
        } 
         public function update_next_tiket($condition)
        {
                $this->db->where($condition);
                
                $this->db->set('counter','counter+1',false); 
                $query=$this->db->update('s_layanan_counter');
                return ($this->db->affected_rows() > 0);
        }  
        public function insert_tiket_queue($value)
        {
                
                $query = $this->db->insert('tiket_queue', $value);
                $insert_id = $this->db->insert_id();
                return $insert_id;
        } 
        public function get_queue_left($condition=null)
        {
                
                if($condition!=null)
                $this->db->where($condition);

                $this->db->select('count(*) as sisa');
                $this->db->where('queue_status',0);
               $query = $this->db->get('tiket_queue');
                return $query->result();
        }
         public function get_tiket_queue($condition=null)
        {
               if($condition!=null)
                $this->db->where($condition);

                
                $this->db->order_by('idtiket','asc');
                $query = $this->db->get('tiket_queue');
                return $query->result();
        } 
         public function get_tiket_queue_last_update($condition=null)
        {
               if($condition!=null)
                $this->db->where($condition);

                $this->db->select('max(updated) as last_update');                

                $query = $this->db->get('tiket_queue');
                return $query->result();
        } 

         public function get_tiket_queue_total($condition=null)
        {
               if($condition!=null)
                $this->db->where($condition);

                $this->db->select('count(*) as total');

                $query = $this->db->get('tiket_queue');
                return $query->result();
        }  
        public function update_tiket($condition,$value)
        {
                $this->db->where($condition);
                
                $this->db->set($value); 
                $query=$this->db->update('tiket_queue');
                return ($this->db->affected_rows() > 0);
        }
        public function startprocess_tiket($condition)
        {
                $this->db->where($condition);
                $this->db->where('queue_status',0);                
                $this->db->set('queue_end','NOW()',false); 
                $this->db->set('service_start','NOW()',false); 
                $this->db->set('queue_status',1); 
                $query=$this->db->update('tiket_queue');
                return ($this->db->affected_rows() > 0);
        }  
        public function startprocess_passedtiket($condition)
        {
                $this->db->where($condition);
                $this->db->where('queue_status',3);                
                $this->db->set('queue_end','NOW()',false); 
                $this->db->set('service_start','NOW()',false); 
                $this->db->set('queue_status',1); 
                $query=$this->db->update('tiket_queue');
                return ($this->db->affected_rows() > 0);
        }  
        public function endprocess_tiket($condition=null)
        {
                if($condition!=null)
                $this->db->where($condition);
                $this->db->where('queue_status',1);                
                $this->db->set('service_end','NOW()',false); 
                $this->db->set('queue_status',2); 
                $query=$this->db->update('tiket_queue');
                return ($this->db->affected_rows() > 0);
        }
        public function passprocess_tiket($condition)
        {
                $this->db->where($condition);
                $this->db->where('queue_status','0');                                
                $this->db->set('queue_status',3); 
                $query=$this->db->update('tiket_queue');
                return ($this->db->affected_rows() > 0);
        } 
         public function get_laporan_jam($condition=null)
        {
               if($condition!=null)
                $this->db->where($condition);

                $this->db->select('date_format( queue_start, \'%Y-%m-%d %H\' ) as tanggal_pj,
                                    date_format( queue_start, \'%H\' ) as jam_pj,
                                    count(*) as total,
                                    count(if(queue_status=4,1,null)) as tidak_dilayani,
                                    count(if(queue_status=2,1,null)) as dilayani,
                                    SUM(if(queue_status=2,TIMESTAMPDIFF(SECOND,queue_start,queue_end),null)) as total_waiting,
                                    SUM(if(queue_status=2,TIMESTAMPDIFF(SECOND,service_start,service_end),null)) as total_service,
                                    SUM(if(queue_status=2,TIMESTAMPDIFF(SECOND,queue_start,service_end),null)) as total_fullservice,
                                    SEC_TO_TIME(ROUND(AVG(if(queue_status=2,TIMESTAMPDIFF(SECOND,queue_start,queue_end),null)))) as average_waiting,
                                    SEC_TO_TIME(ROUND(AVG(if(queue_status=2,TIMESTAMPDIFF(SECOND,service_start,service_end),null)))) as average_service,
                                    SEC_TO_TIME(ROUND(AVG(if(queue_status=2,TIMESTAMPDIFF(SECOND,queue_start,service_end),null)))) as average_total_service');                

                $this->db->order_by('tanggal_pj','asc');
                $this->db->group_by('tanggal_pj');
                $query = $this->db->get('tiket_queue');
                return $query->result();
        }
         public function get_laporan_hari($condition=null)
        {
               if($condition!=null)
                $this->db->where($condition);

                $this->db->select('date_format( queue_start, \'%Y-%m-%d\' ) as tanggal_pj,                                    
                                    count(*) as total,
                                    count(if(queue_status=4,1,null)) as tidak_dilayani,
                                    count(if(queue_status=2,1,null)) as dilayani,
                                    SUM(if(queue_status=2,TIMESTAMPDIFF(SECOND,queue_start,queue_end),null)) as total_waiting,
                                    SUM(if(queue_status=2,TIMESTAMPDIFF(SECOND,service_start,service_end),null)) as total_service,
                                    SUM(if(queue_status=2,TIMESTAMPDIFF(SECOND,queue_start,service_end),null)) as total_fullservice,
                                    SEC_TO_TIME(ROUND(AVG(if(queue_status=2,TIMESTAMPDIFF(SECOND,queue_start,queue_end),null)))) as average_waiting,
                                    SEC_TO_TIME(ROUND(AVG(if(queue_status=2,TIMESTAMPDIFF(SECOND,service_start,service_end),null)))) as average_service,
                                    SEC_TO_TIME(ROUND(AVG(if(queue_status=2,TIMESTAMPDIFF(SECOND,queue_start,service_end),null)))) as average_total_service');                

                $this->db->order_by('tanggal_pj','asc');
                $this->db->group_by('tanggal_pj');
                $query = $this->db->get('tiket_queue');
                return $query->result();
        }
         public function get_laporan_hari_by_pelayanan($condition=null)
        {
               if($condition!=null)
                $this->db->where($condition);

                $this->db->select('date_format( queue_start, \'%Y-%m-%d\' ) as tanggal_pj,
                                    nama_layanan,                                    
                                    count(*) as total,
                                    count(if(queue_status=4,1,null)) as tidak_dilayani,
                                    count(if(queue_status=2,1,null)) as dilayani,
                                    SUM(if(queue_status=2,TIMESTAMPDIFF(SECOND,queue_start,queue_end),null)) as total_waiting,
                                    SUM(if(queue_status=2,TIMESTAMPDIFF(SECOND,service_start,service_end),null)) as total_service,
                                    SUM(if(queue_status=2,TIMESTAMPDIFF(SECOND,queue_start,service_end),null)) as total_fullservice,
                                    SEC_TO_TIME(ROUND(AVG(if(queue_status=2,TIMESTAMPDIFF(SECOND,queue_start,queue_end),null)))) as average_waiting,
                                    SEC_TO_TIME(ROUND(AVG(if(queue_status=2,TIMESTAMPDIFF(SECOND,service_start,service_end),null)))) as average_service,
                                    SEC_TO_TIME(ROUND(AVG(if(queue_status=2,TIMESTAMPDIFF(SECOND,queue_start,service_end),null)))) as average_total_service');                
                $this->db->join('s_layanan','s_layanan.id_layanan=tiket_queue.layanan_id');
                $this->db->order_by('s_layanan.id_layanan','asc');
                $this->db->group_by('tanggal_pj,tiket_queue.layanan_id');
                $query = $this->db->get('tiket_queue');
                return $query->result();
        }
        public function get_laporan_bulan_by_pelayanan($condition=null)
        {
               if($condition!=null)
                $this->db->where($condition);

                $this->db->select('date_format( queue_start, \'%Y-%m\' ) as tanggal_pj,
                                    nama_layanan,                                    
                                    count(*) as total,
                                    count(if(queue_status=4,1,null)) as tidak_dilayani,
                                    count(if(queue_status=2,1,null)) as dilayani,
                                    SUM(if(queue_status=2,TIMESTAMPDIFF(SECOND,queue_start,queue_end),null)) as total_waiting,
                                    SUM(if(queue_status=2,TIMESTAMPDIFF(SECOND,service_start,service_end),null)) as total_service,
                                    SUM(if(queue_status=2,TIMESTAMPDIFF(SECOND,queue_start,service_end),null)) as total_fullservice,
                                    SEC_TO_TIME(ROUND(AVG(if(queue_status=2,TIMESTAMPDIFF(SECOND,queue_start,queue_end),null)))) as average_waiting,
                                    SEC_TO_TIME(ROUND(AVG(if(queue_status=2,TIMESTAMPDIFF(SECOND,service_start,service_end),null)))) as average_service,
                                    SEC_TO_TIME(ROUND(AVG(if(queue_status=2,TIMESTAMPDIFF(SECOND,queue_start,service_end),null)))) as average_total_service');                
                $this->db->join('s_layanan','s_layanan.id_layanan=tiket_queue.layanan_id');
                $this->db->order_by('s_layanan.id_layanan','asc');
                $this->db->group_by('tanggal_pj,tiket_queue.layanan_id');
                $query = $this->db->get('tiket_queue');
                return $query->result();
        }

         public function get_laporan_bulan($condition=null)
        {
               if($condition!=null)
                $this->db->where($condition);

                $this->db->select('date_format( queue_start, \'%Y-%m\' ) as tanggal_pj,
                                    date_format( queue_start, \'%m\' ) as bulan,                                                                        
                                    count(*) as total,
                                    count(if(queue_status=4,1,null)) as tidak_dilayani,
                                    count(if(queue_status=2,1,null)) as dilayani,
                                    SUM(if(queue_status=2,TIMESTAMPDIFF(SECOND,queue_start,queue_end),null)) as total_waiting,
                                    SUM(if(queue_status=2,TIMESTAMPDIFF(SECOND,service_start,service_end),null)) as total_service,
                                    SUM(if(queue_status=2,TIMESTAMPDIFF(SECOND,queue_start,service_end),null)) as total_fullservice,
                                    SEC_TO_TIME(ROUND(AVG(if(queue_status=2,TIMESTAMPDIFF(SECOND,queue_start,queue_end),null)))) as average_waiting,
                                    SEC_TO_TIME(ROUND(AVG(if(queue_status=2,TIMESTAMPDIFF(SECOND,service_start,service_end),null)))) as average_service,
                                    SEC_TO_TIME(ROUND(AVG(if(queue_status=2,TIMESTAMPDIFF(SECOND,queue_start,service_end),null)))) as average_total_service');                

                $this->db->order_by('tanggal_pj','asc');
                $this->db->group_by('tanggal_pj');
                $query = $this->db->get('tiket_queue');
                return $query->result();
        }
        public function get_stat_total($condition=null)
        {
               if($condition!=null)
                $this->db->where($condition);

                $this->db->select('count(*) as total,
                                   count(IF( YEAR(queue_start) = YEAR(CURDATE()),1,NULL) ) as total_year,
                                   count(if( YEAR(queue_start) = YEAR(CURDATE()) && MONTH(queue_start) = MONTH(CURDATE() ),1,NULL)) as total_month ,
                                   count(if( date(queue_start) = CURDATE(),1,NULL) ) as total_day,
                                   CURDATE() as day' );                

                 
                $query = $this->db->get('tiket_queue');
                return $query->result();
        }

        public function get_stat_total_layanan($condition=null)
        {
               if($condition!=null)
                $this->db->where($condition);

                $this->db->select(' count(IF( layanan_id = 1,1,null )) as layanan_1,
                                    count(IF( layanan_id = 2,1,null )) as layanan_2,
                                    count(IF( layanan_id = 3,1,null ) )as layanan_3,
                                    count(IF( layanan_id = 4,1,null )) as layanan_4,                                      
                                    count(IF( layanan_id = 5,1,null )) as layanan_5,
                                    count(IF( layanan_id = 6,1,null )) as layanan_6,
                                    count(IF( layanan_id = 7,1,null )) as layanan_7,
                                    count(IF( layanan_id = 8,1,null )) as layanan_8,
                                    count(*) as total' );                

                 
                $query = $this->db->get('tiket_queue');
                return $query->result();
        }
        public function get_total_year($condition=null)
        {
               if($condition!=null)
                $this->db->where($condition);

                $this->db->select(' count(*) as total_year,
                                    count(IF( MONTH(queue_start)= 1,1,NULL) ) as januari,
                                    count(IF( MONTH(queue_start)= 2,1,NULL) ) as februari,
                                    count(IF( MONTH(queue_start)= 3,1,NULL) ) as maret,
                                    count(IF( MONTH(queue_start)= 4,1,NULL) ) as april,
                                    count(IF( MONTH(queue_start)= 5,1,NULL) ) as mei,
                                    count(IF( MONTH(queue_start)= 6,1,NULL) ) as juni,
                                    count(IF( MONTH(queue_start)= 7,1,NULL) ) as juli,
                                    count(IF( MONTH(queue_start)= 8,1,NULL) ) as agustus,
                                    count(IF( MONTH(queue_start)= 9,1,NULL) ) as september,
                                    count(IF( MONTH(queue_start)= 10,1,NULL) ) as oktober,
                                    count(IF( MONTH(queue_start)= 11,1,NULL) ) as nopember,
                                    count(IF( MONTH(queue_start)= 12,1,NULL) ) as desember' );                

                 
                $query = $this->db->get('tiket_queue');
                return $query->result();
        }
        public function get_total_month($condition=null)
        {
               if($condition!=null)
                $this->db->where($condition);

                $this->db->select(' count(*) as total, 
                                    DAY(queue_start) as hari,
                                    date(queue_start) as tanggal');                

                $this->db->group_by('tanggal');
                $this->db->order_by('tanggal,hari', 'asc');
                $query = $this->db->get('tiket_queue');
                return $query->result();
        }
        public function get_total_current_year($condition=null)
        {
               if($condition!=null)
                $this->db->where($condition);

                $this->db->select(' count(IF( YEAR(queue_start) = YEAR(CURDATE()),1,NULL) ) as total_year,
                                    count(IF( YEAR(queue_start) = YEAR(CURDATE()) && MONTH(queue_start)= 1,1,NULL) ) as januari,
                                    count(IF( YEAR(queue_start) = YEAR(CURDATE()) && MONTH(queue_start)= 2,1,NULL) ) as februari,
                                    count(IF( YEAR(queue_start) = YEAR(CURDATE()) && MONTH(queue_start)= 3,1,NULL) ) as maret,
                                    count(IF( YEAR(queue_start) = YEAR(CURDATE()) && MONTH(queue_start)= 4,1,NULL) ) as april,
                                    count(IF( YEAR(queue_start) = YEAR(CURDATE()) && MONTH(queue_start)= 5,1,NULL) ) as mei,
                                    count(IF( YEAR(queue_start) = YEAR(CURDATE()) && MONTH(queue_start)= 6,1,NULL) ) as juni,
                                    count(IF( YEAR(queue_start) = YEAR(CURDATE()) && MONTH(queue_start)= 7,1,NULL) ) as juli,
                                    count(IF( YEAR(queue_start) = YEAR(CURDATE()) && MONTH(queue_start)= 8,1,NULL) ) as agustus,
                                    count(IF( YEAR(queue_start) = YEAR(CURDATE()) && MONTH(queue_start)= 9,1,NULL) ) as september,
                                    count(IF( YEAR(queue_start) = YEAR(CURDATE()) && MONTH(queue_start)= 10,1,NULL) ) as oktober,
                                    count(IF( YEAR(queue_start) = YEAR(CURDATE()) && MONTH(queue_start)= 11,1,NULL) ) as nopember,
                                    count(IF( YEAR(queue_start) = YEAR(CURDATE()) && MONTH(queue_start)= 12,1,NULL) ) as desember' );                

                 
                $query = $this->db->get('tiket_queue');
                return $query->result();
        }
        public function get_total_custom_year($condition=null)
        {
               if($condition!=null)
                $this->db->where($condition);

                $this->db->select(' count(*) as total_year,
                                    count(IF( MONTH(queue_start)= 1,1,NULL) ) as januari,
                                    count(IF( MONTH(queue_start)= 2,1,NULL) ) as februari,
                                    count(IF( MONTH(queue_start)= 3,1,NULL) ) as maret,
                                    count(IF( MONTH(queue_start)= 4,1,NULL) ) as april,
                                    count(IF( MONTH(queue_start)= 5,1,NULL) ) as mei,
                                    count(IF( MONTH(queue_start)= 6,1,NULL) ) as juni,
                                    count(IF( MONTH(queue_start)= 7,1,NULL) ) as juli,
                                    count(IF( MONTH(queue_start)= 8,1,NULL) ) as agustus,
                                    count(IF( MONTH(queue_start)= 9,1,NULL) ) as september,
                                    count(IF( MONTH(queue_start)= 10,1,NULL) ) as oktober,
                                    count(IF( MONTH(queue_start)= 11,1,NULL) ) as nopember,
                                    count(IF( MONTH(queue_start)= 12,1,NULL) ) as desember' );                

                 
                $query = $this->db->get('tiket_queue');
                return $query->result();
        }

        public function insert_random_tiket(){
            $this->db->query('SET @MIN = \'2018-01-01 01:00:00\';');
            $this->db->query('SET @MAX = \'2019-12-31 23:59:59\';');
            $this->db->query('SET @STARTDATE=TIMESTAMPADD(SECOND, FLOOR(RAND() * TIMESTAMPDIFF(SECOND, @MIN, @MAX)), @MIN);');
            $this->db->query('SET @DATEINIT=@STARTDATE;');
            $this->db->query('SET @ENDDATE=DATE_ADD(@DATEINIT,INTERVAL (FLOOR(RAND()*(1200-60+1)+60)) SECOND);');
            $this->db->query('SET @ENDSERVICE=DATE_ADD(@ENDDATE,INTERVAL (FLOOR(RAND()*(1200-60+1)+60)) SECOND); ');
            $this->db->query('SET @LAYANAN=FLOOR(RAND()*(9-0+1)+0);');
            $this->db->query('SET @STATUSS=FLOOR(RAND()*(5-1+1)+1);');
            $this->db->query('INSERT INTO tiket_queue (no_tiket,layanan_id,queue_start,queue_end,service_start,service_end,queue_status,updated) 
                                    VALUES (\'Z00\',@LAYANAN, @DATEINIT,@ENDDATE,@ENDDATE,@ENDSERVICE,IF(@STATUSS=3,4,@STATUSS),@ENDSERVICE);');
                                
                                

        }
        public function reset_data(){
             
            /*clean login log*/
            $this->db->empty_table('login_details');
            
            /*reset layanan*/
            $this->db->query('update s_layanan set nama_layanan="layanan1",tiket_alias="A",status_layanan=0 where id_layanan=1; ');
            $this->db->query('update s_layanan set nama_layanan="layanan2",tiket_alias="B",status_layanan=0 where id_layanan=2; ');
            $this->db->query('update s_layanan set nama_layanan="layanan3",tiket_alias="C",status_layanan=0 where id_layanan=3; ');
            $this->db->query('update s_layanan set nama_layanan="layanan4",tiket_alias="D",status_layanan=0 where id_layanan=4; ');
            $this->db->query('update s_layanan set nama_layanan="layanan5",tiket_alias="E",status_layanan=0 where id_layanan=5; ');
            $this->db->query('update s_layanan set nama_layanan="layanan6",tiket_alias="F",status_layanan=0 where id_layanan=6; ');
            $this->db->query('update s_layanan set nama_layanan="layanan7",tiket_alias="G",status_layanan=0 where id_layanan=7; ');
            $this->db->query('update s_layanan set nama_layanan="layanan8",tiket_alias="H",status_layanan=0 where id_layanan=8; ');


            /*update counter*/
            $this->db->query('update s_layanan_counter set counter=0 ;');


            /*update alias*/
            $this->db->query('update s_loket_alias set status=0,layanan_id=1,loket_id=id_alias;');

            /*update loket status*/
            $this->db->query('update s_loket_status set counter="-";');

            $this->db->query('update s_tiket_counter set counter="0";');

            $this->db->query('update smart_setting set value="0" where setting="queue_status";');
            $this->db->query('update smart_setting set value="1" where setting="queue_mode";');
            $this->db->query('update smart_setting set value="WhiteSpace Inc" where setting="nama_instansi";');
            $this->db->query('update smart_setting set value="Sengkaling Residence No 3, Batu, Jawa Timur 65233" where setting="alamat_instansi";');
            $this->db->query('update smart_setting set value="0813-3136-8972" where setting="telp_instansi";');
            $this->db->query('update smart_setting set value="1" where setting="jenis_display";');
            $this->db->query('update smart_setting set value="0" where setting="display_list";');
            $this->db->query('update smart_setting set value="0" where setting="slideshow";');
            $this->db->query('update smart_setting set value="0" where setting="video";');
            $this->db->query('update smart_setting set value="0" where setting="key_activation";');
            $this->db->query('update smart_setting set value="admin@whitespace.com" where setting="email";');
            $this->db->query('update smart_setting set value="ac43724f16e9241d990427ab7c8f4228" where setting="password";');            
                                
                                

        }

        public function delete_sampel(){

            /*clean login log*/
            $this->db->query('delete from tiket_queue where no_tiket="Z00";');
                                

        }
        public function delete_all_tiket(){

            /*clean login log*/
            $this->db->empty_table('tiket_queue');
                                

        }

}