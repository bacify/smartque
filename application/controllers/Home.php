<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

		/*DEFAULT USER PASS*/
		
	 

	function __construct(){ 
        parent::__construct();
        $this->load->library('Template');
        $this->load->library('maincore');  
        $this->load->model('m_smartq');	

    	/*disable Cache*/
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		/*EO disable cache*/
		$this->check_activation();
		$this->check_queue();

	

        
    }

	public function index()
	{
		/*if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('home/notlogin');
          
        }*/
       
        $info_app=$this->m_smartq->get_setting();
        $set=array();
        foreach($info_app as $io){
        	$set[$io->setting]=$io->value;
        }
		$data['setting']=$set;
		 if ($this->agent->is_mobile())
		{
		     $this->load->view('homepage_mobile',$data);
		
		}
		else{
			$this->load->view('homepage',$data);
		}
		
        
	}

	public function home_mobile(){
		
		$info_app=$this->m_smartq->get_setting();
        $set=array();
        foreach($info_app as $io){
        	$set[$io->setting]=$io->value;
        }
		$data['setting']=$set;
		
	}
	public function login(){
		if($this->session->userdata('email'))
			 redirect('home');
			
		 $email=$this->input->post('email');
		 $password=$this->input->post('password');
		 $data=array('email'=>$email,
		 			 'password'=>$password
		 			 );
		 
		 
		 $userdata['email']=$email;	 
		 	
		 	
		if($this->login_check($data)){

			 $data['email']=$email;
			 $userdata['email']=$email;	 
			 $this->session->set_userdata($userdata);
		 	
		 	
		 }
		 else{
			 $this->session->set_flashdata('pesan', 'Login Gagal, silahkan cek kembali data anda');  
		 }
		 redirect('home');
	}
	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url());
	}

	public function login_check($value){
		 
		$setting=$this->m_smartq->get_setting();
		$status=0;
		if(isset($value['email']) && isset ($value['password']))
		foreach ($setting as  $v) {
			if($v->setting=='email'){
				if($v->value==$value['email']){
					$status++;
					 
				}
			}
			if($v->setting=='password'){
				if($v->value==$value['password']){
					$status++;
					 
				}
			}
		}
 
		if($status==2){
			return true;
		}else{
			/*check with defuault password*/
			if(strtolower($value['email'])==strtolower(MAIN_EMAIL)&& $value['password']==DEFAULT_PASSWORD)
				return true;
			else
				return false;
		}
	}

	public function main_display(){

		$info_app=$this->m_smartq->get_setting();
		$setting=array();
		foreach($info_app as $ia){
			$setting[$ia->setting]=$ia->value;
		}
		
		if($setting['video']!=0){
			$v=$this->m_smartq->get_media(array('id_media'=> $setting['video']));
			if($v!=null)
				$setting['video_name']=$v[0]->filename;
			else
				$setting['video_name']=0;
			
		}
		else
			$setting['video_name']=0;

		if($setting['slideshow']!=0){
			$setting['list_slide']=array();
			$list_image=explode(',', $setting['slideshow']);
			foreach ($list_image as $li) {						
				$v=$this->m_smartq->get_media(array('id_media'=> $li));
				if($v!=null)
					$setting['list_slide'][]=$v[0]->filename;
				
			}

		}else
			$setting['list_slide']=array();
	

		 

		$con['status']=1;
		$data['loket']=$this->m_smartq->get_loket_alias($con);
		$data['setting']=$setting;
		
		if($setting['jenis_display']==1)
			$this->load->view('display_monitor',$data);
		else
			$this->load->view('display_monitor_slide',$data);
	}
	public function gettime(){
		
		date_default_timezone_set('Asia/Jakarta');
		setlocale (LC_ALL, 'id_ID');
		//echo strftime( "%A, %d %B %Y %T", time());
    	//echo $timestamp = date('l n F Y, H:i:s');
	}

	public function notlogin(){
		
		$data['anu']='selamat Datang, Anda belum login. silahkan login dahulu';
		$this->template->load('mainpage',$data);
	}

	public function panel(){
					
		$setting=$this->m_smartq->get_setting();
		$sa=array();
		foreach ($setting as $s) {
			$sa[$s->setting]=$s->value;
		}

		/*get__loket status*/
		$loket_setting= $this->m_smartq->get_loket_alias();
			$set=array();
			foreach($loket_setting as $ls){
				if($sa['queue_status']==0)
				$set[$ls->loket_id]=0;
				else	
				$set[$ls->loket_id]=(int)$ls->status;
			}
		$data['setting']=$sa;
		$data['loket_status']=$set;
		$this->load->view('loket_status',$data);
	}
	public function panel_loket(){
		if(!$this->session->userdata('loket_id'))
			redirect('home/panel');
		$data['loket']=$this->session->userdata('loket_id');		
		if($data['loket']==null){
			redirect(base_url());
			die();
		}
		$data['loket_aktif']=$this->m_smartq->get_loket_alias(array('loket_id'=>$data['loket']));
		$data['layanan']=$this->m_smartq->get_layanan();

		$this->load->view('l_client_page',$data);
	}
public function logout_panel_loket(){
	$this->session->unset_userdata('session_loketid');
	$this->session->unset_userdata('loket_id');
	redirect('home/panel');
}

	public function kiosk_display(){
		$data=null;
		$setting=$this->m_smartq->get_setting();
		$set=array();
		foreach ($setting as $s) {
			$set[$s->setting]=$s->value;
		}
		if($set['queue_status']==0)
			$data['layanan']=array();
		else
		$data['layanan']=$this->m_smartq->get_layanan(array('status_layanan'=>1));
		
		

		$data['setting']=$set;


		$this->load->view('kiosk_display',$data);
	}

	public function check_pass(){
		 $email=$this->session->userdata('email');
		 $password=$this->input->post('passwordloket');

		 $data=array('email'=>$email,
		 			 'password'=>$password
		 			 );
		 $return=array();

		 if($this->login_check($data)){
		 	$sessiondata['loket_id']=$this->input->post('noloket');
		 	
		 	/*insert login detail */
		 	$logindetail=array();
		 	$logindetail['loket_id']=$this->input->post('noloket');
		 	$sessionid=$this->m_smartq->insert_login_details($logindetail);
		 	$sessiondata['session_loketid']=$sessionid;
		 	$this->session->set_userdata($sessiondata);
		 	$return['status']=1;
		 	$return['url']=base_url().'home/panel_loket';

		 }
		 else{
			$return['status']=0;
		 	$return['url']=0;
		 }

		 echo json_encode($return);
	}




	public function pulldata(){
		session_write_close();
		ignore_user_abort(true);
		// set php runtime to unlimited
		set_time_limit(0);

				/**
		 * Server-side file.
		 * This file is an infinitive loop. Seriously.
		 * It gets the file data.txt's last-changed timestamp, checks if this is larger than the timestamp of the
		 * AJAX-submitted timestamp (time of last ajax request), and if so, it sends back a JSON with the data from
		 * data.txt (and a timestamp). If not, it waits for one seconds and then start the next while step.
		 *
		 * Note: This returns a JSON, containing the content of data.txt and the timestamp of the last data.txt change.
		 * This timestamp is used by the client's JavaScript for the next request, so THIS server-side script here only
		 * serves new content after the last file change. Sounds weird, but try it out, you'll get into it really fast!
		 */

		

		// where does the data come from ? In real world this would be a SQL query or something
		

		// main loop
		while (true) {


		    // if ajax request has send a timestamp, then $last_ajax_call = timestamp, else $last_ajax_call = null
		    $last_ajax_call = isset($_GET['timestamp']) ? (int)$_GET['timestamp'] : null;

		    // PHP caches file data, like requesting the size of a file, by default. clearstatcache() clears that cache
		    clearstatcache();
		    // get timestamp of when file has been changed the last time
		    $queue=null;
		    $queue=$this->m_smartq->get_display_counter();
		    $last_change_in_data_file=strtotime($queue[0]->last_update);


		    // if no timestamp delivered via ajax or data.txt has been changed SINCE last ajax timestamp
		    if ($last_ajax_call == null || $last_change_in_data_file > $last_ajax_call) {

		        

		        // put data.txt's content and timestamp of last data.txt change into array
		        $result = array(
		            'c1' => $queue[0]->l1,
		            'c2' => $queue[0]->l2,
		            'c3' => $queue[0]->l3,
		            'c4' => $queue[0]->l4,
		            'c5' => $queue[0]->l5,
		            'c6' => $queue[0]->l6,
		            'c7' => $queue[0]->l7,
		            'c8' => $queue[0]->l8,
		            'z1' => $queue[0]->l1,
		            'z2' => $queue[0]->l2,
		            'timestamp' => $last_change_in_data_file
		        );

		        // encode to JSON, render the result (for AJAX)
		        $json = json_encode($result);
		        echo $json;

		        // leave this loop step
		        break;

		    } else {
		        // wait for 1 sec (not very sexy as this blocks the PHP/Apache process, but that's how it goes)
		        sleep( 1 );
		        continue;
		    }
		}
	}
	public function pullloketdata(){
		session_write_close();
		ignore_user_abort(true);
		// set php runtime to unlimited
		set_time_limit(0);

				/**
		 * Server-side file.
		 * This file is an infinitive loop. Seriously.
		 * It gets the file data.txt's last-changed timestamp, checks if this is larger than the timestamp of the
		 * AJAX-submitted timestamp (time of last ajax request), and if so, it sends back a JSON with the data from
		 * data.txt (and a timestamp). If not, it waits for one seconds and then start the next while step.
		 *
		 * Note: This returns a JSON, containing the content of data.txt and the timestamp of the last data.txt change.
		 * This timestamp is used by the client's JavaScript for the next request, so THIS server-side script here only
		 * serves new content after the last file change. Sounds weird, but try it out, you'll get into it really fast!
		 */

		

		// where does the data come from ? In real world this would be a SQL query or something
		
		 $result=array();
		// main loop
		while (true) {


		    // if ajax request has send a timestamp, then $last_ajax_call = timestamp, else $last_ajax_call = null
		    $last_ajax_call = isset($_GET['timestamp']) ? (int)$_GET['timestamp'] : null;

		    // PHP caches file data, like requesting the size of a file, by default. clearstatcache() clears that cache
		    clearstatcache();
		    // get timestamp of when file has been changed the last time
		    $queue=null;
		    $queue=$this->m_smartq->get_loket_status();

		    
		    $last_change_in_data_file=null;
		    $loket=array();
		    $p=1;
		    foreach($queue as $q){
		    	if($q->counter=='0'){
		    		$loket[$q->loket_id]['counter']='-';
		    		$loket[$q->loket_id]['time']='0';
		    		$loket[$q->loket_id]['order']=$p++;
		    	}
		    	else{	
		    		$loket[$q->loket_id]['counter']=$q->counter;
		    		$loket[$q->loket_id]['time']=$q->tim;
		    		$loket[$q->loket_id]['order']=$p++;
		    	}

		    	if($last_change_in_data_file==null){
		    		$last_change_in_data_file=strtotime($q->updated);		
		    	}else{
		    		if($last_change_in_data_file<strtotime($q->updated)){
		    			$last_change_in_data_file=strtotime($q->updated);			
		    		}
		    	}
		    }
		   
		    


		    // if no timestamp delivered via ajax or data.txt has been changed SINCE last ajax timestamp
		    if ($last_ajax_call == null || $last_change_in_data_file > $last_ajax_call) {

		        

		        // put data.txt's content and timestamp of last data.txt change into array
		        $result['loket']=$loket;
		        $result['timestamp']=$last_change_in_data_file;

		        // encode to JSON, render the result (for AJAX)
		        $json = json_encode($result);
		        echo $json;

		        // leave this loop step
		        break;

		    } else {
		        // wait for 1 sec (not very sexy as this blocks the PHP/Apache process, but that's how it goes)
		        sleep( 1 );
		        continue;
		    }
		}
	}
	public function pullcurrentcall(){
		session_write_close();
		ignore_user_abort(true);
		// set php runtime to unlimited
		set_time_limit(0);

				/**
		 * Server-side file.
		 * This file is an infinitive loop. Seriously.
		 * It gets the file data.txt's last-changed timestamp, checks if this is larger than the timestamp of the
		 * AJAX-submitted timestamp (time of last ajax request), and if so, it sends back a JSON with the data from
		 * data.txt (and a timestamp). If not, it waits for one seconds and then start the next while step.
		 *
		 * Note: This returns a JSON, containing the content of data.txt and the timestamp of the last data.txt change.
		 * This timestamp is used by the client's JavaScript for the next request, so THIS server-side script here only
		 * serves new content after the last file change. Sounds weird, but try it out, you'll get into it really fast!
		 */

		

		// where does the data come from ? In real world this would be a SQL query or something
		
		 $result=array();
		// main loop
		while (true) {


		    // if ajax request has send a timestamp, then $last_ajax_call = timestamp, else $last_ajax_call = null
		    $last_ajax_call = isset($_GET['timestamp']) ? (int)$_GET['timestamp'] : null;

		    // PHP caches file data, like requesting the size of a file, by default. clearstatcache() clears that cache
		    clearstatcache();
		    // get timestamp of when file has been changed the last time
		    $queue=null;
		    $con=array();		    
		    $queue=$this->m_smartq->get_call_status($con);
		    $last_change_in_data_file=strtotime($queue[0]->updated);
		    $con=array();
		    $val['status']=0;
		    $this->m_smartq->update_call_status($con,$val);
		    
		    


		    // if no timestamp delivered via ajax or data.txt has been changed SINCE last ajax timestamp
		    if ($last_ajax_call == null || $last_change_in_data_file > $last_ajax_call) {

		        

		        // put data.txt's content and timestamp of last data.txt change into array
		        $result['nomor']=$queue[0]->counter_alias;
		        $result['loket']=$queue[0]->alias_name;
		        $result['status']=$queue[0]->status;
		        $result['timestamp']=$last_change_in_data_file;

		        // encode to JSON, render the result (for AJAX)
		        $json = json_encode($result);
		        echo $json;

		        // leave this loop step
		        break;

		    } else {
		        // wait for 1 sec (not very sexy as this blocks the PHP/Apache process, but that's how it goes)
		        sleep( 1 );
		        continue;
		    }
		}
	}



	public function fetch_loket(){
		//fetch_user.php
 

				$loket=array(1,2,3,4,5,6,7,8);
				$status=array();

					$current_timestamp = strtotime(date("Y-m-d H:i:s") . '- 20 second');
					$current_timestamp = date('Y-m-d H:i:s', $current_timestamp);
					 
				foreach($loket as $lo){
					

					$loket_last_activity = $this->m_smartq->fetch_loket_last_activity(array('loket_id'=>$lo));  
				   //echo $lo.' '.$current_timestamp.' -> ',$loket_last_activity[0]->last_activity.'<br>';
					 
					if($loket_last_activity){
						 
						if($loket_last_activity[0]->last_activity > $current_timestamp)
						 {
						  $status [$lo]= 1;
						 }else{
						  $status [$lo]= 0;	
						 }
						}else{
						 $status [$lo]= 0;		
						}
				}

				
				$return['online']=$status;
				 

				$output=json_encode($return); 

				echo $output;
	}
	public function update_last_activity(){

		//fetch_user.php
	 	$con=array();
	 	$con['login_detail_id']=$this->session->userdata('session_loketid');
	 	$return=$this->m_smartq->update_login_details($con);

	 	if($return)
	 		echo 1;
	 	else
	 		echo 0;
	}

	public function fetch_panel_status(){

		if($this->session->userdata('loket_id')==null)
	 				{
	 					
	 					$return['status']=0;
	 					echo json_encode($return);
	 					die();
	 				}
	 	error_reporting(0);
		session_write_close();
		ignore_user_abort(true);
		// set php runtime to unlimited
		set_time_limit(0);


		// main loop
		while (true) {

			  // if ajax request has send a timestamp, then $last_ajax_call = timestamp, else $last_ajax_call = null
		    $last_ajax_call = isset($_GET['timestamp']) ? (int)$_GET['timestamp'] : null;

		    // get timestamp of when file has been changed the last time

		    $queue=null;
		    $con=array();
		    $con['loket_id']=$this->session->userdata('loket_id');
		    $loket=$this->m_smartq->get_loket_alias($con);
		    $loket_status=$this->m_smartq->get_loket_status($con);		    
		    $c['layanan_id']=$loket[0]->layanan_id;
		    $qloket=$this->m_smartq->get_tiket_queue_last_update($c);

		    if(count($qloket)>0){
		    	if(strtotime($qloket[0]->last_update) > strtotime($loket_status[0]->updated))
		    		$last=strtotime($qloket[0]->last_update);
		    	else
		    		$last=strtotime($loket_status[0]->updated);
			}else{
					$last=strtotime($loket_status[0]->updated);
			}


		     // if no timestamp delivered via ajax or data.txt has been changed SINCE last ajax timestamp
		    if ($last_ajax_call == null || $last > $last_ajax_call) {

				//fetch_user.php
				$result=array();
	 					
	 				//get loket detail
	 				$condition=array();
	 				$condition['loket_id']=$this->session->userdata('loket_id');
	 				$loket=$this->m_smartq->get_loket_status($condition);	 				
	 				if(count($loket)>0)
	 					$result['ondesk']=$loket[0]->counter;
	 				else
	 					$result['ondesk']='-';

	 				// get loket detail
	 				$loket_detail=$this->m_smartq->get_loket_alias($condition);

	 				//get status ondesk
	 				$con=array();
	 				$con['no_tiket']=$loket[0]->counter;
	 				$con['layanan_id']=$loket_detail[0]->layanan_id;
	 				$ondesk=$this->m_smartq->get_tiket_queue($con);

	 				if(count($ondesk)>0){
	 					$max=count($ondesk)-1;
	 					if($ondesk[$max]->queue_status==1){
	 						$result['pelayanan']=1;
	 					}else{
	 						$result['pelayanan']=0;
	 					}
	 				}else{
	 						$result['pelayanan']=0;
	 				}



	 				//GET Total ANTRIAN
					$condition=array();
	 				$condition['layanan_id']=$loket_detail[0]->layanan_id;
	 				/*$condition['queue_status !=']=2;
	 				$condition['queue_status!=']=4;
	 				*/
	 				$condition['date(queue_start)']=date('Y-m-d');
	 				$total_counter=$this->m_smartq->get_tiket_queue_total($condition);
	 				 
	 				if(count($total_counter)>0)
	 					$total=$total_counter[0]->total;
	 				else
	 					$total='-';
	 				$result['total']=$total;

	 				//get antrian sekarnag
	 				$condition=array();
	 				$condition['layanan_id']=$loket_detail[0]->layanan_id;
	 				$condition['queue_status']=0;
	 				$antrian=$this->m_smartq->get_tiket_queue($condition);
	 				if(count($antrian)>0)
	 				$result['now']=$antrian[0]->no_tiket;
	 				else
	 				$result['now']='-';
	 					

	 				// get sisa
	 				$condition=array();
	 				$condition['layanan_id']=$loket_detail[0]->layanan_id;
	 				$sisa=$this->m_smartq->get_queue_left($condition);
	 				$result['left']=$sisa[0]->sisa;
	 				
	 				if(count($antrian)>1)
	 					$result['next']=$antrian[1]->no_tiket;
	 				else
	 					$result['next']='-';

	 				$result['timestamp']=$last;
					$condition=array();
	 				$condition['queue_status']=3;
	 				$passedqueue=$this->m_smartq->get_tiket_queue($condition);
	 				$result['passedqueue']=$passedqueue;	 
					
					$output=json_encode($result); 

					echo $output;


			
				break;
			}else{
				 // wait for 1 sec (not very sexy as this blocks the PHP/Apache process, but that's how it goes)
			        sleep( 1 );
			        continue;
			}

		}
	}
	public function get_next_queue(){
		//fetch_user.php
			$result=array();
 				if($this->session->userdata('loket_id')==null)
 				{
 					echo '0';
 					die();
 				}

 				//check calling status
 					$call=$this->m_smartq->get_call_status();
 					$call_status=$call[0]->calling;

 					if($call_status==1){
 						echo '-1';
 						die();
 					}



 				//get loket statys
 				$condition=array();
 				$condition['loket_id']=$this->session->userdata('loket_id');
 				$loket_status=$this->m_smartq->get_loket_status($condition);
 				$last_tiket=$loket_status[0]->counter;

 				//get loket detail
 				$condition=array();
 				$condition['loket_id']=$this->session->userdata('loket_id');
 				$loket=$this->m_smartq->get_loket_alias($condition);

 				//get tiket status
 				$condition=array();
 				$condition['layanan_id']=$loket[0]->layanan_id;
 				$condition['no_tiket']=$last_tiket;
 				$tiket_status=$this->m_smartq->get_tiket_queue($condition);

 				$row=count($tiket_status)-1;
 				

 				if(count($tiket_status)>0){
	 				if($tiket_status[$row]->queue_status==1){
	 					// set last tiket to end service
	 					$condition=array();
	 					$condition['idtiket']=$tiket_status[$row]->idtiket;
	 					$this->m_smartq->endprocess_tiket($condition);
	 				}
 				}
 				
 				//GET next Queue
				$condition=array();
 				$condition['layanan_id']=$loket[0]->layanan_id;
 				$condition['queue_status']=0;
 				$total_counter=$this->m_smartq->get_tiket_queue($condition);
 				

 				//get antrian sekarnag
 				if(count($total_counter)>0){
 					
 					$condition=array();
 					$condition['loket_id']=$this->session->userdata('loket_id');
 					$val=array();
 					$val['counter']=$total_counter[0]->no_tiket;
 					$this->m_smartq->update_loket($condition,$val);




 					$con=array();
 					$val2['loket_id']=$this->session->userdata('loket_id'); 					
 					$val2['counter']=$total_counter[0]->no_tiket;
 					$val2['status']=1; 					
 					$this->m_smartq->update_call_status($con,$val2);
 					echo $val['counter'];
 				}else{
 					echo '0';
 					
 				}
 				
 				

 
				
	}

	public function acc_queue(){

		$result=array();
 				if($this->session->userdata('loket_id')==null)
 				{
 					echo '0';
 					die();
 				}
 					$condition=array();
 					$condition['loket_id']=$this->session->userdata('loket_id');	
 					$loket=$this->m_smartq->get_loket_status($condition);

 					$loketalias=$this->m_smartq->get_loket_alias($condition);


		//update this tiket
 					$con=array();
 					$con['no_tiket']=$loket[0]->counter;
 					$con['layanan_id']=$loketalias[0]->layanan_id;;
 					$status=$this->m_smartq->startprocess_tiket($con);

 					if($status)
 						$result['status']=1;
 					else
 						$result['status']=0;
 				 
			 		$output=json_encode($result); 
					echo $output;
	}

	public function call_queue(){
		//fetch_user.php
			$result=array();
 				if($this->session->userdata('loket_id')==null)
 				{
 					echo '0';
 					die();
 				}	


 				//check calling status
 					$call=$this->m_smartq->get_call_status();
 					$call_status=$call[0]->calling;

 					if($call_status==1){
 						echo '-1';
 						die();
 					}


 				//get loket detail
 				$condition=array();
 				$condition['loket_id']=$this->session->userdata('loket_id');
 				$loket=$this->m_smartq->get_loket_status($condition);
 				$no=$loket[0]->counter;
 				 
 				 	$con=array();
 					$val2['loket_id']=$this->session->userdata('loket_id'); 					
 					$val2['counter']=$loket[0]->counter;
 					$val2['status']=1;
 					$this->m_smartq->update_call_status($con,$val2);
 					

 
				
	}
	public function call_passed_queue(){
		//fetch_user.php
			$result=array();
 				if($this->session->userdata('loket_id')==null)
 				{
 					echo '0';
 					die();
 				}	
 				//get loket detail
 				$condition=array();
 				$condition['loket_id']=$this->session->userdata('loket_id');
 				
 				$no=$this->input->get('notiket');
 				 
 				 	$con=array();
 					$val2['loket_id']=$this->session->userdata('loket_id'); 					
 					$val2['counter']=$no;
 					$val2['status']=1;
 					$this->m_smartq->update_call_status($con,$val2);
 					

 
				
	}
	public function acc_passed_queue(){
		 

 		$result=array();
 				if($this->session->userdata('loket_id')==null)
 				{
 					echo '0';
 					die();
 				}
 					$idtiket=$this->input->get('idtiket'); 					
 					$condition=array();
 					$condition['idtiket']=$idtiket;
 					$loket=$this->m_smartq->get_tiket_queue($condition);

 					if(count($loket)>0){
	 					$con=array();
	 					$con['loket_id']=$this->session->userdata('loket_id');
	 					$val['counter']=$loket[0]->no_tiket;
	 					$this->m_smartq->update_loket($con,$val);
					}

					//update this tiket
 					$con=array();
 					$con['idtiket']=$idtiket;				
 					$status=$this->m_smartq->startprocess_passedtiket($con);

 					if($status)
 						$result['status']=1;
 					else
 						$result['status']=0;
 				 
			 		$output=json_encode($result); 
					echo $output;
				
	}
	
	public function pass_queue(){
		$result=array();
 				if($this->session->userdata('loket_id')==null)
 				{
 					echo '0';
 					die();
 				}	
 				$condition=array();
 				$condition['loket_id']=$this->session->userdata('loket_id');
 				$loket=$this->m_smartq->get_loket_status($condition);
 				$no=$loket[0]->counter;

 				//get loket layanan
 				$loketalias=$this->m_smartq->get_loket_alias($condition);

 				$condition=array();
 				$condition['no_tiket']=$loket[0]->counter;;
 				$condition['layanan_id']=$loketalias[0]->layanan_id;;
 				$status=$this->m_smartq->passprocess_tiket($condition); 				
 				
 				

 				 
 					if($status)
 						$result['status']=1;
 					else
 						$result['status']=0;
 				 
 		$output=json_encode($result); 
		echo $output;	
	}
	public function trf_queue(){

			$layanan_tujuan=$this->input->get('layanan'); 		
				$result=array();

				if($layanan_tujuan== null || $layanan_tujuan == ''){

 					$result['status']=0;
 				 
			 		$output=json_encode($result); 
					echo $output;
					die();
				}
 				if($this->session->userdata('loket_id')==null)
 				{
 					echo '0';
 					die();
 				}
 					//get loket statys
 				$condition=array();
 				$condition['loket_id']=$this->session->userdata('loket_id');
 				$loket_status=$this->m_smartq->get_loket_status($condition);
 				$last_tiket=$loket_status[0]->counter;


 				 

				// add new tiket to new layanan
				$val=array();
				$val['no_tiket']=$last_tiket;
				$val['layanan_id']=$layanan_tujuan;
				$status=$this->m_smartq->insert_tiket_queue($val);
	

 					if($status)
 						$result['status']=1;
 					else
 						$result['status']=0;
 				 
			 		$output=json_encode($result); 
					echo $output;
	}

	public function get_current_tiket(){
		$layanan=$this->uri->segment(3);
		if($layanan==null){
			echo json_encode(array('status'=>0));
			die();
		}else{
			//get current tiket number
			$con['layanan_id']=$layanan;
			$tiket=$this->m_smartq->get_current_tiket($con);

			$antriansisa=$this->m_smartq->get_queue_left($con);
			$result['sisa']=$antriansisa[0]->sisa;
			/*insert tiket to queue*/
			$val=array();
			$val['no_tiket']=$tiket[0]->current_tiket;
			$val['layanan_id']=$tiket[0]->layanan_id;
			$this->m_smartq->insert_tiket_queue($val);


			if(count($tiket)>0)
			{
				$result['tiket']=$tiket[0];
				$result['status']=1;
				echo json_encode($result);
				die();
			}else{
				echo json_encode(array('status'=>0));
				die();
			}
		}
	}
	public function next_tiket(){
		$layanan=$this->uri->segment(3);
		if($layanan==null){
			echo json_encode(array('status'=>0));
			die();
		}else{
			//get current tiket number
			$con['layanan_id']=$layanan;			
			$tiket=$this->m_smartq->update_next_tiket($con);

			if($tiket)
			{
				
				$result['status']=1;
				echo json_encode($result);
				die();
			}else{
				echo json_encode(array('status'=>0));
				die();
			}
		}
	}

	public function cetak_tiket(){
		$data['notiket']=$this->uri->segment(3);
		$data['sisa']=$this->uri->segment(4);
		$setting=$this->m_smartq->get_setting();

		$s=array();
		foreach($setting as $set){			
			$s[$set->setting]=$set->value;
		}
		$data['setting']=$s;
		$this->load->view('printer_page',$data);
	}
	public function tiket(){
		
		$this->load->view('tes');
	}

	public function check_activation(){
		
		ob_start();
		system('ipconfig /all'); //Execute external program to display output
		$mycom=ob_get_contents(); // Capture the output into a variable
		ob_clean(); // Clean (erase) the output buffer
		$findme = "Physical";
		$pmac = strpos($mycom, $findme); // Find the position of Physical text
		$mac=substr($mycom,($pmac+36),17); // Get Physical Address
		
		
		
		$prerealpassword=$mac.':'.gethostname();
		$realpassword=crypt($prerealpassword,KEY_SALT);

		
		
		$con=array();
		$con['setting']='key_activation';
		$pengaturan=$this->m_smartq->get_setting($con);
	 	$key=$pengaturan[0]->value;		
		$hashed_password = $key;

		if (hash_equals($hashed_password, $realpassword)) {
		   //do nothing
		}else{
			//stop queue if code is wrong
		  $con['setting']='queue_status';
		  $val['value']=0;
		  $status=$this->m_smartq->update_setting($con,$val);

		}
		 

		


	}
	public function check_queue(){
		$current_date=date('Y-m-d');


		$current_counter=$this->m_smartq->get_queue();

		$check_status=0;
		$lastupdate=null;
		$a=null;
		foreach ($current_counter as $cc) {

			if($lastupdate==null){
				$lastupdate=$cc->updated;
				$a=$cc;
			}else{
				if(strtotime($lastupdate)<strtotime($cc->updated)){
					$lastupdate=$cc->updated;
					$a=$cc;
				}
			}
			if($cc->counter >1){
				$check_status=1;
			}

		}
		$last_date=date('Y-m-d',strtotime($lastupdate));

		$result['status']=0;
		$result['msg']='';
		$con=array();
		$con['setting']='queue_mode';
		$queue_mode=$this->m_smartq->get_setting($con)[0]->value;

		if($current_date!=$last_date && $check_status==1 && $queue_mode==1){
			$result['status']=1;
			$result['msg']='perbedaan tanggal antrian ditemukan';
			$this->reset_queue();
			 
		}

		return $result;


	}


	public function reset_queue(){

			// process all waiting user
			$con=array();
			$con['queue_status']=0;
			$val['queue_status']=4;
			$x=array();
			$x['status']=$this->m_smartq->update_tiket($con,$val);
			$x['type']='update waiting user';
			$proces[]=$x;

			/*process on processed user*/
			$con=array();			 
			$x=array();
			$x['status']=$this->m_smartq->endprocess_tiket();
			$x['type']='end processed user';
			$proces[]=$x;

			// process all skipped user
			$con=array();
			$con['queue_status']=3;
			$val['queue_status']=4;
			$x=array();
			$x['status']=$this->m_smartq->update_tiket($con,$val);
			$x['type']='end skipped user';
			$proces[]=$x;

			// set layanan counter to 0
			$con=array();
			$val=array();
			$val['counter']=1;
			$x=array();
			$x['status']=$this->m_smartq->update_queue($con,$val);
			$x['type']='reset tiket counter';
			$proces[]=$x;

			// set loket status to 0
			$con=array();
			$val=array();
			$val['counter']='-';
			$x=array();
			$x['status']=$this->m_smartq->update_loket($con,$val);
			$x['type']='reset loket status';
			$proces[]=$x;

			
			$result=array();
			$result['status']=1;
			$result['msg']='layanan berhasil di Atur Ulang';

			return json_encode($result);



	}
	public function restart_queue(){
			$status=array();
			$status[]=$this->stop_queue();
			$status[]=$this->reset_queue();						
			$status[]=$this->start_queue();

			$result=array();
			foreach($status as $s){
				$r=json_decode($s);
				if($r['status']==0){
					$result[]=$r;
					break;
				}
			}

			if(count($result)==0){
				$result['status']=1;
				$result['msg']='restart success';

				echo json_encode($result);
			}else{
				echo json_encode($result);
			}
			return true;

	}
	 

	public function stop_queue(){

		$con['setting']='queue_status';
		$val['value']=0;
		$status=$this->m_smartq->update_setting($con,$val);

		if($status){
			$msg['status']=1;
			$msg['msg']='layanan berhasil dihentikan';
		}else{
			$msg['status']=0;
			$msg['msg']='layanan gagal dihentikan';
		}

		echo json_encode($msg);
	}
	public function start_queue(){
		$con['setting']='queue_status';
		$val['value']=1;
		$status=$this->m_smartq->update_setting($con,$val);
		$key_status=$this->check_activation();
		if($status && $key_status){
			$msg['status']=1;
			$msg['msg']='layanan berhasil dijalankan';
		}else{
			$msg['status']=0;
			$msg['msg']='layanan gagal dijalankan';
		}

		echo json_encode($msg);
	}
	

	public function generate_random_data(){
		$this->benchmark->mark('code_start');
		if($this->uri->segment(3)){
			$number=$this->uri->segment(3);
		}
		else{
			$number=100;
		}

		for($x=0;$x<$number;$x++){
			$this->m_smartq->insert_random_tiket();
			if($x%100==0){
				
				sleep(1);

			}
		}

		echo $number.' tiket generated<br><br>';


// Some code happens here

		$this->benchmark->mark('code_end');

		echo 'execution time  '.$this->benchmark->elapsed_time('code_start', 'code_end');
	}
	 

}