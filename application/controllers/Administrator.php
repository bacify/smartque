<?php
defined('BASEPATH') OR exit('No direct script access allowed');




class Administrator extends CI_Controller {
	

	public $salt;

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		$this->salt = 'seranggalangit';
        parent::__construct();
        $this->load->library('Admintemplate');
        $this->load->library('maincore');  
        $this->load->model('m_smartq');	

    	/*disable Cache*/
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		/*EO disable cache*/
		$this->check_activation();
		$this->check_queue();


        
    }



    public function index()
	{
		if(!$this->session->userdata('email'))
			 redirect(base_url().'home');
		
		$stats=$this->m_smartq->get_stat_total();
		$con['YEAR(queue_start)=']=date('Y');
		$stats_total=$this->m_smartq->get_stat_total_layanan($con);
		$info=array();
		$tlayanan=array();
		$tlayanan[0]=$stats_total[0]->layanan_1;
		$tlayanan[1]=$stats_total[0]->layanan_2;
		$tlayanan[2]=$stats_total[0]->layanan_3;
		$tlayanan[3]=$stats_total[0]->layanan_4;
		$tlayanan[4]=$stats_total[0]->layanan_5;
		$tlayanan[5]=$stats_total[0]->layanan_6;
		$tlayanan[6]=$stats_total[0]->layanan_7;
		$tlayanan[7]=$stats_total[0]->layanan_8;
		$data['total_layanan']=$stats_total;
		$data['tlayanan']=$tlayanan;
		$color=array('#4e73df', '#1cc88a', '#36b9cc','#da4b05','#cb0dc2','#138041','#cbb6f3','#64d61a');
		$data['color']=$color;

		foreach ($stats as $total) {
			$info['total']=$total->total;
			$info['total_year']=$total->total_year;
			$info['total_month']=$total->total_month;
			$info['total_day']=$total->total_day;
			$info['day']=$total->day;
		}
		$data['layanan']=$this->m_smartq->get_layanan();
		$data['info']=$info;
		$current_year_stat=$this->m_smartq->get_total_current_year();
		$year_stats=array();

		foreach ($current_year_stat as $cys) {
			$year_stats['januari']=$cys->januari;
			$year_stats['februari']=$cys->februari;
			$year_stats['maret']=$cys->maret;
			$year_stats['april']=$cys->april;
			$year_stats['mei']=$cys->mei;
			$year_stats['juni']=$cys->juni;
			$year_stats['juli']=$cys->juli;
			$year_stats['agustus']=$cys->agustus;
			$year_stats['september']=$cys->september;
			$year_stats['oktober']=$cys->oktober;
			$year_stats['nopember']=$cys->nopember;
			$year_stats['desember']=$cys->desember;
			 
		}
		$data['year_stats']=$year_stats;

		$this->admintemplate->load('a_page_statistic',$data);
	}

	public function status()
	{
		$con['setting']='queue_status';
		$data['status']=$this->m_smartq->get_setting($con)[0]->value;

		$this->admintemplate->load('a_page_status',$data);
	}

	public function login()
	{
		
		
		$data['anu']='abcsds';
		$this->load->view('login',$data);
	}

	public function loket(){
		$submit = $this->input->post('submit');

		if($submit!=null){
			
			foreach(range(1, 8) as $num){
				$status=null;
				if($this->input->post('status-'.$num)==null)
					$status=0;
				else
					$status=1;
				$alias=null;
				$alias=$this->input->post('alias-'.$num);
				$layanan=null;
				$layanan=$this->input->post('layanan-'.$num);
				$condition=array();
				$condition['loket_id']=$num;
				$value=array();
				$value['alias_name']=$alias;
				$value['status']=$status;
				$value['layanan_id']=$layanan;
				$this->m_smartq->update_loket_alias($condition,$value);
			}
			
		}


		$data=array();
		$data['loket']=$this->m_smartq->get_loket_alias();
		$data['layanan']=$this->m_smartq->get_layanan();
		$this->admintemplate->load('a_loket',$data);
	}
	public function layanan(){

		$submit = $this->input->post('submit');

		if($submit!=null){
			foreach(range(1, 8) as $num){
				$status=null;
				if($this->input->post('status-'.$num)==null)
					$status=0;
				else
					$status=1;
				$nama=null;
				$nama=$this->input->post('nama-'.$num);
				$condition=array();
				$condition['id_layanan']=$num;
				$value=array();
				$value['status_layanan']=$status;
				$value['nama_layanan']=$nama;
				$this->m_smartq->update_layanan($condition,$value);
			}
			
		}

		$data=array();		
		$data['layanan']=$this->m_smartq->get_layanan();
		$this->admintemplate->load('a_layanan',$data);
	}
	public function setting(){
	
		$submit = $this->input->post('submit');
		
		if($submit!=null){
			$err=array();
			 
			 
			/*ubah nama Instansi*/
			$setting=$this->m_smartq->get_setting();


			foreach ($setting as $sm) {
				if($this->input->post($sm->setting)){

					
					if($sm->setting=='queue_status'){
						$key_status=$this->check_activation();
						if(!$key_status)	
							continue;
					}


					$val=array();
					$con=array();
					$val['value']=$this->input->post($sm->setting);
					if($sm->setting=='password'){
						if(strlen($val['value'])==32)
							continue;
						$val['value']=$val['value'];
						
					}
					$con['setting']=$sm->setting;
					$status=$this->m_smartq->update_setting($con,$val);
					$err[]=$status;
					if($status)
					$this->session->set_flashdata($sm->setting, '<b>'.$sm->setting.'</b> berhasil di update');
				}else{
					$val=array();
					$con=array();
					$val['value']=0;
					$con['setting']=$sm->setting;
					$err[]=$this->m_smartq->update_setting($con,$val);
				}
			}
			
			
		}

		$data=array();	
		$setting=$this->m_smartq->get_setting();
		$set=array();
		foreach ($setting as $a) {
			$set[$a->setting]=$a->value;
		}
		$data['setting']=$set;

		$data['images']=$this->m_smartq->get_media(array('ext!='=>'.mp4'));
		$data['video']=$this->m_smartq->get_media(array('ext'=>'.mp4'));
		
		$this->admintemplate->load('a_setting',$data);
	}
	public function media(){

		$submit = $this->input->post('submit');

		if($submit!=null){
			print_r($this->input->post());
			
		}
		$data=array();	
		/*get MEDIA*/
		$data['media']=$this->m_smartq->get_media();

			
		
		$this->admintemplate->load('a_media',$data);
	}
	public function fileupload(){
			
		 		$config['upload_path']          = './assets/uploads/';
                $config['allowed_types']        = 'gif|jpg|png|mp4|avi';
                
                $this->upload->initialize($config);

                if ( ! $this->upload->do_upload('inputfile'))
                {
                        $error = array('error' => $this->upload->display_errors());

                        echo json_encode($error);
                }
                else
                {
                		$data = $this->upload->data();
                		$val=array();
                		$val['filename']=$data['file_name'];
                		$val['size']=$data['file_size'];
                		$val['ext']=$data['file_ext'];
                		$val['width']=$data['image_width'];
                		$val['file_type']=$data['file_type'];
                		$id=$this->m_smartq->save_media($val);

                		
							$initial=base_url().'assets/uploads/'.$data['file_name'];
							$intialpreview=array('caption'=> $data['file_name'],
												  'size' =>$data['file_size'],
												  'file_type' =>$data['file_type'],
												  'downloadUrl'=>$initial,
												  'url'=>base_url().'administrator/deletefile/'.$id,
												 'key' => $id);
							$append=true;

							$return=array('initialPreview'=>$initial,
										   'initialPreviewConfig'=> array($intialpreview),
										'append' => $append);
							echo json_encode($return);
						
                }

         
	}

	public function deletefile(){
		$id=$this->uri->segment(3);
		//echo $id;
		//GET MEDIA
		$con['id_media']=$id;
		$media=$this->m_smartq->get_media($con);
		if($media!=null){
			$filename=$media[0]->filename;
			//DELETE MEDIA
			$status=$this->m_smartq->delete_media($con);
			if($status){
				$fileurl='./assets/uploads/'.$filename;
				if(unlink($fileurl)) {
				     $return['message']='file Deleted successfully';
				}
				else {
				     $return['message']='delete file failed';
				}
			}else{
				     $return['message']='error db query';
			}
		}else{
			$return['message']='db data Not exist';
		}

		echo json_encode($return);
	}

	
	public function restart_queue(){
			$status=array();
			$status[]=$this->stop_queue();
			$status[]=$this->reset_queue();						
			$status[]=$this->start_queue();

			$result=array();
			foreach($status as $s){
				$r=json_decode($s);
				if($r['status']==0){
					$result[]=$r;
					break;
				}
			}

			if(count($result)==0){
				$result['status']=1;
				$result['msg']='restart success';

				echo json_encode($result);
			}else{
				echo json_encode($result);
			}
			return true;

	}

	public function reset_queue(){

			// process all waiting user
			$con=array();
			$con['queue_status']=0;
			$val['queue_status']=4;
			$x=array();
			$x['status']=$this->m_smartq->update_tiket($con,$val);
			$x['type']='update waiting user';
			$proces[]=$x;

			/*process on processed user*/
			$con=array();			 
			$x=array();
			$x['status']=$this->m_smartq->endprocess_tiket();
			$x['type']='end processed user';
			$proces[]=$x;

			// process all skipped user
			$con=array();
			$con['queue_status']=3;
			$val['queue_status']=4;
			$x=array();
			$x['status']=$this->m_smartq->update_tiket($con,$val);
			$x['type']='end skipped user';
			$proces[]=$x;

			// set layanan counter to 0
			$con=array();
			$val=array();
			$val['counter']=1;
			$x=array();
			$x['status']=$this->m_smartq->update_queue($con,$val);
			$x['type']='reset tiket counter';
			$proces[]=$x;

			// set loket status to 0
			$con=array();
			$val=array();
			$val['counter']='-';
			$x=array();
			$x['status']=$this->m_smartq->update_loket($con,$val);
			$x['type']='reset loket status';
			$proces[]=$x;



			 
			$result=array();
			$result['status']=1;
			$result['msg']='layanan berhasil di Atur Ulang';

			echo json_encode($result);



	}

	public function stop_queue(){

		$con['setting']='queue_status';
		$val['value']=0;
		$status=$this->m_smartq->update_setting($con,$val);

		if($status){
			$msg['status']=1;
			$msg['msg']='layanan berhasil dihentikan';
		}else{
			$msg['status']=0;
			$msg['msg']='layanan gagal dihentikan';
		}

		echo json_encode($msg);
	}
	public function start_queue(){
		$con['setting']='queue_status';
		$val['value']=1;
		$status=$this->m_smartq->update_setting($con,$val);
		$key_status=$this->check_activation();
		if($status && $key_status){
			$msg['status']=1;
			$msg['msg']='layanan berhasil dijalankan';
		}else{
			$msg['status']=0;
			$msg['msg']='layanan gagal dijalankan';
		}

		echo json_encode($msg);
	}

	public function getprekey(){
		ob_start();
		phpinfo();
		$conten['phpinfo'] = ob_get_contents();
		system('ipconfig /all'); //Execute external program to display output
		$mycom=ob_get_contents(); // Capture the output into a variable
		ob_clean(); // Clean (erase) the output buffer
		$findme = "Physical";
		$pmac = strpos($mycom, $findme); // Find the position of Physical text
		$mac=substr($mycom,($pmac+36),17); // Get Physical Address
		$conten['mac']=$mac;

		$json=json_encode($conten);
		$re=base64_encode($json);
		 
		Header("Content-Disposition: attachment; filename=filekonfigurasi".date('Ymd').".txt");

		print($re);
		 /*$a=base64_decode($re);
		 $b=json_decode($a);
		 echo $b->phpinfo;*/


		exit(0);

		ob_clean();

	}

	public function key_activation(){
		
		ob_start();
		system('ipconfig /all'); //Execute external program to display output
		$mycom=ob_get_contents(); // Capture the output into a variable
		ob_clean(); // Clean (erase) the output buffer
		$findme = "Physical";
		$pmac = strpos($mycom, $findme); // Find the position of Physical text
		$mac=substr($mycom,($pmac+36),17); // Get Physical Address
		
		
		
		$prerealpassword=$mac.':'.gethostname();
		$realpassword=crypt($prerealpassword,KEY_SALT);

		
		$con=array();
		$con['setting']='key_activation';
		$pengaturan=$this->m_smartq->get_setting($con);
	 	$key=$pengaturan[0]->value;		
		$hashed_password = $key;

		if (hash_equals($hashed_password, $realpassword)) {
		   echo "Password verified!";
		}else{
		   echo 'activation key salah /tidak ditemukan, silahkan download file ini dan hubungi developer untuk mendapatkan kunci aktivasi<br>';
			echo 'a <a href="'.base_url().'administrator/getprekey'.'" > klik untuk download</a>';
		}
		die();

		


	}
	public function gen_activation(){
		
		
		$mac=base64_decode($this->input->get('mac'));
		$host=base64_decode($this->input->get('host'));
		$key=base64_decode($this->input->get('key'));
		if($mac && $host &&$key){
			$prerealpassword=$mac.':'.gethostname();
			$realpassword=crypt($prerealpassword,$key);
			echo $realpassword;
		}else{
			echo 'no input found';
		}
		
		die();

		


	}
	public function filereader(){

	}


	public function laporan_jam(){

		

		$hari=array('Senin','Selasa','Rabu','Kamis','Jum\'at','Sabtu','Minggu');
		$bulan=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','Nopember','Desember');
		$currentdate=$hari[date('N') - 1];
		$b=(int)date('m') - 1;
		
		$currentmonth=$bulan[$b];
		 
		$url=base_url().'administrator/laporan_jam?';
		$data=array();
		$date='';
		$con=array();

		if($this->input->get('tanggal')){
			$request=urldecode($this->input->get('tanggal'));			
			$t=explode('/',$request);

			if(count($t)==3){
				$con['date(queue_start)']=$t[2].'-'.$t[1].'-'.$t[0];
				$date=$t[0].' '.$bulan[($t[1] - 1 )].' '.$t[2];
				$url.='tanggal='.$this->input->get('tanggal').'&set=download';
			}
			else{
				$con['date(queue_start)']=date('Y-m-d');	
				$date=date('d ').$currentmonth.date(' Y');
				$url.='tanggal='.date('d-m-Y').'&set=download';
			}
			

		}
		else{
			$date=date('d ').$currentmonth.date(' Y');
			$con['date(queue_start)']=date('Y-m-d');
			$url.='tanggal='.date('d/m/Y').'&set=download';
		}

		

		$data['url']=$url;
		$data['title']='Laporan Pelayanan Harian - '.$date;
		$data['laporan']=$this->m_smartq->get_laporan_jam($con);

		if($this->input->get('set')=='download'){

			
			$content='';
			$content .= $data['title'].',';
			$content .= "\n";
			// set column title
			$content .= 'Jam pelayanan, Jumlah Kunjungan, Dilayani, Tak Dilayani, Waktu Tunggu (avg), Waktu Pelayanan (avg), Total Waktu (avg)';
			$content .= "\n";

			foreach ($data['laporan'] as $laporan) {
				$content.= $laporan->jam_pj.':00 ,';
				$content.= $laporan->total.' ,';
				$content.= $laporan->dilayani.' ,';
				$content.= $laporan->tidak_dilayani.' ,';
				$content.= $laporan->average_waiting.' ,';
				$content.= $laporan->average_service.' ,';
				$content.= $laporan->average_total_service.',';
				$content .= "\n";
			}
			$data['title'] = preg_replace('/\s+/', '_', $data['title']);
			ob_end_clean();
			Header("Content-Disposition: attachment; filename=".$data['title'].".csv");
			print $content;
			exit();

		}


		$this->admintemplate->load('v_laporan',$data);
	}

	public function laporan_hari(){

		

		$hari=array('Senin','Selasa','Rabu','Kamis','Jum\'at','Sabtu','Minggu');
		$bulan=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','Nopember','Desember');
		$currentdate=$hari[date('N') - 1];
		$b=(int)date('m') - 1;
		
		$currentmonth=$bulan[$b];
		 
		$url=base_url().'administrator/laporan_hari?';
		$data=array();
		$date='';
		$con=array();
		if($this->input->get('bulan')){
			$request=urldecode($this->input->get('bulan'));
			$t=explode('/',$request);

			if(count($t)==3){
				$con['date_format( queue_start, \'%Y-%m\' )=']=$t[2].'-'.$t[1];
				$date=$bulan[($t[1] - 1 )].' '.$t[2];
				$url.='tanggal='.$this->input->get('tanggal').'&set=download';
			}
			else{
				$con['date_format( queue_start, \'%Y-%m\' )=']=date('Y-m');
				$date=$currentmonth.date(' Y');
				$url.='tanggal='.date('m/Y').'&set=download';
			}
			

		}
		else{
			$date=$currentmonth.date(' Y');
			$con['date_format( queue_start, \'%Y-%m\' )=']=date('Y-m');
			$url.='bulan='.date('m/Y').'&set=download';
		}

		

		$data['url']=$url;
		$data['title']='Laporan Pelayanan Bulanan - '.$date;
		$data['laporan']=$this->m_smartq->get_laporan_hari($con);

		if($this->input->get('set')=='download'){
			

			$content='';
			$content .= $data['title'].',';
			$content .= "\n";
			// set column title
			$content .= 'Tanggal pelayanan, Jumlah Kunjungan, Dilayani, Tak Dilayani, Waktu Tunggu (avg), Waktu Pelayanan (avg), Total Waktu (avg)';
			$content .= "\n";

			foreach ($data['laporan'] as $laporan) {
				$newDate = date("d-m-Y", strtotime($laporan->tanggal_pj));
				$content.= $newDate;
				$content.= $laporan->total.' ,';
				$content.= $laporan->dilayani.' ,';
				$content.= $laporan->tidak_dilayani.' ,';
				$content.= $laporan->average_waiting.' ,';
				$content.= $laporan->average_service.' ,';
				$content.= $laporan->average_total_service.',';
				$content .= "\n";
			}

			 $data['title'] = preg_replace('/\s+/', '_', $data['title']);
			ob_end_clean();
			Header("Content-Disposition: attachment; filename=".$data['title'].".csv");
			print $content;
			exit();

		}


		$this->admintemplate->load('v_laporan_hari',$data);
	}
	public function laporan_bulan(){

		

		$hari=array('Senin','Selasa','Rabu','Kamis','Jum\'at','Sabtu','Minggu');
		$bulan=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','Nopember','Desember');
		$b=(int)date('m') - 1;
		$currentdate=$hari[date('N') - 1];
		
		
		$currentmonth=$bulan[$b];
		 
		$url=base_url().'administrator/laporan_bulan?';
		$data=array();
		$date='';
		$con=array();
		if($this->input->get('tahun')){
			$request=urldecode($this->input->get('tahun'));
			

			if(is_numeric($request)){
				$con['date_format( queue_start, \'%Y\' )=']=$request;
				$date=$request;
				$url.='tahun='.$this->input->get('tahun').'&set=download';
			}
			else{
				$con['date_format( queue_start, \'%Y\' )=']=date('Y');
				$date=date(' Y');
				$url.='tahun='.date('Y').'&set=download';
			}
			

		}
		else{
			$date=date(' Y');
			$con['date_format( queue_start, \'%Y\' )=']=date('Y');
			$url.='bulan='.date('Y').'&set=download';
		}

		

		$data['url']=$url;
		$data['title']='Laporan Pelayanan Tahunan - '.$date;
		$data['laporan']=$this->m_smartq->get_laporan_bulan($con);
		$data['bulan']=$bulan;

		if($this->input->get('set')=='download'){

			$content='';
			$content .= $data['title'].',';
			$content .= "\n";
			// set column title
			$content .= 'Tanggal pelayanan, Jumlah Kunjungan, Dilayani, Tak Dilayani, Waktu Tunggu (avg), Waktu Pelayanan (avg), Total Waktu (avg)';
			$content .= "\n";

			foreach ($data['laporan'] as $laporan) {
				
				$content.= $bulan[((int)$laporan->bulan)-1].' ,';
				$content.= $laporan->total.' ,';
				$content.= $laporan->dilayani.' ,';
				$content.= $laporan->tidak_dilayani.' ,';
				$content.= $laporan->average_waiting.' ,';
				$content.= $laporan->average_service.' ,';
				$content.= $laporan->average_total_service.',';
				$content .= "\n";
			}

			$data['title'] = preg_replace('/\s+/', '_', $data['title']); 
			ob_end_clean();
			Header("Content-Disposition: attachment; filename=".$data['title'].".csv");
			print $content;
			exit();

		}


		$this->admintemplate->load('v_laporan_bulan',$data);
	}

	public function test(){
		$this->admintemplate->load('form_sample');
	}
	public function check_activation(){
		
		 

		ob_start();
		system('ipconfig /all'); //Execute external program to display output
		$mycom=ob_get_contents(); // Capture the output into a variable
		ob_clean(); // Clean (erase) the output buffer
		$findme = "Physical";
		$pmac = strpos($mycom, $findme); // Find the position of Physical text
		$mac=substr($mycom,($pmac+36),17); // Get Physical Address
		
		
		
		$prerealpassword=$mac.':'.gethostname();
		$realpassword=crypt($prerealpassword,KEY_SALT);		
		$con=array();
		$con['setting']='key_activation';
		$pengaturan=$this->m_smartq->get_setting($con);
	 	$key=$pengaturan[0]->value;		
		$hashed_password = $key;

		if (hash_equals($hashed_password, $realpassword)) {
		   return true;
		}else{
			//stop queue if code is wrong
		  return false;
		}
	}

	public function stat_hari(){

		

		$hari=array('Senin','Selasa','Rabu','Kamis','Jum\'at','Sabtu','Minggu');
		$bulan=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','Nopember','Desember');
		$currentdate=$hari[date('N') - 1];
		$b=(int)date('m') - 1;
		
		$currentmonth=$bulan[$b];
		 
		$url=base_url().'administrator/stat_bulan?';
		$data=array();
		$data['bulan']=$bulan;
		$data['hari']=$hari;
		$date='';
		$con=array();
		


		if($this->input->get('tanggal')){
	
			$request=urldecode($this->input->get('tanggal'));
			$t=explode('/',$request);
			

			if(count($t)==3){
				$con['date_format( queue_start, \'%Y-%m-%d\' )=']=$t[2].'-'.$t[1].'-'.$t[0];
				$date=$t[0].' '.$bulan[($t[1]-1 )].' '.$t[1];
				$url.='bulan='.$this->input->get('tanggal').'&set=download';
			}
			else{
				$con['date_format( queue_start, \'%Y-%m-%d\' )=']=date('Y-m-d');
				$date=date('d ').$currentmonth.date(' Y');
				$url.='bulan='.date('d/m/Y').'&set=download';
			}
			

		}
		else{
			$date=date('d ').$currentmonth.date(' Y');
			$con['date_format( queue_start, \'%Y-%m-%d\' )=']=date('Y-m-d');
			$url.='bulan='.date('d/m/Y').'&set=download';
		}

		$data['layanan']=$this->m_smartq->get_used_layanan();

		
		
		$data['title']='Laporan Pelayanan Bulanan - '.$date;
		 

		$data['url']=$url;
		 
		$data['laporan']=$this->m_smartq->get_laporan_hari_by_pelayanan($con);

		if($this->input->get('set')=='download'){
			

			$content='';
			$content .= $data['title'].',';
			$content .= "\n";
			// set column title
			$content .= 'Tanggal pelayanan, Layanan, Jumlah Kunjungan, Dilayani, Tak Dilayani, Waktu Tunggu (avg), Waktu Pelayanan (avg), Total Waktu (avg)';
			$content .= "\n";

			foreach ($data['laporan'] as $laporan) {
				
				$newDate = date("d-m-Y", strtotime($laporan->tanggal_pj));
				$content.= $newDate.' ,';
				$content.= $laporan->nama_layanan.' ,';
				$content.= $laporan->total.' ,';
				$content.= $laporan->dilayani.' ,';
				$content.= $laporan->tidak_dilayani.' ,';
				$content.= $laporan->average_waiting.' ,';
				$content.= $laporan->average_service.' ,';
				$content.= $laporan->average_total_service.',';
				$content .= "\n";
			}

			 $data['title'] = preg_replace('/\s+/', '_', $data['title']);
			ob_end_clean();
			Header("Content-Disposition: attachment; filename=".$data['title'].".csv");
			print $content;
			exit();

		}

		
		$this->admintemplate->load('v_laporan_hari_layanan',$data);
	}
	public function stat_bulan(){

		

		$hari=array('Senin','Selasa','Rabu','Kamis','Jum\'at','Sabtu','Minggu');
		$bulan=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','Nopember','Desember');
		$currentdate=$hari[date('N') - 1];
		$b=(int)date('m') - 1;
		
		$currentmonth=$bulan[$b];
		 
		$url=base_url().'administrator/stat_bulan?';
		$data=array();
		$data['bulan']=$bulan;
		$data['hari']=$hari;
		$date='';
		$con=array();
		


		if($this->input->get('bulan')){
	
			$request=urldecode($this->input->get('bulan'));
			$t=explode('/',$request);
			

			if(count($t)==2){
				$con['date_format( queue_start, \'%Y-%m\' )=']=$t[1].'-'.$t[0];
				$date=$bulan[($t[0]-1 )].' '.$t[1];
				$url.='bulan='.$this->input->get('tanggal').'&set=download';
			}
			else{
				$con['date_format( queue_start, \'%Y-%m\' )=']=date('Y-m');
				$date=$currentmonth.date(' Y');
				$url.='bulan='.date('m/Y').'&set=download';
			}
			

		}
		else{
			$date=$currentmonth.date(' Y');
			$con['date_format( queue_start, \'%Y-%m\' )=']=date('Y-m');
			$url.='bulan='.date('m/Y').'&set=download';
		}

		$data['layanan']=$this->m_smartq->get_used_layanan();

		
		
		$data['title']='Laporan Pelayanan Bulanan - '.$date;
		 

		$data['url']=$url;
		 
		$data['laporan']=$this->m_smartq->get_laporan_bulan_by_pelayanan($con);

		if($this->input->get('set')=='download'){
			

			$content='';
			$content .= $data['title'].',';
			$content .= "\n";
			// set column title
			$content .= 'Tanggal pelayanan, Layanan, Jumlah Kunjungan, Dilayani, Tak Dilayani, Waktu Tunggu (avg), Waktu Pelayanan (avg), Total Waktu (avg)';
			$content .= "\n";

			foreach ($data['laporan'] as $laporan) {
				$newDate = date("d-m-Y", strtotime($laporan->tanggal_pj));
				$content.= $newDate.' ,';
				$content.= $laporan->nama_layanan.' ,';
				$content.= $laporan->total.' ,';
				$content.= $laporan->dilayani.' ,';
				$content.= $laporan->tidak_dilayani.' ,';
				$content.= $laporan->average_waiting.' ,';
				$content.= $laporan->average_service.' ,';
				$content.= $laporan->average_total_service.',';
				$content .= "\n";
			}

			 $data['title'] = preg_replace('/\s+/', '_', $data['title']);
			ob_end_clean();
			Header("Content-Disposition: attachment; filename=".$data['title'].".csv");
			print $content;
			exit();

		}

		
		$this->admintemplate->load('v_laporan_bulan_layanan',$data);
	}
	public function check_queue(){
		$current_date=date('Y-m-d');


		$current_counter=$this->m_smartq->get_queue();

		$check_status=0;
		$lastupdate=null;
		$a=null;
		foreach ($current_counter as $cc) {

			if($lastupdate==null){
				$lastupdate=$cc->updated;
				$a=$cc;
			}else{
				if(strtotime($lastupdate)<strtotime($cc->updated)){
					$lastupdate=$cc->updated;
					$a=$cc;
				}
			}
			if($cc->counter >1){
				$check_status=1;
			}

		}
		$last_date=date('Y-m-d',strtotime($lastupdate));

		$result['status']=0;
		$result['msg']='';
		$con=array();
		$con['setting']='queue_mode';
		$queue_mode=$this->m_smartq->get_setting($con)[0]->value;

		if($current_date!=$last_date && $check_status==1 && $queue_mode==1){			
			$result['status']=1;
			$result['msg']='perbedaan tanggal antrian ditemukan';
			$this->reset_queue();
		}

		return $result;

	}
	public function grafik_layanan(){

		

		$stats=$this->m_smartq->get_stat_total();
		$tahun=date('Y');
		if($this->input->get('tahun')){
			$con['YEAR(queue_start)=']=$this->input->get('tahun');	
			$tahun=$this->input->get('tahun');	
		}		
		else	
		$con['YEAR(queue_start)=']=date('Y');
		$stats_total=$this->m_smartq->get_stat_total_layanan($con);
		$stats=$this->m_smartq->get_stat_total($con);
		$info=array();
		$tlayanan=array();
		$tlayanan[0]=$stats_total[0]->layanan_1;
		$tlayanan[1]=$stats_total[0]->layanan_2;
		$tlayanan[2]=$stats_total[0]->layanan_3;
		$tlayanan[3]=$stats_total[0]->layanan_4;
		$tlayanan[4]=$stats_total[0]->layanan_5;
		$tlayanan[5]=$stats_total[0]->layanan_6;
		$tlayanan[6]=$stats_total[0]->layanan_7;
		$tlayanan[7]=$stats_total[0]->layanan_8;
		$data['total_layanan']=$stats_total;
		$data['tlayanan']=$tlayanan;
		$color=array('#4e73df', '#1cc88a', '#36b9cc','#da4b05','#cb0dc2','#138041','#cbb6f3','#64d61a');
		$data['color']=$color;

		foreach ($stats as $total) {
			$info['total']=$total->total;
			$info['total_year']=$total->total_year;
			$info['total_month']=$total->total_month;
			$info['total_day']=$total->total_day;
			$info['day']=$total->day;
		}
		$data['layanan']=$this->m_smartq->get_layanan();
		$data['info']=$info;
		$current_year_stat=$this->m_smartq->get_total_year($con);
		$year_stats=array();

		foreach ($current_year_stat as $cys) {
			$year_stats['januari']=$cys->januari;
			$year_stats['februari']=$cys->februari;
			$year_stats['maret']=$cys->maret;
			$year_stats['april']=$cys->april;
			$year_stats['mei']=$cys->mei;
			$year_stats['juni']=$cys->juni;
			$year_stats['juli']=$cys->juli;
			$year_stats['agustus']=$cys->agustus;
			$year_stats['september']=$cys->september;
			$year_stats['oktober']=$cys->oktober;
			$year_stats['nopember']=$cys->nopember;
			$year_stats['desember']=$cys->desember;
			 
		}
		$data['year_stats']=$year_stats;
		$data['title']='Grafik Layanan 2019';
		$data['url']=base_url();
		$data['tahun']=$tahun;

		
		$this->admintemplate->load('a_grafik_layanan',$data);
	}
	public function grafik_layanan_bulan(){

		
		$m=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','Nopember','Desember');
		$stats=$this->m_smartq->get_stat_total();
		$bulan=date('m');
		$tahun=date('Y');
		if($this->input->get('tanggal')){
			$tanggal=explode('-', $this->input->get('tanggal'));
			$bulan=$tanggal[0];
			$tahun=$tanggal[1];
			$con['MONTH(queue_start)=']=$bulan;
			$con['YEAR(queue_start)=']=$tahun;
		}
		else{
			$con['MONTH(queue_start)=']=$bulan;
			$con['YEAR(queue_start)=']=$tahun;
		}	
		
		$stats_total=$this->m_smartq->get_stat_total_layanan($con);
		$stats=$this->m_smartq->get_stat_total($con);
		$info=array();
		$tlayanan=array();
		$tlayanan[0]=$stats_total[0]->layanan_1;
		$tlayanan[1]=$stats_total[0]->layanan_2;
		$tlayanan[2]=$stats_total[0]->layanan_3;
		$tlayanan[3]=$stats_total[0]->layanan_4;
		$tlayanan[4]=$stats_total[0]->layanan_5;
		$tlayanan[5]=$stats_total[0]->layanan_6;
		$tlayanan[6]=$stats_total[0]->layanan_7;
		$tlayanan[7]=$stats_total[0]->layanan_8;
		$data['total_layanan']=$stats_total;
		$data['tlayanan']=$tlayanan;
		$color=array('#4e73df', '#1cc88a', '#36b9cc','#da4b05','#cb0dc2','#138041','#cbb6f3','#64d61a');
		$data['color']=$color;

		foreach ($stats as $total) {
			$info['total']=$total->total;
			$info['total_year']=$total->total_year;
			$info['total_month']=$total->total_month;
			$info['total_day']=$total->total_day;
			$info['day']=$total->day;
		}
		$data['layanan']=$this->m_smartq->get_layanan();
		$data['info']=$info;
		$current_month_stat=$this->m_smartq->get_total_month($con);
		
		
		$data['month_stat']=$current_month_stat;
		$data['title']='Grafik Layanan '.$m[(int)$bulan-1].' '.$tahun;
		$data['url']=base_url();
		$data['tahun']=$tahun;
		$data['bulan']=$m[(int)$bulan-1];

		
		$this->admintemplate->load('a_grafik_layanan_bulan',$data);
	}
	public function grafik_layanan_loket(){

		 

		$stats=$this->m_smartq->get_stat_total();
		$tahun=date('Y');
		if($this->input->get('tahun')){
			$con['YEAR(queue_start)=']=$this->input->get('tahun');	
			$tahun=$this->input->get('tahun');	
		}		
		else	
		$con['YEAR(queue_start)=']=date('Y');
		$stats_total=$this->m_smartq->get_stat_total_layanan($con);
		$stats=$this->m_smartq->get_stat_total($con);
		$info=array();
		$tlayanan=array();
		$tlayanan[0]=$stats_total[0]->layanan_1;
		$tlayanan[1]=$stats_total[0]->layanan_2;
		$tlayanan[2]=$stats_total[0]->layanan_3;
		$tlayanan[3]=$stats_total[0]->layanan_4;
		$tlayanan[4]=$stats_total[0]->layanan_5;
		$tlayanan[5]=$stats_total[0]->layanan_6;
		$tlayanan[6]=$stats_total[0]->layanan_7;
		$tlayanan[7]=$stats_total[0]->layanan_8;
		$data['total_layanan']=$stats_total;
		$data['tlayanan']=$tlayanan;
		$color=array('#4e73df', '#1cc88a', '#36b9cc','#da4b05','#cb0dc2','#138041','#cbb6f3','#64d61a');
		$data['color']=$color;

		foreach ($stats as $total) {
			$info['total']=$total->total;
			$info['total_year']=$total->total_year;
			$info['total_month']=$total->total_month;
			$info['total_day']=$total->total_day;
			$info['day']=$total->day;
		}
		$data['layanan']=$this->m_smartq->get_layanan();
		$data['info']=$info;
		$status_layanan=array();
		foreach ($data['layanan'] as $lay) {
			$c=array();
			$c['layanan_id']=$lay->id_layanan;
			$c['YEAR(queue_start)=']=$con['YEAR(queue_start)='];
			$st=array();
			$st=$this->m_smartq->get_total_year($c);
			$temp=array();
			foreach ($st as $cys) {
				$temp['januari']=$cys->januari;
				$temp['februari']=$cys->februari;
				$temp['maret']=$cys->maret;
				$temp['april']=$cys->april;
				$temp['mei']=$cys->mei;
				$temp['juni']=$cys->juni;
				$temp['juli']=$cys->juli;
				$temp['agustus']=$cys->agustus;
				$temp['september']=$cys->september;
				$temp['oktober']=$cys->oktober;
				$temp['nopember']=$cys->nopember;
				$temp['desember']=$cys->desember;

			}
			$status_layanan[$lay->id_layanan]=$temp;
		}


		$data['year_stats']=$status_layanan;
		$data['title']='Grafik Layanan 2019';
		$data['url']=base_url();
		$data['tahun']=$tahun;
		$check=array();
		$count=0;
		foreach ($data['layanan'] as $val) {
			$check[$val->id_layanan]=0;
			
			if($this->input->get('check-'.$val->id_layanan)){
				$count++;
				$check[$val->id_layanan]=$this->input->get('check-'.$val->id_layanan);
			}
		}
		if($count==0){
			foreach ($check as $key => $value) {
				 $check[$key]=1;
			}
		}
		$data['check']=$check;
		
		$this->admintemplate->load('a_grafik_layanan_loket',$data);
	}
	public function grafik_layanan_loket_bulan(){

		
		$m=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','Nopember','Desember');
		$stats=$this->m_smartq->get_stat_total();
		$bulan=date('m');
		$tahun=date('Y');
		if($this->input->get('tanggal')){
			$tanggal=explode('-', $this->input->get('tanggal'));
			$bulan=$tanggal[0];
			$tahun=$tanggal[1];
			$con['MONTH(queue_start)=']=$bulan;
			$con['YEAR(queue_start)=']=$tahun;
		}
		else{
			$con['MONTH(queue_start)=']=$bulan;
			$con['YEAR(queue_start)=']=$tahun;
		}	
		
		$stats_total=$this->m_smartq->get_stat_total_layanan($con);
		$stats=$this->m_smartq->get_stat_total($con);
		$info=array();
		$tlayanan=array();
		$tlayanan[0]=$stats_total[0]->layanan_1;
		$tlayanan[1]=$stats_total[0]->layanan_2;
		$tlayanan[2]=$stats_total[0]->layanan_3;
		$tlayanan[3]=$stats_total[0]->layanan_4;
		$tlayanan[4]=$stats_total[0]->layanan_5;
		$tlayanan[5]=$stats_total[0]->layanan_6;
		$tlayanan[6]=$stats_total[0]->layanan_7;
		$tlayanan[7]=$stats_total[0]->layanan_8;
		$data['total_layanan']=$stats_total;
		$data['tlayanan']=$tlayanan;
		$color=array('#4e73df', '#1cc88a', '#36b9cc','#da4b05','#cb0dc2','#138041','#cbb6f3','#64d61a');
		$data['color']=$color;

		foreach ($stats as $total) {
			$info['total']=$total->total;
			$info['total_year']=$total->total_year;
			$info['total_month']=$total->total_month;
			$info['total_day']=$total->total_day;
			$info['day']=$total->day;
		}

		$data['layanan']=$this->m_smartq->get_layanan();
		$data['info']=$info;
		 $status_layanan=array();
		foreach ($data['layanan'] as $lay) {
			$c=array();
			$c['layanan_id']=$lay->id_layanan;
			$c['YEAR(queue_start)=']=$con['YEAR(queue_start)='];
			$c['MONTH(queue_start)=']=$con['MONTH(queue_start)='];
			$st=array();
			$st=$this->m_smartq->get_total_month($c);
			 
			$status_layanan[$lay->id_layanan]=$st;
		}

		
		$data['month_stat']=$status_layanan;
		$data['title']='Grafik Layanan '.$m[(int)$bulan-1].' '.$tahun;
		$data['url']=base_url();
		$data['tahun']=$tahun;
		$data['bulan']=$m[(int)$bulan-1];
		$check=array();
		$count=0;
		foreach ($data['layanan'] as $val) {
			$check[$val->id_layanan]=0;
			
			if($this->input->get('check-'.$val->id_layanan)){
				$count++;
				$check[$val->id_layanan]=$this->input->get('check-'.$val->id_layanan);
			}
		}
		if($count==0){
			foreach ($check as $key => $value) {
				 $check[$key]=1;
			}
		}
		$data['check']=$check;

		
		$this->admintemplate->load('a_grafik_layanan_loket_bulan',$data);
	}

	public function reset_data(){

		$password=$this->input->post('password');

		if($password){
		if($password==PSRESET)
			{
				$this->m_smartq->reset_data();
				$this->session->set_flashdata('pesan','DATA SERVER BERHASIL DIKEMBALIKAN');
				redirect(base_url().'administrator/reset_data');
			}
			else{
				$this->session->set_flashdata('pesan','Password Tidak Sesuai, silahkan ulangi');
				redirect(base_url().'administrator/reset_data');
			}
		}
		$data=array();
		$this->admintemplate->load('a_reset',$data);	
	}
	public function hapus_sampel(){

		$password=$this->input->post('password');

		if($password){
		if($password==PSRESET)
			{
				 $this->m_smartq->delete_sampel();
				$this->session->set_flashdata('pesan','DATA SAMPEL  BERHASIL DIHAPUS');
				redirect(base_url().'administrator/hapus_sampel');
			}
			else{
				$this->session->set_flashdata('pesan','Password Tidak Sesuai, silahkan ulangi');
				redirect(base_url().'administrator/hapus_sampel');
			}
		}
		$data=array();
		$this->admintemplate->load('a_reset',$data);	
	}
	public function hapus_data(){

		$password=$this->input->post('password');

		if($password){
		if($password==PSRESET)
			{
				$this->m_smartq->delete_all_tiket();
				$this->session->set_flashdata('pesan','DATA  BERHASIL DIHAPUS');
				redirect(base_url().'administrator/hapus_data');
			}
			else{
				$this->session->set_flashdata('pesan','Password Tidak Sesuai, silahkan ulangi');
				redirect(base_url().'administrator/hapus_data');
			}
		}
		$data=array();
		$this->admintemplate->load('a_reset',$data);	
	}

}