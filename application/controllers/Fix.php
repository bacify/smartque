<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fix extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

		/*DEFAULT USER PASS*/
		
	 

	function __construct(){ 
        parent::__construct();
        $this->load->library('Template');
        $this->load->library('maincore');  
        $this->load->model('m_smartq');	

    	/*disable Cache*/
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		 

	

        
    }
    public function get_next_queue(){
		//fetch_user.php
			$result=array();
 				if($this->session->userdata('loket_id')==null)
 				{
 					echo '0';
 					die();
 				}

 				//check calling status
 				/*	$call=$this->m_smartq->get_call_status();
 					$call_status=$call[0]->calling;

 					if($call_status==1){
 						echo '-1';
 						die();
 					}
				*/


 				//get loket statys
 				$condition=array();
 				$condition['loket_id']=$this->session->userdata('loket_id');
 				$loket_status=$this->m_smartq->get_loket_status($condition);
 				$last_tiket=$loket_status[0]->counter;

 				//get loket detail
 				$condition=array();
 				$condition['loket_id']=$this->session->userdata('loket_id');
 				$loket=$this->m_smartq->get_loket_alias($condition);

 				//get tiket status
 				$condition=array();
 				$condition['layanan_id']=$loket[0]->layanan_id;
 				$condition['no_tiket']=$last_tiket;
 				$tiket_status=$this->m_smartq->get_tiket_queue($condition);

 				$row=count($tiket_status)-1;
 				

 				if(count($tiket_status)>0){
	 				if($tiket_status[$row]->queue_status==1){
	 					// set last tiket to end service
	 					$condition=array();
	 					$condition['idtiket']=$tiket_status[$row]->idtiket;
	 					$this->m_smartq->endprocess_tiket($condition);
	 				}
 				}
 				
 				//GET next Queue
				$condition=array();
 				$condition['layanan_id']=$loket[0]->layanan_id;
 				$condition['queue_status']=0;
 				$total_counter=$this->m_smartq->get_tiket_queue($condition);
 				

 				//get antrian sekarnag
 				if(count($total_counter)>0){
 					
 					$condition=array();
 					$condition['loket_id']=$this->session->userdata('loket_id');
 					$val=array();
 					$val['counter']=$total_counter[0]->no_tiket;
 					$this->m_smartq->update_loket($condition,$val);




 					$con=array();
 					$val2['loket_id']=$this->session->userdata('loket_id'); 					
 					$val2['counter']=$total_counter[0]->no_tiket;
 					$val2['status']=1; 					
 					//$this->m_smartq->update_call_status($con,$val2);
 					echo $val['counter'];
 				}else{
 					echo '0';
 					
 				}
 				
 				

 
				
	}
    public function call_queue(){
		//fetch_user.php
			$result=array();
 				if($this->session->userdata('loket_id')==null)
 				{
 					echo '0';
 					die();
 				}	


 				//check calling status
 					$call=$this->m_smartq->get_call_status();
 					$call_status=$call[0]->calling;

 					if($call_status==1){
 						echo '-1';
 						die();
 					}


 				//get loket detail
 				$condition=array();
 				$condition['loket_id']=$this->session->userdata('loket_id');
 				$loket=$this->m_smartq->get_loket_status($condition);
 				$no=$loket[0]->counter;
 				 
 				 	$con=array();
 					$val2['loket_id']=$this->session->userdata('loket_id'); 					
 					$val2['counter']=$loket[0]->counter;
 					$val2['status']=1;
 					$this->m_smartq->update_call_status($con,$val2);
 					

 
				
	}


    public function start_calling(){
			$con=array();
		    $val['calling']=1;
		    $status=$this->m_smartq->update_call_status($con,$val);
		if($status){
			$msg['status']=1;
			$msg['msg']='status suara aktif';
		}else{
			$msg['status']=0;
			$msg['msg']='status suara gagal';
		}

		echo json_encode($msg);
	}
	public function end_calling(){
			$con=array();
		    $val['calling']=0;
		    $status=$this->m_smartq->update_call_status($con,$val);
		if($status){
			$msg['status']=1;
			$msg['msg']='status suara nonaktif';
		}else{
			$msg['status']=0;
			$msg['msg']='status suara gagal';
		}

		echo json_encode($msg);
	}

	public function login_loket(){
		 $email=$this->session->userdata('email');
		 $password=$this->input->post('passwordloket');

		 $data=array('email'=>$email,
		 			 'password'=>$password
		 			 );
		 $return=array();

		 if($this->login_check($data)){
		 	$sessiondata['loket_id']=$this->input->post('noloket');
		 	
		 	/*insert login detail */
		 	$logindetail=array();
		 	$logindetail['loket_id']=$this->input->post('noloket');
		 	$sessionid=$this->m_smartq->insert_login_details($logindetail);
		 	$sessiondata['session_loketid']=$sessionid;
		 	$loket=array();
		 	$loket=$this->m_smartq->get_loket_alias($logindetail);
		 	if(count($loket)>0){
		 		$sessiondata['dataloket']=$loket[0];
		 		$c['id_layanan']=$loket[0]->layanan_id;
		 		$layanan=array();
		 		$layanan=$this->m_smartq->get_layanan($c);
		 		if(count($layanan)>0)
		 			$sessiondata['datalayanan']=$layanan[0];
		 		else
		 			$sessiondata['datalayanan']=array('id_layanan'=>$loket[0]->layanan_id,'nama_layanan'=>'unknown service',
		 											   'tiket_alias'=>'unknown alias');
		 	}
		 	$this->session->set_userdata($sessiondata);
		 	$return['status']=1;
		 	$return['url']=base_url().'home/panel_loket';

		 }
		 else{
			$return['status']=0;
		 	$return['url']=0;
		 }

		 echo json_encode($return);
	}
	public function login_check($value){
		 
		$setting=$this->m_smartq->get_setting();
		$status=0;
		 
		if(isset($value['email']) && isset ($value['password']))
		foreach ($setting as  $v) {
			if($v->setting=='email'){
				if($v->value==$value['email']){
					$status++;
					 
				}
			}
			if($v->setting=='password'){
				 
				if($v->value==$value['password']){
					$status++;
					 
				}
			}
		}
 		 
		if($status==2){
			return true;
		}else{
			/*check with defuault password*/
			if(strtolower($value['email'])==strtolower(MAIN_EMAIL)&& $value['password']==DEFAULT_PASSWORD)
				return true;
			else
				return false;
		}
	}

	public function ping(){
		echo '1';
	}
	public function call_passed_queue(){
		//fetch_user.php
			$result=array();
 				if($this->session->userdata('loket_id')==null)
 				{
 					echo '0';
 					die();
 				}	

 				//check calling status
 					$call=$this->m_smartq->get_call_status();
 					$call_status=$call[0]->calling;

 					if($call_status==1){
 						echo '-1';
 						die();
 					}

 					
 				//get loket detail
 				$condition=array();
 				$condition['loket_id']=$this->session->userdata('loket_id');
 				
 				$no=$this->input->get('notiket');
 				 
 				 	$con=array();
 					$val2['loket_id']=$this->session->userdata('loket_id'); 					
 					$val2['counter']=$no;
 					$val2['status']=1;
 					$this->m_smartq->update_call_status($con,$val2);
 					

 
				
	}

}
