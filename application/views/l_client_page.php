 <?php 
 date_default_timezone_set('Asia/Jakarta');

 ?>
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Smart Queue - Dashboard</title>

  <!-- load bootstrap css -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" >

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url();?>assets/fontawesome/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">



  

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url();?>assets/js/jquery-3.4.0.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>


  

<!-- Custom styles for this template-->
  <!-- <link href="<?php echo base_url();?>assets/css/display.css" rel="stylesheet"> -->

  <style type="text/css">
  body{
  	background-color: #615959;
  }
 .logo{
 	width: 80px;

 }
 .display-10 {
    font-size: 10rem;
    font-weight: 300;
    line-height: 1.2;
}
.label-logo{
	display: none;
}

@media screen and (max-width: 768px) {
  .label-text {    
    display: none;
  }
  .label-logo{
  	display: inline;
  }
}


  </style>






</head>
<body>

	 <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-primary">
				  <a class="navbar-brand" href="#"> <img src="<?php echo base_url();?>assets/img/sq_logo.png" class="logo img-fluid rounded"> Panel SmartQ</a>
				  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				    <span class="navbar-toggler-icon"></span>
				  </button>

				  <div class="collapse navbar-collapse" id="navbarSupportedContent">
				    <ul class="navbar-nav mr-auto">
				    <!--   <li class="nav-item active">
				        <a class="nav-link" href="#">
				          <i class="fas fa-arrow-circle-left "></i>
				           Kembali
				          <span class="sr-only">(current)</span>
				          </a>
				      </li>
				       -->
				    </ul>
				    <ul class="navbar-nav ">
				      <li class="nav-item">
				        <a class="nav-link" href="<?php echo base_url();?>home/logout_panel_loket">
				         <i class="fas fa-sign-out-alt"></i>
				            <span class="badge badge-info"></span>
				          </i>
				          
				        </a>
				      </li>
				    </ul>
				    
				  </div>
	</nav>

	<div class="container-fluid my-3">
		<div class="card">
			<div class="card-header bg-primary">
				<h3 class="badge-primary"> Layanan Administrasi - Loket <?php echo $loket.' - '.$this->session->userdata('datalayanan')->nama_layanan.' ( '.$this->session->userdata('datalayanan')->tiket_alias.' )';?></h3>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-sm-2 mb-2">
						<div class="card border-primary">
							<div class="card-body">
								<center>
									<h1 class="font-weight-bold text-total" >100</h1>
								</center>
							</div>
							<div class="card-footer bg-primary py-2">
								<center>
									<h5 class="font-weight-bold ">
										<span class="label-text badge badge-primary">Total Antrian</span>
										<span class="label-logo badge badge-primary">Total</span>
								</h5>
								</center>
							</div>
						</div>
						
					</div>
					<div class="col-sm-2 mb-2">
						<div class="card border-primary">
							<div class="card-body">
								<center>
									<h1 class="font-weight-bold text-sisa">29</h1>
								</center>
							</div>
							<div class="card-footer bg-primary py-2">
								<center>
									<h5 class="font-weight-bold ">
										<span class="label-text badge badge-primary">Sisa Antrian</span>
										<span class="label-logo badge badge-primary">Left</span>
									</h5>
								</center>
							</div>
						</div>
						
					</div>
					<div class="col-sm-2 mb-2">
						<div class="card border-primary">
							<div class="card-body">
								<center>
									<h1 class="font-weight-bold text-sekarang">70</h1>
								</center>
							</div>
							<div class="card-footer bg-primary py-2">
								<center>
									<h5 class="font-weight-bold ">
										<span class="label-text badge badge-primary">Nomor Saat Ini</span>
										<span class="label-logo badge badge-primary">Now</span>
									</h5>
								</center>
							</div>
						</div>
						
					</div>
					<div class="col-sm-2 mb-2">
						<div class="card border-primary">
							<div class="card-body">
								<center>
									<h1 class="font-weight-bold text-lanjut">71</h1>
								</center>
							</div>
							<div class="card-footer bg-primary py-2">
								<center>
									<h5 class="font-weight-bold mx-auto">
										<span class="label-text badge badge-primary">Nomor Selanjutnya</span>
										<span class="label-logo badge badge-primary">next</span>
									</h5>
								</center>
							</div>
						</div>
						
					</div>
					<div class="col-sm-2 mb-2">
						<div class="card border-danger">
							<div class="card-body">
								<center>
									<h1 class="font-weight-bold text-ondesk">70</h1>
								</center>
							</div>
							<div class="card-footer bg-danger py-2">
								<center>
									<h5 class="font-weight-bold ">
										<span class="label-text badge badge-danger">Sedang Dilayani</span>
										<span class="label-logo badge badge-danger">Desk</span>
									</h5>
								</center>
							</div>
						</div>
						
					</div>
				</div>


				
			</div>
			<!-- EOF CARD BODY -->
			<div class="card-footer">

			</div>
			<!-- EOF CARD FOOTER -->
		</div>

			<div class="row mt-2">
					<div class="col-sm-6">
						<button id="btn-next" class="btn btn-lg btn-info mb-2" type="button" value=""><i class="fas fa-forward"></i> Selanjutnya</button>
						<button id="btn-recall"class="btn btn-lg btn-info mb-2" type="button" value="" disabled><i class="fas fa-microphone"></i> Panggil</button>
						
						
					</div>
					<div class="col-sm-6 text-right">
						<button id="btn-acc" class="btn btn-lg btn-info mb-2 " type="button" value="" disabled><i class="fas fa-check-circle"></i> Terima</button>
						<button id="btn-pass" class="btn btn-lg btn-info mb-2" type="button" value="" disabled><i class="fas fa-step-forward"></i> Lewati</button>
					</div>
				</div>
			<div class="card mt-2">
					<div class="col-sm-6">
						<div class="page-header">
							<h3> Daftar no Antrian yang dilewati</h3>
						</div>
						<table class="table table-hover table-sm table-bordered">
							<thead class="thead-dark"><tr>
										<th>No</th>
										<th>No Antrian</th>
										<th>Tindakan</th>
									</tr> 
							</thead>
							<tbody id="table-passed">
								<tr>
										<td colspan="3">No Data</td>
										
								</tr>
							</tbody>
						</table>
					</div>
			</div>	
	</div>


<script type="text/javascript">
	function recall_passed(id){
    			var queryString = {'notiket' : id};
    			 $.ajax({
			   		url:"<?php echo base_url();?>fix/call_passed_queue",
			   		method:"GET",
			   		data: queryString,
				   success:function(data)
				   {
				   		if(data=='-1'){
				   			alert('panggilan suara sedang aktif');
				   		}

				   }
			  	})
    		}
    function acc_passed(id){
    			var queryString = {'idtiket' : id};
    			 $.ajax({
			   		url:"<?php echo base_url();?>home/acc_passed_queue",
			   		method:"GET",
			   		data: queryString,
				   success:function(data)
				   {
				   

				   }
			  	})
    		}

	$(document).ready(function(){

		 
		 
		 	var total,left,now,next,ondesk='-';

		 	 setInterval(function(){   
			  update_last_activity();			  
			 }, 20000);
			 
			  fetch_panel_status();			  
			  
			 

			 

			

			 function fetch_panel_status(timestamp)
			 {

			 	var queryString = {'timestamp' : timestamp};
			  $.ajax({
			   url:"<?php echo base_url();?>home/fetch_panel_status",
			   method:"GET",
			   data: queryString,
			   success:function(data){
			   	
			   	r=JSON.parse(data);

			   	if(r.status==0){
			   		var result = confirm( "Anda belum Login, Silahkan login terlebih dahulu?" );

						if ( result ) {
						    window.location.replace("<?php echo base_url();?>");
						} else {
						    throw new Error("Something went badly wrong!");
						}
			   	}
			   
			    $('.text-total').html(r.total);
			    $('.text-sisa').html(r.left);
			    $('.text-sekarang').html(r.now);
			    $('.text-lanjut').html(r.next);
			    $('.text-ondesk').html(r.ondesk);
			    total=r.total;
			    left=r.left;
			    now=r.now;
			    next=r.next;
			    ondesk=r.ondesk;
			    tbl_body='';
			    a=1;
			    $.each(r.passedqueue, function(index,value) {
			    	
			        tbl_row='<tr><td>'+(a++)+'</td>'+
			        			'<td>'+value.no_tiket+'</td>'+
			        			'<td><button class="btn btn-sm btn-info mb-2" type="button" onclick="recall_passed(\''+value.no_tiket+'\')"><i class="fas fa-microphone"></i> Panggil Ulang</button> '+
			        			    '<button class="btn btn-sm btn-info mb-2 " type="button" onclick="acc_passed(\''+value.idtiket+'\')"><i class="fas fa-check-circle"></i> Terima</button></td></tr>';
			        tbl_body+=tbl_row;

			    });
			    if(r.passedqueue.lenght==0){
			    	tbl_row='<tr><td colspan="3">No Data</td></tr>';
					tbl_body+=tbl_row;			    	
			    }				
			    $('#table-passed').html(tbl_body);

			    if(r.pelayanan==1){
			    	$("#btn-next").attr("disabled", false);
			   		$("#btn-recall").attr("disabled", true);
			   		$("#btn-pass").attr("disabled", true);
			   		$("#btn-acc").attr("disabled", true);
			    }else{
			    	
			   		
			    }

			    fetch_panel_status(r.timestamp)
			   },statusCode: {
			        500: function() {
			        	fetch_panel_status();
			          console.log("Script exhausted");
			        }
			     }
			  });
			 }

			 function update_last_activity()
			 {
			  $.ajax({
			   url:"<?php echo base_url();?>home/update_last_activity",
			   success:function(data)
			   {

			   }
			  })
			 }
 


			/*BUTTON CONTROL*/
			$("#btn-acc").click(function(){
					$("#btn-next").attr("disabled", false);
			   		$("#btn-recall").attr("disabled", true);
			   		$("#btn-pass").attr("disabled", true);
			   		$("#btn-acc").attr("disabled", true);
				$.ajax({
			   	url:"<?php echo base_url();?>home/acc_queue",
			   success:function(data)
			   {

			   	r=JSON.parse(data);
			   	if(r.status==0){
			   		alert('Antrian Gagal Diterima');
			   		$("#btn-next").attr("disabled", true);
			   		$("#btn-recall").attr("disabled", false);
			   		$("#btn-pass").attr("disabled", false);
			   		$("#btn-acc").attr("disabled", false);
			   	}else{
			   		
			   		$("#btn-next").attr("disabled", false);
			   		$("#btn-recall").attr("disabled", true);
			   		$("#btn-pass").attr("disabled", true);
			   		$("#btn-acc").attr("disabled", true);
			   		
			   		
			   	}

			   }
			  });

        	 
    		});
    		/*BUTTON CONTROL*/
			$("#btn-pass").click(function(){
					$("#btn-next").attr("disabled", false);
			   		$("#btn-recall").attr("disabled", true);
			   		$("#btn-acc").attr("disabled", true);
			   		$("#btn-pass").attr("disabled", true);
			   		$.ajax({
			   	url:"<?php echo base_url();?>home/pass_queue",
			   success:function(data)
			   {

				   	r=JSON.parse(data);
				   	if(r.status==0){
				   		alert('Antrian Gagal Diterima');
						$("#btn-next").attr("disabled", true);
				   		$("#btn-recall").attr("disabled", false);
				   		$("#btn-acc").attr("disabled", false);
				   		$("#btn-pass").attr("disabled", false);

				   	}else{
				   		
				   		$("#btn-next").attr("disabled", false);
			   			$("#btn-recall").attr("disabled", true);
			   			$("#btn-acc").attr("disabled", true);
			   			$("#btn-pass").attr("disabled", true);
				   		
				   	}

			   }
			  });


        	 
    		}); 
    		/*BUTTON CONTROL*/
			$("#btn-next").click(function(){
				$("#btn-next").attr("disabled", true);
				$("#btn-recall").attr("disabled", false);
			   	$("#btn-acc").attr("disabled", false);
			   	$("#btn-pass").attr("disabled", false);
        	  $.ajax({
			   	url:"<?php echo base_url();?>fix/get_next_queue",
			   success:function(data)
			   {
			   	if(data==0){
			   		alert('ANTRIAN SUDAH HABIS/BELUM ADA ANTRIAN');
			   		$("#btn-next").attr("disabled", false);
			   	}else if(data=='-1'){
			   		$("#btn-next").attr("disabled", false);
					$("#btn-recall").attr("disabled", true);
				   	$("#btn-acc").attr("disabled", true);
				   	$("#btn-pass").attr("disabled", true);
			   		alert('panggilan suara sedang aktif');

			   	}else{
			   		ondesk=data;
			   		now=data;
			   		
			   		
			   	}

			   }
			  });
    		}); 
    		
    		 /*BUTTON CONTROL*/
			$("#btn-recall").click(function(){
				$("#btn-recall").attr("disabled", true);
        	  $.ajax({
			   	url:"<?php echo base_url();?>fix/call_queue",
			   success:function(data)
			   {
			   		 if(data=='-1'){
			   			alert('panggilan suara sedang aktif');

			  		 }
			  		 $("#btn-recall").attr("disabled", false);

			   }
			  })
    		}); 



    		

	});


</script>
</body>
</html>




