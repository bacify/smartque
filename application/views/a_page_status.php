 <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Status Layanan</h1>

          <div class="row">
          	<div class="col-lg-6 ">
          		<div class="card shadow mb-4">
          			<div class="card-header py-3 text-status">
          				<?php if($status==1){
          					echo '<h4 class="m-0 font-weight-bold text-badge badge-primary text-center blinking">SEDANG BERJALAN</h4>';
          					}
          					else{
          						echo '<h4 class="m-0 font-weight-bold text-badge badge-danger text-center blinking">LAYANAN TIDAK AKTIF</h4>';
          					
          				}
          				?>
                  		
                	</div>
                	<ul class="list-group list-group-flush">
					    <li class="list-group-item">
					    	 <button id="btn-stop" class="btn btn-primary btn-icon-split" <?php if($status==0) echo 'disabled';?>>
			                    <span class="icon text-white-50">
			                      <i class="fas fa-power-off"></i>
			                    </span>
			                    <span class="text">Stop Layanan</span>
			                  </button>
              			</li>
					    <li class="list-group-item">
					    	<button id="btn-start" class="btn btn-success btn-icon-split" <?php if($status==1) echo 'disabled';?>>
			                    <span class="icon text-white-50">
			                      <i class="far fa-play-circle"></i>
			                    </span>
			                    <span class="text">Start Layanan</span>
			                </button>
					    </li>
					    <li class="list-group-item">
					    	<button id="btn-reset" class="btn btn-info btn-icon-split">
			                    <span class="icon text-white-50">
			                      <i class="fas fa-sync-alt"></i>
			                    </span>
			                    <span class="text">Reset Layanan</span>
			                </button>
					    </li>
					  </ul>
                	<div class="card-body">
                			<small class="text-muted"><b>*</b>pastikan kunci aktivasi sesuai untuk memastikan layanan berjalan dengan baik</small>
                	</div>
          		</div>

          	</div>
          </div>

          <script type="text/javascript">

          $(document).ready(function(){

	          	/*BUTTON CONTROL*/
				$("#btn-stop").click(function(){
					$("#btn-stop").attr("disabled", true);
	        	  $.ajax({
				   	url:"<?php echo base_url();?>administrator/stop_queue",
				   success:function(data)
				   {

				   	r=JSON.parse(data);

				   	if(r.status==1){
				   		alert(r.msg);
				   		$("#btn-start").attr("disabled", false);
				   		$('.text-status').html('<h4 class="m-0 font-weight-bold text-badge badge-danger text-center blinking">LAYANAN TIDAK AKTIF</h4>');
				   	}else{
				   		alert(r.msg);
				   		$("#btn-stop").attr("disabled", false);
				   			
				   		
				   	}

				   

				   }
				  })
	    		}); 

	    		/*BUTTON CONTROL*/
				$("#btn-start").click(function(){
					$("#btn-start").attr("disabled", true);
	        	  $.ajax({
				   	url:"<?php echo base_url();?>administrator/start_queue",
				   success:function(data)
				   {

				   	r=JSON.parse(data);

				   	if(r.status==1){
				   		alert(r.msg);
				   		$("#btn-stop").attr("disabled", false);
				   		$('.text-status').html('<h4 class="m-0 font-weight-bold text-badge badge-primary text-center blinking">SEDANG BERJALAN</h4>');
				   	}else{
				   		alert(r.msg);
				   		$("#btn-start").attr("disabled", false);
				   		
				   		
				   	}

				   	

				   }
				  })
	    		});

				$("#btn-reset").click(function(){
					$("#btn-reset").attr("disabled", true);
					
	        	  $.ajax({
				   	url:"<?php echo base_url();?>administrator/reset_queue",
				   success:function(data)
				   {

				   	r=JSON.parse(data);

				   	if(r.status==1){
				   		alert(r.msg);
				   		$("#btn-stop").attr("disabled", false);
				   		$("#btn-start").attr("disabled", true);
				   		$("#btn-reset").attr("disabled", false);
				   		
				   	}else{
				   		alert(r.msg);
				   		$("#btn-start").attr("disabled", false);
				   		$("#btn-stop").attr("disabled", true);
				   		$("#btn-reset").attr("disabled", false);
				   		
				   	}

				   }
				  })
	    		});

		 });
          </script>