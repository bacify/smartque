<h1 class="page-header mb-4 text-gray-800"> Pengaturan Loket</h2>

	<div class="row">
		<div class="col-lg-6 col-sm-12">
			 
			<?php 
			 		
			 		$x=$this->session->flashdata();
			 		if(count($x)>0){
			 			echo '<div class="alert alert-info alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
						foreach ($x as $d) {
								echo $d.'<br>';		   
							 
						}
						echo '</div>';
						
					}
			?>
			
			 
			<form class="form" action="#" method="POST">
			<table class="table table-sm">
				<thead><tr>
							<th> No</th>
							<th> Setting </th>
							<th> Value</th>							
						</tr>
				</thead>
				<tbody>
					<tr><td>1</td>
						<td>Status Antrian</td>
						<td><input id="status-antrian" class="form-control" name="queue_status" type="checkbox" value="1" data-toggle="toggle" data-onstyle="primary" data-width="50" <?php if($setting['queue_status']==1) echo 'checked';?>  ></td>
					</tr>
					<tr><td>2</td>
						<td>Mode Antrian</td>
						<td><input id="mode_antrian" class="form-control" name="queue_mode" type="checkbox" value="1" data-toggle="toggle" data-onstyle="primary" data-on="auto" data-off="manual" <?php if($setting['queue_mode']==1) echo 'checked';?>></td>
					</tr>
					<tr><td>3</td>
						<td>Nama Instansi</td>
						<td><input id="nama_instansi" class="form-control" name="nama_instansi" type="text" value="<?php echo $setting['nama_instansi'];?>"></td>
					</tr>
					<tr><td>4</td>
						<td>alamat Instansi</td>
						<td><input id="alamat_instansi" class="form-control" name="alamat_instansi" type="text" value="<?php echo $setting['alamat_instansi'];?>"></td>
					</tr>
					<tr><td>5</td>
						<td>Telp Instansi</td>
						<td><input id="telp_instansi" class="form-control" name="telp_instansi" type="text" value="<?php echo $setting['telp_instansi'];?>"></td>
					</tr>
					<tr><td>6</td>
						<td>Tampilan Display</td>
						<td><select id="jenis_display" class="form-control" name="jenis_display" >
							<option value="1" <?php if($setting['jenis_display']==1) echo 'selected';?>>Video</option>
							<option value="2" <?php if($setting['jenis_display']==2) echo 'selected';?>>Slideshow (Gambar)</option>
							</select></td>
					</tr>
					<tr><td>7</td>
						<td>Video</td>
						<td><select id="video" class="form-control" name="video" >
							<?php 
							foreach($video as $v){
								echo '<option value="'.$v->id_media.'">'.$v->id_media.'. '.$v->filename.' </option>';

							}
							if(count($video)==0){
								echo '<option value="0">Video tidak tersedia</option>';
							}
							?>
							
							</select></td>
					</tr>	
					<tr><td>8</td>
						<td>SlideShow</td>
						<td><input id="slideshow" class="form-control" name="slideshow" type="text" value="<?php echo $setting['slideshow'];?>" readonly></td>
					</tr>			
					<tr><td>9</td>
						<td>Key</td>
						<td><input id="key_activation" class="form-control" name="key_activation" type="text" value="<?php echo $setting['key_activation'];?>" >
							<small id="emailHelp" class="form-text text-muted">cek kunci aktivasi anda <a target="_blank" href="<?php echo base_url();?>administrator/key_activation" class="btn-link">disini</a></small></td>
					</tr>				
					<tr><td>10</td>
						<td>Login User</td>
						<td><input id="login" class="form-control" name="email" type="email" value="<?php echo $setting['email'];?>" ></td>
					</tr>		
					<tr><td>11</td>
						<td>Password</td>
						<td><div class="input-group ">
							  <input id="password" type="password" class="form-control" placeholder="password" name="password" value="<?php echo $setting['password'];?>" autocomplete="off" data-toggle="tooltip" title="ketik Ulang untuk mengganti password!">
							  <div class="input-group-append">
							    <button class="btn btn-outline-info reveal" type="button"><i class="fas fa-eye-slash"></i></button>
							  </div>
							</div>
						</td>
					</tr>				
				</tbody>
			</table>			
			<button class="btn btn-success float-right" type="submit" name="submit" value="save">Simpan</button>
			</form>
		</div>
	</div>


<div id="modal-slideshow" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Gambar Slideshow</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
		    <label for="exampleFormControlSelect2">Daftar Gambar yang tersedia</label>
		    <select multiple class="form-control" id="listgambar">
		      <?php 
		      		$x=1;
		      		$lg=explode(",",$setting['slideshow']);
		      	foreach($images as $i){
		      		

		      		if(count($lg)>0){
		      			$status=0;
		      			foreach($lg as $l){

		      				if($i->id_media==$l){
		      					$status=1;		      				
		      					break;
		      				}
		      			}

		      			if($status==0){		      				
		      				echo '<option value="'.$i->id_media.'">'.$i->id_media.'. '.$i->filename.' </option>';
		      			}else{
		      				echo '<option value="'.$i->id_media.'" selected>'.$i->id_media.'. '.$i->filename.' </option>';
		      			}
		      		}else{
		      				echo '<option value="'.$i->id_media.'">'.$i->id_media.'. '.$i->filename.'</option>';
		      		}
		      }
		      if(count($images)==0){
		      		echo '<option value="0">gambar tidak tersedia</option>';
		      }

		      ?>
		    </select>
		    <small id="emailHelp" class="form-text text-muted">Gunakan tombol <b>Ctrl</b> untuk lebih dari 1 pilihan</small>
		  </div>
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-primary" data-dismiss="modal">Save</button>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo base_url();?>assets/js/homepage.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
	    $('#slideshow').click(function(){
	       	 $('#modal-slideshow').modal('show');
	    });


	    $('#modal-slideshow').on('show.bs.modal', function (e) {
  			console.log('modal dipanggli');
		})
		 $('#modal-slideshow').on('hide.bs.modal', function (e) {
  			console.log('modal ditutup');
  			a=$('#listgambar').val();
//  			console.log(a.join(","));
  			$('#slideshow').val(a.join(","));

		})
	})

	$(".reveal").on('click',function() {
    var $pwd = $("#password");
    if ($pwd.attr('type') === 'password') {
        $pwd.attr('type', 'text');
        $(".reveal").html('<i class="fas fa-eye"></i>');
    } else {
        $pwd.attr('type', 'password');
        $(".reveal").html('<i class="btn-eye fas fa-eye-slash">');
    }

});
</script>
