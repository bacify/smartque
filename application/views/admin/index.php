 <?php 
 date_default_timezone_set('Asia/Jakarta');

 ?>
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Smart Queue - Dashboard</title>

  <link rel="shortcut icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon">
  <link rel="icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon">

  
  <!-- Custom styles for this template-->
  <link href="<?php echo base_url();?>assets/css/sb-admin-2.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/css/admincustom.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/css/bootstrap4-toggle.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">


 

<!-- Custom fonts for this template-->
  <link href="<?php echo base_url();?>assets/fontawesome/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">


 
  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url();?>assets/js/jquery-3.4.0.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url();?>/assets/js/moment-with-locales.min.js"></script>
  <script src="<?php echo base_url();?>/assets/js/bootstrap4-toggle.min.js"></script>




  <!-- DATEPICKER -->
  <link href="<?php echo base_url();?>assets/css/bootstrap-datepicker.standalone.min.css" rel="stylesheet">
   <script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.min.js"></script>
   <script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.id.min.js"></script>

   <!-- file input plugin -->
  <link href="<?php echo base_url();?>assets/css/fileinput.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/fileinputthemes/explorer-fas/theme.css" rel="stylesheet">
  <script src="<?php echo base_url();?>assets/js/piexif.js"></script>
  <script src="<?php echo base_url();?>assets/js/sortable.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/purify.min.js"></script>
 
  <!-- import slick (slider) -->
   <link href="<?php echo base_url();?>assets/slick/slick.css" rel="stylesheet">
   <script src="<?php echo base_url();?>assets/slick/slick.min.js"></script>
  
   

   <!-- load the JS files in the right order -->
  
  <script src="<?php echo base_url();?>assets/js/fileinput.min.js"></script>
  <script src="<?php echo base_url();?>assets/fileinputthemes/explorer-fas/theme.js"></script>



  <!-- Page level plugins -->
  <script src="<?php echo base_url();?>assets/chart.js/Chart.min.js"></script>


</head>
<body  id="page-top" class="sidebar-toggled">

	<!-- Page Wrapper -->
  	<div id="wrapper">

	  	<!-- Sidebar -->
	    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

	      <!-- Sidebar - Brand -->
	      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo base_url();?>administrator">
	        <div class="sidebar-brand-icon rotate-n-15">
	          <img src="<?php echo base_url();?>assets/img/sq_logo.png" class="logo img-fluid rounded">
	        </div>
	        <div class="sidebar-brand-text mx-3">SmartQ Panel </div>
	      </a>

	      <!-- Divider -->
	      <hr class="sidebar-divider my-0">

	      <!-- Nav Item - Status -->
	      <li class="nav-item active">
	        <a class="nav-link" href="<?php echo base_url();?>administrator">
	          <i class="fas fa-fw fa-tachometer-alt"></i>
	          <span>Dashboard</span></a>
	      </li>

	      <!-- Divider -->
	      <hr class="sidebar-divider">

	      <!-- Heading -->
	      <div class="sidebar-heading">
	        Pengaturan
	      </div>

	      <!-- Nav Item - Pages Collapse Menu -->
	      <li class="nav-item">
	        <a class="nav-link collapsed" href="<?php echo base_url();?>administrator/status"  >
	          <i class="fas fa-fw fa-cog"></i>
	          <span>Status</span>
	        </a>
	        
	      </li>

	      <!-- Nav Item - Utilities Collapse Menu -->
	      <li class="nav-item">
	        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
	          <i class="fas fa-fw fa-wrench"></i>
	          <span>Pengaturan</span>
	        </a>
	        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
	          <div class="bg-white py-2 collapse-inner rounded">
	            <h6 class="collapse-header">Pengaturan:</h6>
	            <a class="collapse-item" href="<?php echo base_url();?>administrator/setting">Pengaturan Aplikasi</a>
	            <a class="collapse-item" href="<?php echo base_url();?>administrator/layanan">Layanan</a>
	            <a class="collapse-item" href="<?php echo base_url();?>administrator/loket">Loket</a>
	            <a class="collapse-item" href="<?php echo base_url();?>administrator/media">media</a>
	            
	          </div>
	        </div>
	      </li>

	      <!-- Divider -->
	      <hr class="sidebar-divider">

	      <!-- Heading -->
	      <div class="sidebar-heading">
	        Laporan
	      </div>

	      <!-- Nav Item - Tables -->
	      <li class="nav-item">
	        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#list-laporan" aria-expanded="true" aria-controls="list-laporan">
	          <i class="fas fa-fw fa-table"></i>
	          <span>Laporan</span></a>
	          <div id="list-laporan" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
	          <div class="bg-white py-2 collapse-inner rounded">
	            <h6 class="collapse-header">Laporan Kunjungan</h6>
	            <a class="collapse-item" href="<?php echo base_url();?>administrator/laporan_jam">Laporan PerJam</a>
	            <a class="collapse-item" href="<?php echo base_url();?>administrator/laporan_hari">Laporan PerHari</a>
	            <a class="collapse-item" href="<?php echo base_url();?>administrator/laporan_bulan">Laporan PerBulan</a>
	            <div class="collapse-divider"></div>
	            <h6 class="collapse-header">Performa Loket:</h6>
	            <a class="collapse-item" href="<?php echo base_url();?>administrator/stat_hari">Laporan PerHari</a>
	            <a class="collapse-item" href="<?php echo base_url();?>administrator/stat_bulan">Laporan PerBulan</a>
	            
	            
	          </div>
	        </div>
	      </li>

	      <!-- Nav Item - Charts -->
	      <li class="nav-item">
	        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#list-grafik" aria-expanded="true" aria-controls="list-grafik">
	          <i class="fas fa-fw fa-chart-area"></i>
	          <span>Grafik</span></a>
	          <div id="list-grafik" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
	          <div class="bg-white py-2 collapse-inner rounded">
	            <h6 class="collapse-header">Grafik Transaksi</h6>
	            <a class="collapse-item" href="<?php echo base_url();?>administrator/grafik_layanan">Grafik Layanan</a>
	            <a class="collapse-item" href="<?php echo base_url();?>administrator/grafik_layanan_bulan">Grafik Layanan (b)</a>
	            <a class="collapse-item" href="<?php echo base_url();?>administrator/grafik_layanan_loket">Grafik Performa Loket</a>
	            <a class="collapse-item" href="<?php echo base_url();?>administrator/grafik_layanan_loket_bulan">Grafik Performa Loket(b)</a>
	            
	            
	          </div>
	        </div>
	      </li>
	       <!-- Heading -->
	      <div class="sidebar-heading">
	        MASTER
	      </div>

	       <li class="nav-item">
	        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#list-data" aria-expanded="true" aria-controls="list-data">
	          <i class="fas fa-fw fa-database"></i>
	          <span>DATA</span></a>
	          <div id="list-data" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
	          <div class="bg-white py-2 collapse-inner rounded">
	            <h6 class="collapse-header">*MASTER DATA*</h6>
	            <a class="collapse-item" href="<?php echo base_url();?>administrator/reset_data">RESET Pengaturan</a>
	            <a class="collapse-item" href="<?php echo base_url();?>administrator/hapus_sampel">Hapus Sampel</a>
	            <a class="collapse-item" href="<?php echo base_url();?>administrator/hapus_data">Hapus Semua (*)</a>	            
	                       
	            
	          </div>
	        </div>
	      </li>

	      
	      <!-- Divider -->
	      <hr class="sidebar-divider d-none d-md-block">

	      <li class="nav-item">
	        <a class="nav-link" href="<?php echo base_url();?>">
	          <i class="fas fa-sign-out-alt "></i>
	          <span>Back to Home</span></a>
	      </li>

	      <!-- Divider -->
	      <hr class="sidebar-divider d-none d-md-block">

	      <!-- Sidebar Toggler (Sidebar) -->
	      <div class="text-center d-none d-md-inline">
	        <button class="rounded-circle border-0" id="sidebarToggle"></button>
	      </div>

	    </ul>
	    <!-- End of Sidebar -->
	    <!-- Content Wrapper -->
    	<div id="content-wrapper" class="d-flex flex-column">

    		<!-- Main Content -->
      		<div id="content">
	 		<?php 
            echo $header; 
            ?>
				<!-- Begin Page Content -->
        		<div class="container-fluid">
				 	<?php
				  	echo $content; 
				  	?>
				</div>
        		<!-- /.container-fluid -->
			</div>
      		<!-- End of Main Content -->

      		<?php 		  
		  	echo $footer; 
		  	?>


		</div>
    	<!-- End of Content Wrapper -->
	</div>
  <!-- End of Page Wrapper -->



  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="<?php echo base_url();?>administrator/login">Logout</a>
        </div>
      </div>
    </div>
  </div>

  
 <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url();?>assets/js/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url();?>assets/js/sb-admin-2.min.js"></script>
  

  <!-- Page level custom scripts -->
 <!--  <script src="assets/js/demo/chart-area-demo.js"></script>
  <script src="assets/js/demo/chart-pie-demo.js"></script> -->

  <script type="text/javascript">
  	$(document).ready(function(){

		if (window.matchMedia("(max-width: 700px)").matches) {
	  /* The viewport is less than, or equal to, 700 pixels wide */

	} else {
	  /* The viewport is greater than 700 pixels wide */
	  $("#sidebarToggle").click();
	  
	}

	});
  </script>



</body>
</html>