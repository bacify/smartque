

          <!-- Page Heading -->
          

          <!-- Content Row -->
         
           <div class="row">
            <div class="col-md-12">
               
              <h1 class="h3 mb-0 text-gray-800 float-left">Grafik</h1>
            
           
              <form class="form-inline float-right" method="GET">
                <?php foreach($layanan as $la){
                  ?>                
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="checkbox" id="check-<?php echo $la->id_layanan;?>" name="check-<?php echo $la->id_layanan;?>" value="1" <?php if($check[$la->id_layanan]==1) echo 'checked';?> >
                  <label class="form-check-label" for="inlineCheckbox1"><?php echo $la->nama_layanan;?></label>
                </div>
                <?php 
                  }
              ?>
                
                 <label class="sr-only" for="inputtgl">Tahun</label>
                  <input type="text" class="form-control mb-2 mr-sm-2 inputtgl" id="inputtgl" name="tahun" placeholder="cari tahun lainnya" autocomplete="off" required readonly="">
                   <button type="submit" class="btn btn-primary mb-2">Cari</button>
              </form>

            </div>
          </div>

          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Data Kunjungan (<?php echo $tahun;?>)</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                    <canvas id="myAreaChart"></canvas>
                  </div>
                </div>
              </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-4 col-lg-5">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Data Kunjungan (<?php echo $tahun;?>)</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-pie pt-4 pb-2">
                    <canvas id="myPieChart"></canvas>
                  </div>
                  <div class="mt-4 text-center small label-layanan">
                    <span class="mr-2">
                      <i class="fas fa-circle text-primary"></i> Direct
                    </span>
                    <span class="mr-2">
                      <i class="fas fa-circle text-success"></i> Social
                    </span>
                    <span class="mr-2">
                      <i class="fas fa-circle text-info"></i> Referral
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Content Row -->
          <div class="row">

            <!-- Content Column -->
            <div class="col-lg-6 mb-4">

              <!-- Project Card Example -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Persentase Kunjungan (<?php echo $tahun;?>)</h6>
                </div>
                <div class="card-body">
                  <?php 
                  
                        $xx=0;
                        foreach($layanan as $la){
                            $jumlah=0;
                            if($info['total']!=0)
                              $jumlah=$tlayanan[$xx] / $info['total'] * 100;
                            echo '<h4 class="small font-weight-bold">'.$la->nama_layanan.'  ('.$tlayanan[$xx].')<span class="float-right">'.round($jumlah,2).'%</span></h4>
                                  <div class="progress mb-4">
                                    <div class="progress-bar" role="progressbar" style="width: '.$jumlah.'%;background-color:'.$color[$xx++].';" aria-valuenow="'.$jumlah.'" aria-valuemin="0" aria-valuemax="100"></div>
                                  </div>';

                        }
                  ?>
                  
                </div>
              </div>

         

            </div>

             <div class="col-md-6 mb-4">
              <div class="card border-left-primary shadow py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Jumlah Kunjungan (<?php echo $tahun;?>)</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $info['total'];?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

           
          </div>

        

     




       <!-- Page level custom scripts -->
  <!-- <script src="<?php echo base_url();?>assets/demo/chart-area-demo.js"></script> -->
  
<script type="text/javascript">
  // Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

function number_format(number, decimals, dec_point, thousands_sep) {
  // *     example: number_format(1234.56, 2, ',', ' ');
  // *     return: '1 234,56'
  number = (number + '').replace(',', '').replace(' ', '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + Math.round(n * k) / k;
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}

// Area Chart Example
var ctx = document.getElementById("myAreaChart");
var myLineChart = new Chart(ctx, {
  type: 'line',
  data: {
    
    labels: [<?php foreach ($year_stats[1] as $key => $value) {
                      echo '"'.substr($key,0,3).'",';
                    }?>],
    datasets: 
        [
          <?php 
                $x=0;
                foreach($layanan as $ln){
                  if($check[$ln->id_layanan]==0)
                    continue;
                ?>
        {
      label: "<?php echo $ln->nama_layanan;?>",
      lineTension: 0.3,
      backgroundColor: "rgba(78, 115, 223, 0.05)",
      borderColor: "<?php echo $color[$x++];?>",
      pointRadius: 3,
      pointBackgroundColor: "rgba(78, 115, 223, 1)",
      pointBorderColor: "rgba(78, 115, 223, 1)",
      pointHoverRadius: 3,
      pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
      pointHoverBorderColor: "rgba(78, 115, 223, 1)",
      pointHitRadius: 10,
      pointBorderWidth: 2,
      data: [<?php foreach ($year_stats[$ln->id_layanan] as $key => $value) {
                      echo '"'.$value.'",';
                    }?>],
    }, 

    <?php 
      }
    ?>
    ],    
  },
  options: {
    maintainAspectRatio: false,
    layout: {
      padding: {
        left: 10,
        right: 25,
        top: 25,
        bottom: 0
      }
    },
    scales: {
      xAxes: [{
        time: {
          unit: 'month'
        },
        gridLines: {
          display: false,
          drawBorder: false
        },
        ticks: {
          maxTicksLimit: 12
        }
      }],
      yAxes: [{
        ticks: {
          maxTicksLimit: 8,
          padding: 10,
          // Include a dollar sign in the ticks
          callback: function(value, index, values) {
            return  number_format(value);
          }
        },
        gridLines: {
          color: "rgb(234, 236, 244)",
          zeroLineColor: "rgb(234, 236, 244)",
          drawBorder: false,
          borderDash: [2],
          zeroLineBorderDash: [2]
        }
      }],
    },
    legend: {
      display: false
    },
    tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      titleMarginBottom: 10,
      titleFontColor: '#6e707e',
      titleFontSize: 14,
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      intersect: false,
      mode: 'index',
      caretPadding: 10,
      callbacks: {
        label: function(tooltipItem, chart) {
          var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
          return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
        }
      }
    }
  }
});


</script>
  <script type="text/javascript">
    // Set new default font family and font color to mimic Bootstrap's default styling
      Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
      Chart.defaults.global.defaultFontColor = '#858796';

      // Pie Chart Example
      var ctx = document.getElementById("myPieChart");
      var myPieChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
          labels: [<?php foreach ($layanan as $la) {
                      echo '"'.$la->nama_layanan.'",';
                    }?>],
          datasets: [{
            data: [<?php 
                    
                    foreach ($tlayanan as $ta) {
                      echo '"'.$ta.'",';
                      
                    }?>],
            backgroundColor: [<?php 
                    
                    foreach ($color as $co) {
                      echo '"'.$co.'",';
                      
                    }?>],
            hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf'],
            hoverBorderColor: "rgba(234, 236, 244, 1)",
          }],
        },
        options: {
          maintainAspectRatio: false,
          tooltips: {
            backgroundColor: "rgb(255,255,255)",
            bodyFontColor: "#858796",
            borderColor: '#dddfeb',
            borderWidth: 1,
            xPadding: 15,
            yPadding: 15,
            displayColors: false,
            caretPadding: 10,
          },
          legend: {
            display: false
          },
          cutoutPercentage: 80,
        },
      });
      
      <?php 
        $html='';
        $x=0;
        foreach ($layanan as $la) {
          $html.=' <span class="mr-3" style="white-space: nowrap;"><i class="fas fa-circle" style="color:'.$color[$x++].';"></i>'.$la->nama_layanan.'</span>';
        }

        ?>
        $('.label-layanan').html('<?php echo $html;?>');

  </script>
  <script>
    $(document).ready(function () {
      $('#inputtgl').datepicker({
        format: "yyyy",
        minViewMode: 2,
        endDate: "+0d",
        language: "id",
        autoclose: true,
        todayHighlight: true
    });
    });
</script>