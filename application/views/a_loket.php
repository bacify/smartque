<h1 class="page-header mb-4 text-gray-800"> Pengaturan Loket</h2>

	<div class="row">
		<div class="col-lg-6 col-sm-12">
			<form class="form" action="#" method="POST">
			<table class="table table-sm">
				<thead><tr>
							<th> No Loket</th>
							<th> Alias </th>
							<th> Layanan</th>
							<th> Status</th>
						</tr>
				</thead>
				<tbody>
					<?php foreach($loket as $lo){
						echo '<tr><td>'.$lo->loket_id.'</td>'.
								'<td><input class="form-control" name="alias-'.$lo->loket_id.'" type="text" value="'.$lo->alias_name.'" maxlength="5" size="5"></td>';
								
						echo '<td><select class="mb-3 form-control" name="layanan-'.$lo->loket_id.'">';
								foreach ($layanan as $la){				
									if($la->status_layanan==0)
										continue;						  
									if($lo->layanan_id==$la->id_layanan)
										  echo '<option value="'.$la->id_layanan.'" selected>'.$la->nama_layanan.'</option>';
									else
										  echo '<option value="'.$la->id_layanan.'">'.$la->nama_layanan.'</option>';
								}
								'</select></td>';
								
								if($lo->status==1)
								echo '<td><input id="status-'.$lo->loket_id.'" class="form-control" name="status-'.$lo->loket_id.'" type="checkbox" value="1" data-toggle="toggle" data-onstyle="primary" data-width="50" checked></td>';
								else
								echo '<td><input id="status-'.$lo->loket_id.'" class="form-control" name="status-'.$lo->loket_id.'" type="checkbox" value="1" data-toggle="toggle" data-onstyle="primary" data-width="50"></td>';

						echo '</tr>';
					}
					?>
					
				</tbody>
			</table>			
			<button class="btn btn-success float-right" type="submit" name="submit" value="save">Simpan</button>
			</form>
		</div>
	</div>
