<!DOCTYPE html>
<html lang="en">
<head>
	<title>SMART QUEUE SYSTEM</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="shortcut icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon">
  <link rel="icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fontawesome/css/all.min.css">
<!--===============================================================================================-->
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/homepage.css">
<!--===============================================================================================-->
</head>
<body>
	
	
	<div class="size1 bg0 where1-parent">
		<!-- Coutdown -->
		<div class="d-flex justify-content-center align-items-center bg-img1 size2 where1 overlay1 where2 respon2" style="background-image: url('<?php echo base_url();?>assets/img/bg01.jpg');">

		<?php 

			/*show menu on logged*/
			if($this->session->userdata('email')){
				?>

			<div class="container">
		<div class="row">
			<button id="btn-loket-1" href="<?php echo base_url();?>home/main_display" class="flex-col-c-m size6 m-l-10 m-r-10 m-t-15 s2-txt4 bor2 btn btn-outline-info btn-lg <?php if($loket_status[1]==0) echo 'd-none';?>" data-toggle="modal" data-target="#PasswordModal" data-idloket="1" data-status="offline">
					<span id="text-loket-1" class="text-light">Loket 1</span> 
					<i class="status fas fa-4x fa-tv m-b-5 text-danger"></i> 					
					<span class="status-t font-weight-bold text-danger"> offline</span></button>
				
				<button id="btn-loket-2" href="<?php echo base_url();?>home/main_display" class="flex-col-c-m size6 m-l-10 m-r-10 m-t-15 s2-txt4 bor2 btn btn-outline-info btn-lg <?php if($loket_status[2]==0) echo 'd-none';?>" data-toggle="modal" data-target="#PasswordModal" data-idloket="2" data-status="offline">
					<span id="text-loket-2" class="text-light">Loket 2</span> 
					<i class="status fas fa-4x fa-tv m-b-5 text-danger"></i> 
					<span class="status-t font-weight-bold text-danger"> offline</span></button>

				<button id="btn-loket-3" href="<?php echo base_url();?>home/main_display" class="flex-col-c-m size6 m-l-10 m-r-10 m-t-15 s2-txt4 bor2 btn btn-outline-info btn-lg <?php if($loket_status[3]==0) echo 'd-none';?>" data-toggle="modal" data-target="#PasswordModal" data-idloket="3" data-status="offline">
					<span id="text-loket-3" class="text-light">Loket 3</span> 
					<i class="status fas fa-4x fa-tv m-b-5 text-danger"></i> 
					<span class="status-t font-weight-bold text-danger"> offline</span></button>
				
				<button id="btn-loket-4" href="<?php echo base_url();?>home/main_display" class="flex-col-c-m size6 m-l-10 m-r-10 m-t-15 s2-txt4 bor2 btn btn-outline-info btn-lg <?php if($loket_status[4]==0) echo 'd-none';?>" data-toggle="modal" data-target="#PasswordModal" data-idloket="4" data-status="offline">
					<span id="text-loket-4">Loket 4</span> 
					<i class="status fas fa-4x fa-tv m-b-5 text-danger"></i> 
					<span class="status-t font-weight-bold text-danger"> offline</span></button>
		</div>	 
		<div class="row">
			<button id="btn-loket-5" href="<?php echo base_url();?>home/main_display" class="flex-col-c-m size6 m-l-10 m-r-10 m-t-15 s2-txt4 bor2 btn btn-outline-info btn-lg <?php if($loket_status[5]==0) echo 'd-none';?>" data-toggle="modal" data-target="#PasswordModal" data-idloket="5" data-status="offline">
					<span id="text-loket-1" class="text-light">Loket 5</span> 
					<i class="status fas fa-4x fa-tv m-b-5 text-danger"></i> 					
					<span class="status-t font-weight-bold text-danger"> offline</span></button>
				
				<button id="btn-loket-6" href="<?php echo base_url();?>home/main_display" class="flex-col-c-m size6 m-l-10 m-r-10 m-t-15 s2-txt4 bor2 btn btn-outline-info btn-lg <?php if($loket_status[6]==0) echo 'd-none';?>" data-toggle="modal" data-target="#PasswordModal" data-idloket="6" data-status="offline" >
					<span id="text-loket-2" class="text-light">Loket 6</span> 
					<i class="status fas fa-4x fa-tv m-b-5 text-danger"></i> 					
					<span class="status-t font-weight-bold text-danger"> offline</span></button>

				<button id="btn-loket-7" href="<?php echo base_url();?>home/main_display" class="flex-col-c-m size6 m-l-10 m-r-10 m-t-15 s2-txt4 bor2 btn btn-outline-info btn-lg  <?php if($loket_status[7]==0) echo 'd-none';?>" data-toggle="modal" data-target="#PasswordModal" data-idloket="7" data-status="offline" >
					<span id="text-loket-3" class="text-light">Loket 7</span> 
					<i class="status fas fa-4x fa-tv m-b-5 text-danger"></i> 
					<span class="status-t font-weight-bold text-danger"> offline</span></button>
				
				<button id="btn-loket-8" href="<?php echo base_url();?>home/main_display" class="flex-col-c-m size6 m-l-10 m-r-10 m-t-15 s2-txt4 bor2 btn btn-outline-info btn-lg  <?php if($loket_status[8]==0) echo 'd-none';?>" data-toggle="modal" data-target="#PasswordModal" data-idloket="8" data-status="offline" >
					<span id="text-loket-3" class="text-light">Loket 8</span> 
					<i class="status fas fa-4x fa-tv m-b-5 text-danger"></i> 
					<span class="status-t font-weight-bold text-danger"> offline</span></button>
		</div>	 
	</div>
	<?php 
			}
			?> 
			
			<!-- 
				<button href="<?php echo base_url();?>home/main_display" class="flex-col-c-m size6 m-l-10 m-r-10 m-t-15 s2-txt4 bor2 btn btn-outline-info btn-lg" data-toggle="modal" data-target="#PasswordModal" data-idloket="1" data-status="online">
					<span id="text-loket-1" class="text-light">Loket 1</span> 
					<i class="fas fa-4x fa-tv m-b-5 text-success"></i> 
					
					<span class="font-weight-bold text-success"> Online</span></button>
				
				<button href="<?php echo base_url();?>home/main_display" class="flex-col-c-m size6 m-l-10 m-r-10 m-t-15 s2-txt4 bor2 btn btn-outline-info btn-lg" data-toggle="modal" data-target="#PasswordModal" data-idloket="2" data-status="offline">
					<span id="text-loket-2" class="text-light">Loket 2</span> 
					<i class="fas fa-4x fa-tv m-b-5 text-danger"></i> 
					<span class="font-weight-bold text-danger"> offline</span></button>

				<button href="<?php echo base_url();?>home/main_display" class="flex-col-c-m size6 m-l-10 m-r-10 m-t-15 s2-txt4 bor2 btn btn-outline-info btn-lg" data-toggle="modal" data-target="#PasswordModal" data-idloket="3" data-status="online">
					<span id="text-loket-3" class="text-light">Loket 4</span> 
					<i class="fas fa-4x fa-tv m-b-5 text-success"></i> 
					<span class="badge badge-success  font-weight-bold"> Online</span></button>
				
				<button href="<?php echo base_url();?>home/main_display" class="flex-col-c-m size6 m-l-10 m-r-10 m-t-15 s2-txt4 bor2 btn btn-outline-info btn-lg " data-toggle="modal" data-target="#PasswordModal" data-idloket="4" data-status="offline">
					<span id="text-loket-4">Loket 4</span> 
					<i class="fas fa-4x fa-tv m-b-5 text-danger"></i> 
					<span class="badge badge-danger font-weight-bold  "> offline</span></button> -->

				
			
				
						 
		</div>

		
		<!-- Form -->
		<div class="size3 flex-col-sb flex-w p-l-75 p-r-75 p-t-45 p-b-45 respon1">
			<center>
			<div class="wrap-pic1">
				<img src="<?php echo base_url();?>assets/img/sq_logo.png" class="img-rounded" alt="LOGO">
			</div>
		</center>

			<div class="p-t-50 p-b-60">
				<?php 

			/*show menu on logged*/
			if($this->session->userdata('email')){
				?>

				<p class="m1-txt1 p-b-36">
					Selamat Datang di <span class="m1-txt2">Smart Queue System</span>
				</p>
				<a  class="btn btn-outline-info btn-lg" href="<?php echo base_url();?>home"><i class="fas fa-2x fa-arrow-circle-left m-b-5"></i> Kembali</></a>
				<?php 
					}else{
				?>
					<p class="m1-txt1 p-b-36">
					Selamat Datang di <span class="m1-txt2">Smart Queue System</span>, silahkan login!
				</p>
				<form class="contact100-form validate-form" action="<?php echo base_url();?>home/login" method="POST">				
					<div class="text-warning"><center><h3><?php echo $this->session->flashdata('pesan');?></h3></center></div>
					<div class="wrap-input100 m-b-20 validate-input" data-validate = "Email is required: ex@abc.xyz">
						<input class="s2-txt1 placeholder0 input100" type="email" name="email" placeholder="Alamat Email">
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 m-b-20 validate-input" data-validate = "password is required">
						<input class="s2-txt1 placeholder0 input100" type="password" name="password" placeholder="Password" autocomplete="off">
						<span class="focus-input100"></span>
					</div>

					<div class="w-full">
						<button class="flex-c-m s2-txt2 size4 bg1 bor1 hov1 trans-04" type="submit" value="login" name="login">
							Login
						</button>
					</div>
				</form>
				<?php }
				?>

				<p class="s2-txt3 p-t-18">
					Silahkan Hubungi vendor jika kesulitan.
				</p>
			</div>

		<div class="flex-w pos-relative">
				<a href="#" class="flex-c-m size5 bg3 how1 trans-04 m-r-5">
					<i class="fab fa-facebook"></i>
				</a>

				<a href="#" class="flex-c-m size5 bg4 how1 trans-04 m-r-5">
					<i class="fab fa-twitter"></i>
				</a>

				<a href="#" class="flex-c-m size5 bg6 how1 trans-04 m-r-5">
					<i class="fab fa-instagram"></i>
				</a>
				<p class="text-right right-0 p-r-0 p-t-10 pos-absolute m1-txt5">
					<i class="fas fa-pencil-alt"></i> <span class="m1-txt6 pr-4">WhiteSpace</span>
				</p>
			</div>
		</div>
	</div>



	<!-- Modal -->
<div class="modal fade" id="PasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
    	<form method="POST" action="<?php echo base_url().'fix/login_loket';?>" id="form_login">
	      <div class="modal-header bg-dark">
	        <h5 class="modal-title text-light" id="PasswordModallabel">Loket <span class="noloket font-weight-bold">0</span> (<span class="statusloket font-weight-bold">0</span>)</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	     			<div class="form-group">	
	     			 <input type="hidden" name="noloket" class="inputloket" value="0">		   		  
			  		  <input type="password" name="passwordloket" class="form-control" id="passwordloket" aria-describedby="passwordhelp" placeholder="masukkan ulang Password!" autocomplete="off">
			  		    <small id="passresult" class="form-text text-danger font-weight-bold"></small>
					    <small id="passwordhelp" class="form-text text-muted">*pastikan loket dalam keadaan <b>offline</b> untuk mencegah duplikasi data.</small>
					  
			 		 </div>
	      </div>
	      <div class="modal-footer">

	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
	        <button type="submit" id="btn-submit-pass" class="btn btn-success">Lanjutkan</button>
	      </div>
  		</form>
    </div>
  </div>
</div>




	

<!--===============================================================================================-->	
	<script src="<?php echo base_url();?>assets/js/jquery-3.4.0.min.js"></script>
<!--===============================================================================================-->
	
	<script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
<!--===============================================================================================-->

<!--===============================================================================================-->
	
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/js/homepage.js"></script>

	<script>  

		$(document).ready(function(){

			 fetch_loket();

			 setInterval(function(){   
			  fetch_loket();
			  
			 }, 5000);

			 function fetch_loket()
			 {
			  $.ajax({
			   url:"<?php echo base_url();?>home/fetch_loket",
			   method:"POST",
			   dataType: 'json',
			   success:function(data){

			     
			      $.each(data.online, function(index, element) {
		            console.log('no '+index+'  => '+element);
		            if(element=='0'){
			            $('#btn-loket-'+index+' > .status').removeClass('text-success').addClass('text-danger');
			            $('#btn-loket-'+index+' > .status-t').removeClass('text-success').addClass('text-danger').html('Offline');
		        	}else{
		        		$('#btn-loket-'+index+' > .status').removeClass('text-danger').addClass('text-success');
			            $('#btn-loket-'+index+' > .status-t').removeClass('text-danger').addClass('text-success').html('Online');
		        	}
		            
		        });
			   	
			   }
			  });
			 }


			  $('#PasswordModal').on('show.bs.modal', function (event) {
			  var button = $(event.relatedTarget);
			  var recipient = button.data('idloket');
			  var status = button.data('status');
			  
			  var modal = $(this);
			  modal.find('.noloket').text(recipient);
			  if(status=='online')
			  modal.find('.statusloket').addClass('text-success').text(status);
				else
			  modal.find('.statusloket').addClass('text-danger').text(status);

			  modal.find('.inputloket').val(recipient);
			});
			 $('#PasswordModal').on('hide.bs.modal', function (event) {
			  var modal = $(this);
			  modal.find('.statusloket').removeClass('text-success');	
			  modal.find('.statusloket').removeClass('text-danger');
			  
			});


			 $("#form_login").submit(function(){
            $.ajax({
              url:$(this).attr("action"),
              data:$(this).serialize(),
              type:$(this).attr("method"),
              dataType: 'json',
              beforeSend: function() {
                
                $("#btn-submit-pass").attr("disabled",true);
                $("#passwordloket").val('');

              },
              complete:function() {
                setTimeout(function(){
                	$("#btn-submit-pass").attr("disabled",false);								
                },2000);

              },
              success:function(result) {
                
                r=result;
                
                if(r.status==1){
                	window.location.href = r.url;

                }else{
                	$('#passresult').html('Password salah, silahkan ulangi');
                }
              }
            })
            return false;
          });


			 

		})
</script>

</body>
</html>


