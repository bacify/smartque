<!DOCTYPE html>
<html lang="en">
<head>
	<title>SMART QUEUE SYSTEM</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="shortcut icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon">
  <link rel="icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fontawesome/css/all.min.css">
<!--===============================================================================================-->
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/util.css">	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/kiosk.css">
<!--===============================================================================================-->
</head>
<body>
	
	
	<div class="size1 bg0 where1-parent">
		<!-- Coutdown -->
		<div class="flex-c-m bg-img1 size2 where1 overlay1 	respon2" style="background-image: url('<?php echo base_url();?>assets/img/bg01.jpg');">
			<div class="container-fluid">
			<div class="row">
				<?php 
				$jumlah_layanan=count($layanan);
				if($jumlah_layanan>4){
					$a=1;

					foreach ($layanan as $la){
					    echo '<div class="col-md-6 my-2">
							    <button   id="btn-tiket-'.$a++.'" class="btn-t text-left btn btn-outline-info btn-block btn-lg btn-wrap-text s2-txt6 size7 btn-mod-transparent " value="'.$la->id_layanan.'" ><i class="fas   fa-hand-point-right"></i> '.$la->nama_layanan.'</button>
							  </div>';
					}
				}else if($jumlah_layanan<5 && $jumlah_layanan >0){
					$a=1;

					foreach ($layanan as $la){
					    echo '<div class="col-md-8 offset-md-2 my-2">
							    <button   id="btn-tiket-'.$a++.'" class="btn-t text-left btn btn-outline-info btn-block btn-lg btn-wrap-text s2-txt6 size7 btn-mod-transparent " value="'.$la->id_layanan.'"><i class="fas   fa-hand-point-right"></i> '.$la->nama_layanan.'</button>
							  </div>';
					}
				}else{
					echo '<div class="col-md-8 offset-md-2 my-2 badge-info text-center"><h2>tidak ada layanan yang aktif</h2></div>';
				}

				?>
				<!-- <div class="col-md-8 offset-md-2 my-2">
				<button   id="btn-tiket-1" class="text-left btn btn-outline-info btn-block btn-lg btn-wrap-text s2-txt6 size7 btn-mod-transparent "><i class="fas   fa-hand-point-right"></i> Pendaftaran Pasien BPJS DAN ASURANSI</button>
				</div>
				
				<div class="col-md-8 offset-md-2 my-2">
				<button  id="btn-tiket-2" class="text-left btn btn-outline-info btn-block btn-lg btn-wrap-text s2-txt6 size7 btn-mod-transparent"><i class="fas  fa-hand-point-right"></i> Pendaftaran Pasien Umum</button>
				</div>
				<div class="col-md-8 offset-md-2 my-2">
				<button  id="btn-tiket-3" class="text-left btn btn-outline-info btn-block btn-lg btn-wrap-text s2-txt6 size7 btn-mod-transparent"><i class="fas   fa-hand-point-right"></i> Pendaftaran Pasien Online</button>
				</div>
				<div class="col-md-8 offset-md-2 my-2">
				<button   id="btn-tiket-4" class="text-left btn btn-outline-info btn-block btn-lg btn-wrap-text s2-txt6 size7 btn-mod-transparent"><i class="fas   fa-hand-point-right"></i> lain-lain</button>
				</div>
				 --> 
			</div>
			</div>
			<?php 

			/*show menu on logged*/
			if($this->session->userdata('email')){
				?>
			
				
			<?php 
			}
			?> 
				
						 
		</div>
		
		<!-- Form -->
		<div class="size3 flex-col-sb flex-w p-l-20 p-r-85 p-t-45 p-b-45 respon1">
			<center>
			<div class="wrap-pic1">
				<img src="<?php echo base_url();?>assets/img/sq_logo.png" class="img-rounded" alt="LOGO">
			</div>
			</center>
			<div class="page-header">
				<h1 class="m1-txt4 text-center">silahkan pilih Loket Tujuan Anda <i class="fas fa-2x fa-hand-point-right"></i>
				</h1>
			</div>

			<div class=" p-b-60">
				 

				
				<p class="m1-txt2 p-t-60 p-b-0 text-center">Smart Queue System</p>				 

				<p class="m1-txt3 p-t-5 text-center">
					<?php echo $setting['nama_instansi'];?>
				</p>
				<p class="s2-txt5 p-b-5 text-center"><?php echo $setting['alamat_instansi'];?></p>
				<p class="s2-txt5 p-b-5 text-center"><?php echo $setting['telp_instansi'];?></p>
			</div>

			<div class="flex-w pos-relative">
				<!-- <a href="#" class="flex-c-m size5 bg3 how1 trans-04 m-r-5">
					<i class="fab fa-facebook"></i>
				</a>

				<a href="#" class="flex-c-m size5 bg4 how1 trans-04 m-r-5">
					<i class="fab fa-twitter"></i>
				</a>

				<a href="#" class="flex-c-m size5 bg6 how1 trans-04 m-r-5">
					<i class="fab fa-instagram"></i>
				</a> -->
				<p class="text-right right-0 p-r-0 p-t-10 pos-absolute m1-txt5">
					<i class="fas fa-pencil-alt"></i> <span class="m1-txt6">WhiteSpace</span>
				</p>
			</div>
		</div>


		<button type="button" id="b-close" class="btn btn-transparent" style="position: fixed; bottom: 0;left: 0;">
 			&nbsp
			</button>

	</div>
	

	<div id="print">
	</div>

	<div id="notification" class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" data-backdrop="static">
  <div class="modal-dialog modal-sm modal-dialog-centered ">
    <div class="modal-content rounded">
      <div class="modal-header bg-warning justify-content-center">
        <center><h5 class="modal-title">Konfirmasi</h5> </center>
      </div>
      <div id="konten-modal" class="modal-body justify-content-center">
        <center><p> Apakah anda ingin menutup aplikasi?</p>
        </center>
      </div>
      <div class="modal-footer justify-content-center">
        	 <button type="button" class="btn btn-danger w-close">Ya</button>
       		 <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>
	

<!--===============================================================================================-->	
	<script src="<?php echo base_url();?>assets/js/jquery-3.4.0.min.js"></script>
<!--===============================================================================================-->
	
	<script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
<!--===============================================================================================-->

<!--===============================================================================================-->
	
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/js/homepage.js"></script>

	<script>
		/* Get the documentElement (<html>) to display the page in fullscreen */
		 

		// Find the right method, call on correct element
			function launchFullScreen(element) {
			  if(element.requestFullScreen) {
			    element.requestFullScreen();
			  } else if(element.mozRequestFullScreen) {
			    element.mozRequestFullScreen();
			  } else if(element.webkitRequestFullScreen) {
			    element.webkitRequestFullScreen();
			  }
			}

			// Launch fullscreen for browsers that support it!
			//launchFullScreen(document.documentElement); // the whole page


			if((window.fullScreen) ||
			 	  (window.innerWidth == screen.width && window.innerHeight == screen.height)) {

			} else {
				/*var
				          el = document.documentElement
				        , rfs =
				               el.requestFullScreen
				            || el.webkitRequestFullScreen
				            || el.mozRequestFullScreen
				    ;
				    rfs.call(el);*/
				    $('#element').popover('show')
			}
		</script>


		<script type="text/javascript">
			
			$('#b-close').mousedown(function(){
				notifikasi= setTimeout(function(){  $('#notification').modal('show'); },5000);
			});
			$('#b-close').mouseup(function(){
				clearTimeout(notifikasi);
			});
			
			$('.w-close').click(function(){
				window.close();
			});


			$('.btn-t').click(function() {
				  layanan=$(this).val();
				//  console.log(layanan);
				  button=$(this);
				  button.attr("disabled", true);
				  notifikasi2= setTimeout(function(){  

				  	if($('.btn-t').is(":disabled")){
				  		location.reload();
				  	}

				   },5000);
				$.ajax({
				   	url:"<?php echo base_url();?>home/get_current_tiket/"+layanan,
					success:function(data)
					{
						r1=JSON.parse(data);
						//cetak tiket 
						console.log(r1.tiket.current_tiket);
						//update tiket next
						/*$.ajax({
						   	url:"<?php echo base_url();?>home/cetak_tiket/",
							success:function(dat)
							{
								
							}
					 	})*/
						var win = window.open('<?php echo base_url();?>home/cetak_tiket/'+r1.tiket.current_tiket+'/'+r1.sisa, '_blank');
							if (win) {
							    //Browser has allowed it to be opened
							    win.focus();
							} else {
							    //Browser has blocked it
							    alert('Please allow popups for this website');
							}

						$.ajax({
						   	url:"<?php echo base_url();?>home/next_tiket/"+layanan,
							success:function(data2)
							{
								r2=JSON.parse(data2);
								//cetak tiket 
								console.log(r2);
								//update tiket next
								 button.attr("disabled", false);
								 clearTimeout(notifikasi2);
							}
					 	})
					}
				 })
			});
		</script>

</body>
</html>