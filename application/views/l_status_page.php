 <?php 
 date_default_timezone_set('Asia/Jakarta');

 ?>
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Smart Queue - Dashboard</title>

  <!-- load bootstrap css -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" >

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url();?>assets/fontawesome/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">



  

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url();?>assets/js/jquery-3.4.0.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>


  

<!-- Custom styles for this template-->
  <!-- <link href="<?php echo base_url();?>assets/css/display.css" rel="stylesheet"> -->

  <style type="text/css">
   
 .logo{
 	width: 80px;

 }
 .display-10 {
    font-size: 10rem;
    font-weight: 300;
    line-height: 1.2;
}
.label-logo{
	display: none;
}

@media screen and (max-width: 768px) {
  .label-text {    
    display: none;
  }
  .label-logo{
  	display: inline;
  }
}
.online-status{
  
  
  box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #28a745 0 -1px 9px, #28a745 0 2px 12px;
  -webkit-animation: blinkYellow 1s infinite;
  -moz-animation: blinkYellow 1s infinite;
  -ms-animation: blinkYellow 1s infinite;
  -o-animation: blinkYellow 1s infinite;
  animation: blinkYellow 1s infinite;
}
.offline-status{
  
  
  box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #dc3545 0 -1px 9px, #dc3545 0 2px 12px;;
  -webkit-animation: blinkYellow 1s infinite;
  -moz-animation: blinkYellow 1s infinite;
  -ms-animation: blinkYellow 1s infinite;
  -o-animation: blinkYellow 1s infinite;
  animation: blinkYellow 1s infinite;
}


  </style>






</head>
<body>

	

	<div class="container-fluid my-3">
		<div class="card ">
			<div class="card-header bg-dark ">
				<h4 class="badge-dark"> Layanan Administrasi - Status Loket  </h4>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-sm-3 mb-2">
						<div class="card border-success ">
							<div class="catd-header">

							</div>
							<div class="card-body">
								<center>
									<h1 class="font-weight-bold text-total">100</h1>
								</center>
							</div>
							<div class="card-footer bg-success py-2 online-status">
								<center>
									<h5 class="font-weight-bold ">
										<span class="label-text badge badge-success">Online</span>
										<span class="label-logo badge badge-success">Online</span>
										<button class="btn btn-success btn-md float-right border border-light"><i class="fas fa-sign-in-alt"></i></button>
								</h5>
								</center>
							</div>
						</div>
						
					</div>

					<div class="col-sm-3 mb-2">
						<div class="card border-success ">
							<div class="card-body">
								<center>
									<h1 class="font-weight-bold text-sisa">Loket A</h1>
								</center>
							</div>
							<div class="card-footer bg-success py-2 online-status">
								<center>
									<h5 class="font-weight-bold ">
										<span class="label-text badge badge-success">Online</span>
										<span class="label-logo badge badge-success">Online</span>
										<button class="btn btn-success btn-md float-right border border-light"><i class="fas fa-sign-in-alt"></i></button>
									</h5>
								</center>
							</div>
						</div>
						
					</div>
					<div class="col-sm-3 mb-2">
						<div class="card border-danger ">
							<div class="card-body">
								<center>
									<h1 class="font-weight-bold text-sekarang">70</h1>
								</center>
							</div>
							<div class="card-footer bg-danger py-2 offline-status">
								<center>
									<h5 class="font-weight-bold ">
										<span class="label-text badge badge-danger">Offline</span>
										<span class="label-logo badge badge-danger">Offline</span>
										<button class="btn btn-danger btn-md float-right border border-light"><i class="fas fa-sign-in-alt"></i></button>
									</h5>
								</center>
							</div>
						</div>
						
					</div>
					<div class="col-sm-3 mb-2">
						<div class="card border-success ">
							<div class="card-body">
								<center>
									<h1 class="font-weight-bold text-lanjut">71</h1>
								</center>
							</div>
							<div class="card-footer bg-success py-2 online-status" >
								<center>
									<h5 class="font-weight-bold mx-auto">
										<span class="label-text badge badge-success">Online</span>
										<span class="label-logo badge badge-success">Online</span>
										<button class="btn btn-success btn-md float-right border border-light" class="btn btn-primary" data-toggle="modal" data-target="#PasswordModal" data-idloket="4" data-status="online"><i class="fas fa-sign-in-alt"></i></button>
									</h5>
								</center>
							</div>
						</div>
						
					</div>
				</div>


				
			</div>
			<!-- EOF CARD BODY -->
			<div class="card-footer">

			</div>
			<!-- EOF CARD FOOTER -->
		</div>

			 
			
	</div>


 

<!-- Modal -->
<div class="modal fade" id="PasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
    	<form>
	      <div class="modal-header bg-primary">
	        <h5 class="modal-title text-light" id="PasswordModallabel">Loket <span class="noloket font-weight-bold">0</span> (<span class="statusloket font-weight-bold">0</span>)</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	     			<div class="form-group">	
	     			 <input type="hidden" name="noloket" class="inputloket" value="0">		   		  
			  		  <input type="password" name="passwordloket" class="form-control" id="passwordloket" aria-describedby="passwordhelp" placeholder="Ketik Password!">
					    <small id="passwordhelp" class="form-text text-muted">*pastikan loket dalam keadaan <b>offline</b> untuk mencegah duplikasi data.</small>
			 		 </div>
	      </div>
	      <div class="modal-footer">

	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
	        <button type="button" class="btn btn-primary">Lanjutkan</button>
	      </div>
  		</form>
    </div>
  </div>
</div>


</body>
</html>

<script>  


$(document).ready(function(){

 fetch_loket();

 setInterval(function(){   
  fetch_loket();
  
 }, 5000);

 function fetch_loket()
 {
  $.ajax({
   url:"<?php echo base_url();?>home/fetch_loket",
   method:"POST",
   success:function(data){
    $('#user_details').html(data);
   }
  });
 }


  $('#PasswordModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget);
  var recipient = button.data('idloket');
  var status = button.data('status');
  var modal = $(this);
  modal.find('.noloket').text(recipient);
  if(status=='online')
  modal.find('.statusloket').addClass('text-success').text(status);
	else
  modal.find('.statusloket').addClass('text-danger').text(status);
  modal.find('.modal-body input .inputloket').val(recipient);
});
 $('#PasswordModal').on('hide.bs.modal', function (event) {
  var modal = $(this);
  modal.find('.statusloket').removeClass('text-success');	
  modal.find('.statusloket').removeClass('text-danger');
  
});


})
</script>