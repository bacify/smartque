 <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">RESET SERVER</h1>

          <div class="row">
          	<div class="col-lg-6 ">
          		<div class="card shadow mb-4">
          			<div class="card-header py-3 text-status">
          				<h4 class="p-2 font-weight-bold text-badge badge-danger text-center blinking">PERHATIAN!</h4>
                  		
                		 <?php 
                		 		 
                		 		if($this->session->flashdata('pesan')){
		                	 	echo '<div class="alert alert-warning alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
		                	 	 echo $this->session->flashdata('pesan').'</div>';
                	 	 			}
                	 	 			?>

					 <form method="POST">
					 
						  <div class="form-group">						     
						    <input type="password" class="form-control" id="password" name="password" placeholder="masukkan Password RESET" required>
						    <small id="emailHelp" class="form-text text-muted">*bukan password MASTER.</small>
						  </div>
						   
						  <button type="submit" class="btn btn-danger btn-icon-split float-right"><i class="fas fa-power-off"></i> RESET</button>
						</form>
						</div>
                	<div class="card-body">
                			<small class="text-muted"><b>*</b>MENU ini akan mengembalikan Pengaturan Aplikasi ke pengaturan AWAL.</small>
                	</div>
          		</div>

          	</div>
          </div>

          <script type="text/javascript">

          $(document).ready(function(){

	          	/*BUTTON CONTROL*/
				$("#btn-stop").click(function(){
					$("#btn-stop").attr("disabled", true);
	        	  $.ajax({
				   	url:"<?php echo base_url();?>administrator/stop_queue",
				   success:function(data)
				   {

				   	r=JSON.parse(data);

				   	if(r.status==1){
				   		alert(r.msg);
				   		$("#btn-start").attr("disabled", false);
				   		$('.text-status').html('<h4 class="m-0 font-weight-bold text-badge badge-danger text-center blinking">LAYANAN TIDAK AKTIF</h4>');
				   	}else{
				   		alert(r.msg);
				   		$("#btn-stop").attr("disabled", false);
				   			
				   		
				   	}

				   

				   }
				  })
	    		}); 

	    		/*BUTTON CONTROL*/
				$("#btn-start").click(function(){
					$("#btn-start").attr("disabled", true);
	        	  $.ajax({
				   	url:"<?php echo base_url();?>administrator/start_queue",
				   success:function(data)
				   {

				   	r=JSON.parse(data);

				   	if(r.status==1){
				   		alert(r.msg);
				   		$("#btn-stop").attr("disabled", false);
				   		$('.text-status').html('<h4 class="m-0 font-weight-bold text-badge badge-primary text-center blinking">SEDANG BERJALAN</h4>');
				   	}else{
				   		alert(r.msg);
				   		$("#btn-start").attr("disabled", false);
				   		
				   		
				   	}

				   	

				   }
				  })
	    		});

				$("#btn-reset").click(function(){
					$("#btn-reset").attr("disabled", true);
					
	        	  $.ajax({
				   	url:"<?php echo base_url();?>administrator/reset_queue",
				   success:function(data)
				   {

				   	r=JSON.parse(data);

				   	if(r.status==1){
				   		alert(r.msg);
				   		$("#btn-stop").attr("disabled", false);
				   		$("#btn-start").attr("disabled", true);
				   		$("#btn-reset").attr("disabled", false);
				   		
				   	}else{
				   		alert(r.msg);
				   		$("#btn-start").attr("disabled", false);
				   		$("#btn-stop").attr("disabled", true);
				   		$("#btn-reset").attr("disabled", false);
				   		
				   	}

				   }
				  })
	    		});

		 });
          </script>