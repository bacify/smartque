 <?php 
 date_default_timezone_set('Asia/Jakarta');

 ?>
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Smart Queue - Dashboard</title>

  <!-- load bootstrap css -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" >

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url();?>assets/fontawesome/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">



  

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url();?>assets/js/jquery-3.4.0.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url();?>/assets/js/moment-with-locales.min.js"></script>


  

<!-- Custom styles for this template-->
  <!-- <link href="<?php echo base_url();?>assets/css/display.css" rel="stylesheet"> -->

  <style type="text/css">
  #mainbody {
 	 background-image: url("<?php echo base_url()?>assets/img/main-bg.jpg");
  }
  	#header {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    height: 100px;
    background-color: rgba(51, 55, 69, 0.8);
}
#footer {
    position: fixed;
    bottom: 0;
    left: 0;
    right: 0;
    height: 60px;
    background-color:  rgba(254, 95, 85, 0.8);
}
#content {
    position: fixed;
    top: 100px;
    bottom: 60px;
    left: 0;
    right: 0;
    background-color: rgba(230, 52, 98, 0.8);
    overflow: hidden;
    

}

#today-time{
	text-align: right;
}
#status-antrian{
	
	text-align: center;

}

.display-10 {
    font-size: 10rem;
    font-weight: 300;
    line-height: 1.2;
}

/*col same height tweak*/
.row.display-flex {
  display: flex;
  flex-wrap: wrap;
}
.row.display-flex > [class*='col-'] {
  flex-grow: 1;
}

.panel-ads
{
	max-height: 430px;
}
#status-loket{
	text-align: center;
}

  </style>






</head>
<body id="mainbody">

	<!-- Page Wrapper -->
  	<div id="wrapper">	  	
	    <!-- Content Wrapper -->
    	<div id="content-wrapper">

    		<!-- Main Content -->
      		<div id="aaa">
	 		
				<div id="header" class="fixed-top"> 
					
					<div class="row display-flex">
						<div class="col-md-8">
							<center>
								<h1>Selamat Data Di RSUD RS ABCSDASD</h1>
							
							<span class="address"> jalansdasdasdasdasd asdasdasdl </span>
							</center>
						</div>
						<div class="col-md-4 my-auto" >
								<div id="today-time" class=" mx-2"><h3 id="time"> 12:12:12</h3> </div>
						</div>
					
					</div>
				</div>
				<div id="content" > 
					 
						<div id="status-antrian" class="row display-flex m-2">
							<div class="col-md-8 panel-ads ">
								<div class="Card h-100 border">
									<img src="<?php echo base_url();?>assets/img/main-bg.jpg" class="rounded w-100 h-100 p-1" alt="Responsive image">
								</div>
								
							</div>
							<div class="col-md-4 panel-status  ">
								<div class="card h-100 ">
									<div class="card-header bg-info">
										<h4 class="font-weight-bold display-4 "><span class="badge badge-info">Loket A1</span></h4>
									</div>									 
									<div class="card-body bg-primary">
										<h3 class="card-title">Nomor Antrian</h3>
										<h1 class="display-10"><span id="z1" class="badge badge-primary ">A001</span></h1>
										
									</div>
									<div class="card-footer bg-primary">
										<h3 class="font-weight-bold "><span class="badge badge-primary">jumlah antrian : 10</span></h3>
									</div>
								</div>
							</div>
						</div>
						<div id="status-loket" class="row display-flex m-2">
							<div class="col-md-3 panel-loket ">
								<div class="card h-100 ">
									<div class="card-header bg-info ">
										<h4 class="font-weight-bold"><span >Loket A1</span></h4>
									</div>									 
									<div class="card-body bg-primary">
									
										<h1 class="display-1"><span id="c1" class="badge badge-primary ">001</span></h1>
										
									</div>
								</div>

								
							</div>
							<div class="col-md-3 panel-loket">
								<div class="card h-100 ">
									<div class="card-header bg-info ">
										<h4 class="font-weight-bold"><span >Loket A1</span></h4>
									</div>									 
									<div class="card-body bg-primary">
									
										<h1 class="display-1"><span id="c2" class="badge badge-primary ">001</span></h1>
										
									</div>
								</div>
								
								
							</div>
							<div class="col-md-3 panel-loket">
								<div class="card h-100 ">
									<div class="card-header bg-info ">
										<h4 class="font-weight-bold"><span >Loket A1</span></h4>
									</div>									 
									<div class="card-body bg-primary">
									
										<h1 class="display-1"><span id="c3"  class="badge badge-primary ">001</span></h1>
										
									</div>
								</div>
								
								
							</div>
							<div class="col-md-3 panel-loket">
								<div class="card h-100 ">
									<div class="card-header bg-info ">
										<h4 class="font-weight-bold"><span >Loket A1</span></h4>
									</div>									 
									<div class="card-body bg-primary">
									
										<h1 class="display-1"><span id="c4" class="badge badge-primary ">001</span></h1>
										
									</div>
								</div>
								
								
							</div>
							 
						</div>
					 
				</div>
				<div id="footer" >
					<div class="row h-100">
					   <div class="col-sm-12 my-auto">
					     <h3 class="font-weight-bold text-warning my-auto" ><marquee class=" " behavior="scroll" direction="left">Your scrolling text goes here</marquee></h3>
					   </div>
					</div>					
				
				</div>


			</div>
      		<!-- End of Main Content -->  		
		</div>
    	<!-- End of Content Wrapper -->
	</div>
  <!-- End of Page Wrapper -->



 <script>
$(document).ready(function() {
    setInterval(timestamp, 1000);
});

function timestamp() {
	$('#time').html(moment().locale('id').format("dddd, Do MMMM YYYY, HH:mm:ss"));
}
</script>

  <!-- Page level custom scripts -->
 <!--  <script src="assets/js/demo/chart-area-demo.js"></script>
  <script src="assets/js/demo/chart-pie-demo.js"></script> -->

</body>
</html>






<script type="text/javascript">
	/**
 * AJAX long-polling
 *
 * 1. sends a request to the server (without a timestamp parameter)
 * 2. waits for an answer from server.php (which can take forever)
 * 3. if server.php responds (whenever), put data_from_file into #response
 * 4. and call the function again
 *
 * @param timestamp
 */
function getContent(timestamp)
{
    var queryString = {'timestamp' : timestamp};

    $.ajax(
        {
            type: 'GET',
            url: '<?php echo base_url();?>home/pulldata',
            data: queryString,
            success: function(data){
                // put result data into "obj"
                var obj = jQuery.parseJSON(data);
                // put the data_from_file into #response
               /* $('#response').html(obj.data_from_file);*/
                // call the function again, this time with the timestamp we just got from server.php
                $('#c1').html(obj.c1);
                $('#c2').html(obj.c2);
                $('#c3').html(obj.c3);
                $('#c4').html(obj.c4);
                $('#z1').html(obj.z1);
                getContent(obj.timestamp);
            }
        }
    );
}

// initialize jQuery
$(function() {
    getContent();
});

</script>