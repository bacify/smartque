 <?php 
 date_default_timezone_set('Asia/Jakarta');

 ?>
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Smart Queue </title> 

  <link rel="shortcut icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon">
  <link rel="icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon">


  <!-- load bootstrap css -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" >
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/plyr.css" >
  <link rel="stylesheet" href="<?php echo base_url();?>assets/slick/slick.css" >
  <link rel="stylesheet" href="<?php echo base_url();?>assets/slick/slick-theme.css" >

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url();?>assets/fontawesome/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">



  

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url();?>assets/js/jquery-3.4.0.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url();?>/assets/js/moment-with-locales.min.js"></script>
  <script src="<?php echo base_url();?>/assets/js/plyr.polyfilled.min.js"></script>


  <script src="<?php echo base_url();?>/assets/slick/slick.min.js"></script>
  <script src="<?php echo base_url();?>/assets/js/howler.min.js"></script>




  <style type="text/css">

  @font-face {
  font-family: Lato-Bold;
  src: url('<?php echo base_url();?>assets/fonts/Lato/Lato-Bold.ttf'); 
  }
  @font-face {
    font-family: Poppins-Regular;
    src: url('<?php echo base_url();?>assets/fonts/Poppins/Poppins-Regular.ttf'); 
  }

  @font-face {
    font-family: Poppins-Bold;
    src: url('<?php echo base_url();?>assets/fonts/Poppins/Poppins-Bold.ttf'); 
  }

  @font-face {
    font-family: Lato-Regular;
    src: url('<?php echo base_url();?>assets/fonts/Lato/Lato-Regular.ttf'); 
  }


  body{
  	/*background: rgb(50, 54, 57);*/
  	  font-family: "Lato-Bold";
  }
  	.video-container{
  			
		  width: 100%;		  
		  position: relative; /* If you want text inside of it */
  	}
  	.text-container{
  		width: 100%;
  		height: 100%
  	}
    .title-container{
      width: 100%;
      height: 15% 
    }
    .content-container{
     width: 100%;
      height: 85%  
    }
    .utama{
      background-color: black;
    }
    .badge-black{
          color: #fff;
    background-color: black;
    }
    .bg-anu {
    background-color: #5adad3!important;
}
.logo{
  height: 60px;
}
.display-x {
    font-size: 10rem;
    font-weight: 400;
    line-height: 1;
}
  	


  </style>


</head>
<body class="utama bg-black">
<div class="container-fluid px-3 ">
  <div class="row py-1 px-1">    
      	<div class="col-lg-8 pr-1 pl-0">
      		<div class="text-container bg-anu ">
	      		<div class="title-text float-left py-auto">              
	      			<h3 class="my-3 ml-1"> <img src="<?php echo base_url();?>assets/img/sq_logo.png" class="logo img-fluid rounded p-0 m-0 "> </h3>
	      		</div>
	      		<div class="info-text float-right mr-2 py-3">
	      			<h3 class="text-right m-0"> <?php echo $setting['nama_instansi'];?></h3>
	      			<p class="text-right m-0"><?php echo $setting['alamat_instansi'];?></p>
	      		</div>
      		</div>
      	</div>
      	<div class="col-lg-4 pr-1 pl-0">
      		<div class="text-container  bg-anu ">
      			<h2 id="time" class="pt-4 text-center"> JAM</h2> 
      		</div>
      	</div>
     
  </div>

  <div class="row py-1 px-1">
  	<div class="col-lg-8 pr-1 pl-0">
  		<div class="video-container  rounded border border-info">
  			<video id="player" poster="<?php echo base_url().'assets/uploads/'.$setting['video_name'];?>" id="player" muted="muted" loop>
				    <source src="<?php echo base_url().'assets/uploads/'.$setting['video_name'];?>" type="video/mp4" />
				    

				    <!-- Captions are optional -->
				   <!--  <track kind="captions" label="English captions" src="/path/to/captions.vtt" srclang="en" default /> -->
				</video>
  		</div>
  	</div>

  	<div class="col-lg-4 pr-1 pl-0">
      <div class="text-container ">
  		<div class="title-container">  			
        <div class="container-box h-100">
            <div class="text-center badge-info h-100">
                  <h2 class="page-header pt-4 font-weight-bold">Status Saat Ini </h2>              
            </div>
          </div>
        </div>
        <div class="content-container">          
    			<div class="container-box h-50 ">
    				<div class="card text-center h-100 rounded-0 border-0 bg-anu">
              <div class="card-header border-bottom badge-danger rounded-0">
                 <h1 class="page-header p-0 m-0">Loket <b id="no-ganjil"> </b></h1>
              </div>
              <div class="card-body">
                <h1 class="display-x font-weight-bold text-ganjil p-0 m-0 "> - </h1>
              </div>
             
            </div>
    			</div>
          <div class="container-box h-50 ">
            <div class="card text-center   h-100 rounded-0 border-0 bg-anu">
              <div class="card-header border-bottom badge-info  rounded-0">
                 <h1 class="page-header p-0 m-0">Loket <b id="no-genap"> </b></h1>
              </div>
              <div class="card-body">
                <h2 class="display-x text-lg font-weight-bold text-genap p-0 m-0"> - </h2>
              </div>
             
            </div>
          </div>
  			 
  			 
      </div>
    </div>
  	</div>

  </div>
   <div class="row py-1 px-1 loket-slider">
    
     
   	<?php 
      $x=1;
      foreach($loket as $l){
        if($x++%2==0){
   		?>
   	
	  	<div id="loket-<?php echo $l->loket_id; ?>" class="col-lg-3 pr-1 pl-0 mb-1 loket-sld">
	  		<div class="text-container  bg-anu rounded border border-info">
	      		<div class="card text-center border-0 bg-anu">
	      			<div class="card-title border-bottom border-info badge-info">
	      				 <h2 class="page-header">Loket <b><?php echo $l->alias_name; ?></b></h2>
	      			</div>
	      			<div class="card-body">
	      				<h1 id="counterloket-<?php echo $l->loket_id; ?>" class="text-lg font-weight-bold my-1 py-0"> - </h1> 
	      			</div>
	      			<div class="card-footer">
	      				<small class="text-muted timecounter-<?php echo $l->loket_id; ?>">00:00:01</small>
	      			</div>
	      		</div>
	      	</div>
	  	</div>
  	<?php 
    }else{
        ?>
    
      <div id="loket-<?php echo $l->loket_id; ?>" class="col-lg-3 pr-1 pl-0 mb-1 loket-sld">
        <div class="text-container  bg-anu rounded border border-danger">
            <div class="card text-center border-0 bg-anu">
              <div class="card-title border-bottom border-danger badge-danger">
                 <h2 class="page-header">Loket <b><?php echo $l->alias_name; ?></b></h2>
              </div>
              <div class="card-body">
                <h1 id="counterloket-<?php echo $l->loket_id; ?>" class="text-lg font-weight-bold my-1 py-0"> - </h1> 
              </div>
              <div class="card-footer">
                <small class="text-muted timecounter-<?php echo $l->loket_id; ?>">00:00:01</small>
              </div>
            </div>
          </div>
      </div>
    <?php 
    }
	  }
	  ?>
  </div>
  	

  </div>
</div>
	

<div id="notification" class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" data-backdrop="static">
  <div class="modal-dialog modal-sm modal-dialog-centered ">
    <div class="modal-content rounded">
      <div class="modal-header bg-warning justify-content-center">
        <center><h5 class="modal-title">Server Error</h5> </center>
      </div>
      <div id="konten-modal" class="modal-body justify-content-center">
        <center><p> menghubungkan ulang... (<span id="text-recon">1</span>/3)</p>
        <div class="spinner-border"></div></center>
      </div>
      <div class="modal-footer justify-content-center">
        
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">  
	const player = new Plyr('#player', {
    				title: 'Example Title',
    				autoplay:true,
    				hideControls:true,
    				ratio:'16:9',
    				fullscreen:true,
    				volume:0,
    				

				});



  $(document).ready(function() {
    timestamp();
    setInterval(timestamp, 1000);

});



function timestamp() {
  $('#time').html(moment().locale('id').format("dddd, Do MMMM YYYY, HH:mm:ss"));
}



/*add autorefresh script*/
function autorefresh(timeoutPeriod) {
  setTimeout("location.reload(true);",timeoutPeriod);
}
window.onload = autorefresh(300000); //refresh 5minutes

</script>


<script type="text/javascript">

  var retry=0;
  

	/**
 * AJAX long-polling
 *
 * 1. sends a request to the server (without a timestamp parameter)
 * 2. waits for an answer from server.php (which can take forever)
 * 3. if server.php responds (whenever), put data_from_file into #response
 * 4. and call the function again
 *
 * @param timestamp
 */
function getContent(timestamp)
{
    var queryString = {'timestamp' : timestamp};

    $.ajax(
        {
            type: 'GET',
            url: '<?php echo base_url();?>home/pullloketdata',
            data: queryString,
            success: function(data){

              $('#notification').modal('hide');
              retry=0;

                // put result data into "obj"
                var obj = jQuery.parseJSON(data);
                var x=1;
                for(a=1;a<=8;a++){
                  $('#counterloket-'+a).html(obj.loket[a]['counter']);
                	$('.timecounter-'+a).html(obj.loket[a]['time']);
                  if(obj.loket[a]['order']==1){
                    if(obj.loket[a]!='-'){
                      $('#no-ganjil').html(a);
                      $('.text-ganjil').html(obj.loket[a]['counter']);
                      x++;
                    }
                  }else if(obj.loket[a]['order']==2){
                     if(obj.loket[a]!='-'){
                      $('#no-genap').html(a);
                      $('.text-genap').html(obj.loket[a]['counter']);
                      x++;
                    } 
                  }
                }

                // put the data_from_file into #response
               /* $('#response').html(obj.data_from_file);*/
                // call the function again, this time with the timestamp we just got from server.php
               
               // console.log(obj);
                getContent(obj.timestamp);
                restart_slick();

            },
            error:function(error){
              $('#notification').modal('show');
              
              if(retry==3)
              {
                $('#konten-modal').html('<p>Server Tidak Dapat terhubung, silahkan cek Jaringan lalu refresh </p>');
                return ;
              }else{
                retry++;
                $('#text-recon').html(retry);
                setTimeout(
                  function(){ 
                    getContent(); 
                    getcall();
                      },10000);
              }

            }
        }
    );
}
function getcall(timestamp)
{
    var queryString = {'timestamp' : timestamp};

    $.ajax(
        {
            type: 'GET',
            url: '<?php echo base_url();?>home/pullcurrentcall',
            data: queryString,
            success: function(data){
                // put result data into "obj"
                var obj = jQuery.parseJSON(data);
               // put the data_from_file into #response
               /* $('#response').html(obj.data_from_file);*/
                // call the function again, this time with the timestamp we just got from server.php
                
                if(obj.status==1&&timestamp!='undefined'){
               		playaudio(obj.nomor,obj.loket);
               	}
                
                getcall(obj.timestamp);
                
               
            }
        }
    );
}

a=$('.loket-sld').length;
 

if($('.loket-sld').length>3){
  $('.loket-slider').slick({
  	  infinite: true,
  	  slidesToShow: 4,
  	  slidesToScroll: 1,
  	  speed:3000,
  	  autoplay: true,
    	 autoplaySpeed: 2000,
    	 arrows:false,
    	 touchMove:false,
    	 swipe:false,
  });
}

function restart_slick(){
  
  if($('.loket-sld').length>3){
  	$('.loket-slider').slick('unslick');
  	$('.loket-slider').slick({
  	  infinite: true,
  	  slidesToShow: 4,
  	  slidesToScroll: 1,
  	  speed:3000,
  	  autoplay: true,
    	 autoplaySpeed: 2000,
    	 arrows:false,
  	});
  }
}


// initialize jQuery
$(function() {
  $.ajax({
            url:"<?php echo base_url();?>fix/end_calling",
           success:function(data)
           {          

           }
          });
    getContent();
    getcall();

   

});

function playaudio(text,loket){

	console.log('panggil '+text+' , ke loket '+loket);
	kata=text.toLowerCase().replace(/\s/g, "");
	loketalias=loket.toLowerCase().replace(/\s/g, "").split('');

  if(kata.length!=0 && loketalias.length!=0){
    
      nomor=kata.match(/\d+/);
      if(nomor!=null){
        kodeantrian=kata.substr(0,(kata.length-nomor[0].length)); 
        kodeantrianarray=kodeantrian.split('');

        /*create sound kode antrian*/
        var audio=new Array(kodeantrianarray.length);
        for (i = 0; i < kodeantrianarray.length; i++) { 
          //  console.log(kata[i])
            audio[i] = new Audio('<?php echo base_url();?>assets/audio/'+kodeantrianarray[i]+'.mp3');     

        }  
        for (i = 0; i < kodeantrianarray.length; i++) { 
          //  console.log(kata[i])      
            if(i<(kodeantrianarray.length-1))
              audio[i].next=audio[i+1];
        }
        for (i = 0; i < kodeantrianarray.length; i++) { 
          //  console.log(kata[i])      
            if(i<(kodeantrianarray.length-1))
              audio[i].onended=function(){
                this.next.play();
              }
        }



          /*create sound nomor*/
        /*kodenomor=nomor[0];

        if(kodenomor<12){
          audionomor=new Array(1);
          audionomor[0] = new Audio('<?php echo base_url();?>assets/audio/'+kodenomor+'.mp3');     
        }else if(kodenomor>11 && kodenomor<20){
          audionomor=new Array(2);
          lastdigit=kodenomor.toString().substr(-1);
          audionomor[0] = new Audio('<?php echo base_url();?>assets/audio/'+lastdigit+'.mp3');               
          audionomor[1] = new Audio('<?php echo base_url();?>assets/audio/belas.mp3');               
        }else if(kodenomor>19 && kodenomor<100 && (kodenomor%10==0)){
          audionomor=new Array(2);
          firstdigit=kodenomor.toString().substr(0,1);
          audionomor[0] = new Audio('<?php echo base_url();?>assets/audio/'+firstdigit+'.mp3');               
          audionomor[1] = new Audio('<?php echo base_url();?>assets/audio/puluh.mp3');               
        }else if(kodenomor>19 && kodenomor<100 && (kodenomor%10!=0)){
          audionomor=new Array(3);
          firstdigit=kodenomor.toString().substr(0,1);
          lastdigit=kodenomor.toString().substr(-1);
          audionomor[0] = new Audio('<?php echo base_url();?>assets/audio/'+firstdigit+'.mp3');               
          audionomor[1] = new Audio('<?php echo base_url();?>assets/audio/puluh.mp3');               
          audionomor[2] = new Audio('<?php echo base_url();?>assets/audio/'+lastdigit+'.mp3');               
        }else if(kodenomor==100){
          audionomor=new Array(1);
          audionomor[0] = new Audio('<?php echo base_url();?>assets/audio/'+kodenomor+'.mp3');                         
        }else if(kodenomor>100 && kodenomor <112){
          audionomor=new Array(2);
          nextnumber=Number(kodenomor.substr(1)).toString();
          audionomor[0] = new Audio('<?php echo base_url();?>assets/audio/100.mp3'); 
          audionomor[1] = new Audio('<?php echo base_url();?>assets/audio/'+nextnumber+'.mp3');                             
        }else if(kodenomor>100 && kodenomor >111 && kodenomor<120){
          audionomor=new Array(3);
          nextnumber=Number(kodenomor.substr(1)).toString();
          lastdigit=nextnumber.toString().substr(-1);
          audionomor[0] = new Audio('<?php echo base_url();?>assets/audio/100.mp3');               
          audionomor[1] = new Audio('<?php echo base_url();?>assets/audio/'+lastdigit+'.mp3');               
          audionomor[2] = new Audio('<?php echo base_url();?>assets/audio/belas.mp3');               

        }else if(kodenomor>119 && kodenomor<200 && (kodenomor%10==0)){
          audionomor=new Array(3);
          nextnumber=Number(kodenomor.substr(1)).toString();
          firstdigit=nextnumber.toString().substr(0,1);
          audionomor[0] = new Audio('<?php echo base_url();?>assets/audio/100.mp3');           
          audionomor[1] = new Audio('<?php echo base_url();?>assets/audio/'+firstdigit+'.mp3');               
          audionomor[2] = new Audio('<?php echo base_url();?>assets/audio/puluh.mp3');         
          
        }else if(kodenomor>119 && kodenomor<200 && (kodenomor%10!=0)){
          audionomor=new Array(4);
          nextnumber=Number(kodenomor.substr(1)).toString();
          firstdigit=nextnumber.toString().substr(0,1);
          lastdigit=nextnumber.toString().substr(-1);
          audionomor[0] = new Audio('<?php echo base_url();?>assets/audio/100.mp3');    
          audionomor[1] = new Audio('<?php echo base_url();?>assets/audio/'+firstdigit+'.mp3');               
          audionomor[2] = new Audio('<?php echo base_url();?>assets/audio/puluh.mp3');               
          audionomor[3] = new Audio('<?php echo base_url();?>assets/audio/'+lastdigit+'.mp3');              
        }else if(kodenomor>100 &&kodenomor<1000 && (kodenomor%100==0)){
          audionomor=new Array(2);
          firstnumber=kodenomor.toString().substr(0,1);
          audionomor[0] = new Audio('<?php echo base_url();?>assets/audio/'+firstnumber+'.mp3');            
          audionomor[1] = new Audio('<?php echo base_url();?>assets/audio/ratus.mp3');            
        }else if(kodenomor>100 &&kodenomor<1000 && (kodenomor%100!=0)){          
          firstnumber=kodenomor.toString().substr(0,1);
          nextnumber=Number(kodenomor.substr(1)).toString();
          if(nextnumber<12){
            audionomor=new Array(3);
            audionomor[0] = new Audio('<?php echo base_url();?>assets/audio/'+firstnumber+'.mp3');            
            audionomor[1] = new Audio('<?php echo base_url();?>assets/audio/ratus.mp3');     
            audionomor[2] = new Audio('<?php echo base_url();?>assets/audio/'+nextnumber+'.mp3');     
          }else if(nextnumber>11 && nextnumber<20){
            audionomor=new Array(4);
            lastdigit=nextnumber.toString().substr(-1);
            audionomor[0] = new Audio('<?php echo base_url();?>assets/audio/'+firstnumber+'.mp3');            
            audionomor[1] = new Audio('<?php echo base_url();?>assets/audio/ratus.mp3');     
            audionomor[2] = new Audio('<?php echo base_url();?>assets/audio/'+lastdigit+'.mp3');               
            audionomor[3] = new Audio('<?php echo base_url();?>assets/audio/belas.mp3');               
          }else if(nextnumber>19 && nextnumber<100 && (nextnumber%10==0)){
            audionomor=new Array(4);
            firstdigit=nextnumber.toString().substr(0,1);
            audionomor[0] = new Audio('<?php echo base_url();?>assets/audio/'+firstnumber+'.mp3');            
            audionomor[1] = new Audio('<?php echo base_url();?>assets/audio/ratus.mp3');     
            audionomor[2] = new Audio('<?php echo base_url();?>assets/audio/'+firstdigit+'.mp3');               
            audionomor[3] = new Audio('<?php echo base_url();?>assets/audio/puluh.mp3');               
          }else if(nextnumber>19 && nextnumber<100 && (nextnumber%10!=0)){
            audionomor=new Array(5);
            firstdigit=kodenomor.toString().substr(0,1);
            lastdigit=kodenomor.toString().substr(-1);
            audionomor[0] = new Audio('<?php echo base_url();?>assets/audio/'+firstnumber+'.mp3');            
            audionomor[1] = new Audio('<?php echo base_url();?>assets/audio/ratus.mp3');                 
            audionomor[2] = new Audio('<?php echo base_url();?>assets/audio/'+firstdigit+'.mp3');               
            audionomor[3] = new Audio('<?php echo base_url();?>assets/audio/puluh.mp3');               
            audionomor[4] = new Audio('<?php echo base_url();?>assets/audio/'+lastdigit+'.mp3');               
          }      
        }else if(kodenomor==1000){
          audionomor=new Array(1);
          audionomor[0] = new Audio('<?php echo base_url();?>assets/audio/'+kodenomor+'.mp3');               
        }else{
          console.log('nomor diluar jangkauan'+kodenomor);
          return false;
        }*/


        kodenomor=nomor[0];
          audionomor=new Array(1);
          audionomor[0] = new Audio('<?php echo base_url();?>assets/audio/'+kodenomor+'.mp3');     


        for (i = 0; i < audionomor.length; i++) { 
          //  console.log(kata[i])      
            if(i<(audionomor.length-1))
              audionomor[i].next=audionomor[i+1];
        }
        for (i = 0; i < audionomor.length; i++) { 
          //  console.log(kata[i])      
            if(i<(audionomor.length-1))
              audionomor[i].onended=function(){
                this.next.play();
              }
        }


      audio[audio.length-1].next=audionomor[0];

        

      
      }else{
        console.log('kode antrian salah =>'+kata);
        return false;
      }
      

    	
      /*create sound loket*/
      var loketaudio=new Array(loketalias.length);
    	for (i = 0; i < loketalias.length; i++) { 
      	//	console.log(kata[i])
      		loketaudio[i] = new Audio('<?php echo base_url();?>assets/audio/'+loketalias[i]+'.mp3'); 		

    	}
    	for (i = 0; i < loketalias.length; i++) { 
      	//	console.log(kata[i])  		
      		if(i<(loketalias.length-1))
      			loketaudio[i].next=loketaudio[i+1];
    	}
    	for (i = 0; i < loketalias.length; i++) { 
      	//	console.log(kata[i])  		
      		if(i<(loketalias.length-1))
      			loketaudio[i].onended=function(){
      				this.next.play();
      			}
      		
      		
    	}
      loketaudio[loketalias.length-1].onended=function(){
         $.ajax({
            url:"<?php echo base_url();?>fix/end_calling",
           success:function(data)
           {          

           }
          });
      }

      soundx=new Audio('<?php echo base_url();?>assets/audio/soundx.mp3');  
    	nomorantrian=new Audio('<?php echo base_url();?>assets/audio/nomorantrian.mp3'); 	
      soundx.next=nomorantrian;
      soundx.onended=function(){
        
        this.next.play();
      };
      nomorantrian.next=audio[0];
    	nomorantrian.onended=function(){
    		
    		this.next.play();
    	};

    	silahkan=new Audio('<?php echo base_url();?>assets/audio/silahkan.mp3'); 	
    	menuju=new Audio('<?php echo base_url();?>assets/audio/menuju.mp3'); 	
    	loket=new Audio('<?php echo base_url();?>assets/audio/loket.mp3'); 	
    	noloket=new Audio('<?php echo base_url();?>assets/audio/b.mp3'); 	

    	
      

    	audionomor[audionomor.length-1].next=silahkan;
    	silahkan.next=menuju;
    	menuju.next=loket;
    	loket.next=loketaudio[0];

    	

    	audionomor[audionomor.length-1].onended=function(){
        this.next.play();
      };
      audio[audio.length-1].onended=function(){
    		this.next.play();
    	};
    	silahkan.onended=function(){
    		this.next.play();
    	};
    	menuju.onended=function(){
    		this.next.play();
    	};
    	loket.onended=function(){
    		this.next.play();
    	};

      

    	 /*end sound*/
               

    	var promise = soundx.play();

    if (promise !== undefined) {
        promise.then(_ => {
             /*start sound*/
            $.ajax({
                url:"<?php echo base_url();?>fix/start_calling",
               success:function(data)
               {          

               }
            });  
        }).catch(error => {
            // Autoplay was prevented.
            // Show a "Play" button so that user can start playback.
        });
    }

  }else{
    console.log('string error');
  }

	
}

function play(audio){
	audio.play();
}

 

</script>
</body>
</html>