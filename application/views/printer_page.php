<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>receipt</title>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/normalize.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/paper.css">
  <script src="<?php echo base_url();?>assets/js/jquery-3.4.0.min.js"></script>
  <script src="<?php echo base_url();?>/assets/js/moment-with-locales.min.js"></script>
  

  <style>
    /*body {
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
    }*/
    @page { size: 80mm 80mm } /* output size */
    body.receipt .sheet { width: 80mm; height: 80mm } /* sheet size */
    @media print { body.receipt { width: 80mm } } /* fix for Chrome */

    .receipt {
      text-align: center;
    }
      h1{
        margin-bottom: 2mm;
        margin-top: 2mm;
        font-size: 3em !important;  
      }
      
      h2{

      margin: auto 0;
    }
      h3{
        
        margin: auto 0;   
      }
      p{        

        color: #666;     
        margin: 0;
      }
       
   #top{
      border-bottom: 0.2mm solid black;
      
   }   
      #mid{
        margin-top: 1mm;     
      }
     #bot{
       margin-bottom: 3mm;
        border-top: 0.2mm solid black;
        margin-top: 3mm;
      
     }  

    #legalcopy{
          margin-top: 1mm;
        }
    .light { 
          font-weight: lighter;
          margin-bottom: 2mm

        }
    #footer{
        margin-top:5mm;     
        text-align:center;
    }
    p.small{
      font-size: .8em !important;
    }

  </style>
</head>

<body class="receipt">
  <section class="sheet padding-5mm">   
  
    
    <center id="top">
      <div class="logo"></div>
      <div class="info"> 
        <h2><?php echo $setting['nama_instansi'];?></h2>
        <p class="small"><?php echo $setting['alamat_instansi'].' - '.$setting['telp_instansi'];?></p>
      </div><!--End Info-->
    </center><!--End InvoiceTop-->


     <div id="mid">
      <div class="info">     
        <p>Nomor Antrian Anda:</p>
        <b><h1 class="teks-besar"> 
            <?php echo $notiket;?>
        </h1></b>
        
      <p>silahkan menunggu nomor anda dipanggil</p>
      <p>jumlah antrian : <b> <?php echo $sisa;?> </b> </p>
      </div>
    </div><!--End Invoice Mid-->


   <div id="bot">
      <div id="legalcopy">
            <p class="legal"><strong>Terima Kasih</strong>  atas kunjungan Anda. 
            </p>
          </div>

  </div><!--End InvoiceBot-->
  <div id="footer">
    <p class="light waktu"> Senin 01 July 2019, 19:00:00</p>
  </div>
  


  </section>

<script type="text/javascript">
   moment.locale('id');  // Set the default/global locale
  a=moment().format('Do MMMM YYYY, HH:mm:ss');
  $('.waktu').html(a);

    function PrintWindow() {                    
       window.print();            
       CheckWindowState();
    }

    function CheckWindowState()    {           
        if(document.readyState=="complete") {
            window.close(); 
        } else {           
            setTimeout("CheckWindowState()", 2000)
        }
    }
   PrintWindow();
</script> 


</body>
</html>