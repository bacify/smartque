
<div class="row">
	<div class="col-md-12">
		<p class="mb-4 text-gray-800 float-left"><u> <?php echo $title;?></u> ( <a href="<?php echo $url;?>"  class="btn btn-link">Unduh </a>)</p>
		<form class="form-inline float-right" method="GET">
		   <label class="sr-only" for="inputtgl">Tanggal</label>
		  	<input type="text" class="form-control mb-2 mr-sm-2 inputtgl" id="inputtgl" name="tanggal" placeholder="tanggal" autocomplete="off" required readonly="">
       
		  	 <button type="submit" class="btn btn-primary mb-2">Cari</button>
		</form>

	</div>
</div>



	<div class="row">

	<div class="col-md-12">
               <table class="table table-sm table-bordered" id="tabel-laporan">
                    <thead class="table-primary text-center">
                           <th>Bulan</th>
                           <th>Nama Layanan</th>
                           <th>Jumlah Kunjungan</th>
                           <th>Dilayani</th>
                           <th>Tak Dilayani</th>
                           <th>Waktu Tunggu (AVG)</th>
                           <th>Waktu Pelayanan (AVG)</th>
                           <th>Total </th>
                    </thead>				
                    <tbody>
                    	<?php 
                    	foreach ($laporan as $la) {
                        
                        $newDate = date("d-m-Y", strtotime($la->tanggal_pj));
                        echo '<tr><td>'.$newDate.'</td>'.
                             '<td>'.$la->nama_layanan.'</td>'.
                    				 '<td>'.$la->total.'</td>'.
                    				 '<td>'.$la->dilayani.'</td>'.
                    				 '<td>'.$la->tidak_dilayani.'</td>'.
                    				 '<td>'.$la->average_waiting.'</td>'.
                    				 '<td>'.$la->average_service.'</td>'.
                    				 '<td>'.$la->average_total_service.'</td></tr>';
                    	}
                    	?>
                    	
                    </tbody>
               </table>
        </div>
</div>

<script>
    $(document).ready(function () {
    	$('#inputtgl').datepicker({
		    format: "dd/mm/yyyy",
        
		    endDate: "+0d",
		    language: "id",
		    autoclose: true,
		    todayHighlight: true
		});
    });
</script>