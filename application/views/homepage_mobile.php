<!DOCTYPE html>
<html lang="en">
<head>
	<title>SMART QUEUE SYSTEM</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="shortcut icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon">
  <link rel="icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fontawesome/css/all.min.css">
<!--===============================================================================================-->
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/util.css">	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/homepage.css">
<!--===============================================================================================-->
</head>
<body>
	
	
	<div class="size1 bg0 where1-parent">
		<!-- Coutdown -->
	<div class="flex-c-m bg-img1 size2 where1 overlay1 where2 respon2" style="background-image: url('<?php echo base_url();?>assets/img/bg01.jpg');">
		<?php 
			/*show menu on logged*/
		if($this->session->userdata('email')){

			if($setting['queue_status']==0){
				echo '<div class="fixed-top container-fluid">
						<div class="row ">
						<div class="col-md-5 offset-md-7 my-2 badge-warning text-center">Status Antrian Tidak aktif, Silahkan aktifkan melalui menu Konfigurasi</div>
						</div>
						</div>';
				}

			
		?>
			<div class="container-fluid">
			<div class="row">
				<div class="col-md-8 offset-md-2 my-2">
					<div class="badge-info text-center  s2-txt7 my-2 py-1"><p class="text-black-50 text"><u>Halaman</u></p></div>
							    <a  target="_blank" href="<?php echo base_url();?>administrator" class="btn-t text-left btn btn-outline-info btn-block btn-lg btn-wrap-text   btn-transparent text-center py-4"> <h2 class="s2-txt7"><i class="fas  fa-tools m-b-5"></i> Konfigurasi</h2></a>
							  </div>
							  <div class="col-md-8 offset-md-2 my-2">
							    <a  target="_blank" href="<?php echo base_url();?>home/kiosk_display" class="btn-t text-left btn btn-outline-info btn-block btn-lg btn-wrap-text   btn-transparent text-center py-4"> <h2 class="s2-txt7"><i class="fas fa-newspaper m-b-5"></i> Panel Kiosk</h2></a>
							  </div>
							  <div class="col-md-8 offset-md-2 my-2">
							    <a  target="_blank" href="<?php echo base_url();?>home/panel" class="btn-t text-left btn btn-outline-info btn-block btn-lg btn-wrap-text   btn-transparent text-center py-4"> <h2 class="s2-txt7"><i class="fas fa-chalkboard-teacher m-b-5"></i> Panel Loket</h2></a>
							  </div>
							  <div class="col-md-8 offset-md-2 my-2">
							    <a  target="_blank" href="<?php echo base_url();?>home/main_display" class="btn-t text-left btn btn-outline-info btn-block btn-lg btn-wrap-text   btn-transparent text-center py-4"> <h2 class="s2-txt7"><i class="fas fa-tv m-b-5"></i> Panel Display</h2></a>
							  </div>
				<!-- <div class="col-md-8 offset-md-2 my-2">
				<button   id="btn-tiket-1" class="text-left btn btn-outline-info btn-block btn-lg btn-wrap-text s2-txt6 size7 btn-mod-transparent "><i class="fas   fa-hand-point-right"></i> Pendaftaran Pasien BPJS DAN ASURANSI</button>
				</div>
				
				<div class="col-md-8 offset-md-2 my-2">
				<button  id="btn-tiket-2" class="text-left btn btn-outline-info btn-block btn-lg btn-wrap-text s2-txt6 size7 btn-mod-transparent"><i class="fas  fa-hand-point-right"></i> Pendaftaran Pasien Umum</button>
				</div>
				<div class="col-md-8 offset-md-2 my-2">
				<button  id="btn-tiket-3" class="text-left btn btn-outline-info btn-block btn-lg btn-wrap-text s2-txt6 size7 btn-mod-transparent"><i class="fas   fa-hand-point-right"></i> Pendaftaran Pasien Online</button>
				</div>
				<div class="col-md-8 offset-md-2 my-2">
				<button   id="btn-tiket-4" class="text-left btn btn-outline-info btn-block btn-lg btn-wrap-text s2-txt6 size7 btn-mod-transparent"><i class="fas   fa-hand-point-right"></i> lain-lain</button>
				</div>
				 --> 
			</div>
			</div>

		<?php 

		}
		?>
		 
				
						 
		</div>
		
		<!-- Form -->
		<div class="size3 flex-col-sb flex-w p-l-75 p-r-75 p-t-45 p-b-45 respon1">
			<center>
			<div class="wrap-pic1">
				<img src="<?php echo base_url();?>assets/img/sq_logo.png" class="img-rounded" alt="LOGO">
			</div>
		</center>

			<div class="p-t-50 p-b-60">
				<?php 

			/*show menu on logged*/
			if($this->session->userdata('email')){
				?>

				<p class="m1-txt1 p-b-36">
					Selamat Datang di <span class="m1-txt2">Smart Queue System</span>
				</p>
				<a  class="btn btn-outline-info btn-lg" href="<?php echo base_url();?>home/logout"><i class="fas fa-2x fa-sign-out-alt m-b-5"></i> Logout</></a>
				<?php 
					}else{
				?>
					<p class="m1-txt1 p-b-36">
					Selamat Datang di <span class="m1-txt2">Smart Queue System</span>, silahkan login!
				</p>
				<form class="contact100-form validate-form" action="<?php echo base_url();?>home/login" method="POST">				
					<div class="text-warning"><center><h3><?php echo $this->session->flashdata('pesan');?></h3></center></div>
					<div class="wrap-input100 m-b-20 validate-input" data-validate = "Email is required: ex@abc.xyz">
						<input class="s2-txt1 placeholder0 input100" type="email" name="email" placeholder="Alamat Email">
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 m-b-20 validate-input" data-validate = "password is required">
						<input class="s2-txt1 placeholder0 input100" type="password" name="password" placeholder="Password" autocomplete="off">
						<span class="focus-input100"></span>
					</div>

					<div class="w-full">
						<button class="flex-c-m s2-txt2 size4 bg1 bor1 hov1 trans-04" type="submit" value="login" name="login">
							Login
						</button>
					</div>
				</form>
				<?php }
				?>

				<p class="s2-txt3 p-t-18">
					Silahkan Hubungi vendor jika kesulitan.
				</p>
			</div>

			<div class="flex-w pos-relative">
				<a href="#" class="flex-c-m size5 bg3 how1 trans-04 m-r-5">
					<i class="fab fa-facebook"></i>
				</a>

				<a href="#" class="flex-c-m size5 bg4 how1 trans-04 m-r-5">
					<i class="fab fa-twitter"></i>
				</a>

				<a href="#" class="flex-c-m size5 bg6 how1 trans-04 m-r-5">
					<i class="fab fa-instagram"></i>
				</a>
				<p class="text-right right-0 p-r-0 p-t-10 pos-absolute m1-txt5">
					<i class="fas fa-pencil-alt"></i> <span class="m1-txt6 pr-4">WhiteSpace</span>
				</p>
			</div>
		</div>
	</div>


	<div id="print">
	</div>
	

<!--===============================================================================================-->	
	<script src="<?php echo base_url();?>assets/js/jquery-3.4.0.min.js"></script>
<!--===============================================================================================-->
	
	<script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
<!--===============================================================================================-->

<!--===============================================================================================-->
	
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/js/homepage.js"></script>

	<script>
		/* Get the documentElement (<html>) to display the page in fullscreen */
		 

		// Find the right method, call on correct element
			function launchFullScreen(element) {
			  if(element.requestFullScreen) {
			    element.requestFullScreen();
			  } else if(element.mozRequestFullScreen) {
			    element.mozRequestFullScreen();
			  } else if(element.webkitRequestFullScreen) {
			    element.webkitRequestFullScreen();
			  }
			}

			// Launch fullscreen for browsers that support it!
			//launchFullScreen(document.documentElement); // the whole page


			if((window.fullScreen) ||
			 	  (window.innerWidth == screen.width && window.innerHeight == screen.height)) {

			} else {
				/*var
				          el = document.documentElement
				        , rfs =
				               el.requestFullScreen
				            || el.webkitRequestFullScreen
				            || el.mozRequestFullScreen
				    ;
				    rfs.call(el);*/
				    $('#element').popover('show')
			}
		</script>


		<script type="text/javascript">
			
			
			// $('.btn-t').click(function() {
			// 	  layanan=$(this).val();
			// 	//  console.log(layanan);
			// 	  button=$(this);
			// 	  button.attr("disabled", true);
			// 	$.ajax({
			// 	   	url:"<?php echo base_url();?>home/get_current_tiket/"+layanan,
			// 		success:function(data)
			// 		{
			// 			r1=JSON.parse(data);
			// 			//cetak tiket 
			// 			console.log(r1.tiket.current_tiket);
			// 			//update tiket next
			// 			/*$.ajax({
			// 			   	url:"<?php echo base_url();?>home/cetak_tiket/",
			// 				success:function(dat)
			// 				{
								
			// 				}
			// 		 	})*/
			// 			var win = window.open('<?php echo base_url();?>home/cetak_tiket/'+r1.tiket.current_tiket+'/'+r1.sisa, '_blank');
			// 				if (win) {
			// 				    //Browser has allowed it to be opened
			// 				    win.focus();
			// 				} else {
			// 				    //Browser has blocked it
			// 				    alert('Please allow popups for this website');
			// 				}

			// 			$.ajax({
			// 			   	url:"<?php echo base_url();?>home/next_tiket/"+layanan,
			// 				success:function(data2)
			// 				{
			// 					r2=JSON.parse(data2);
			// 					//cetak tiket 
			// 					console.log(r2);
			// 					//update tiket next
			// 					 button.attr("disabled", false);
			// 				}
			// 		 	})
			// 		}
			// 	 })
			// });
		</script>

</body>
</html>