<h1 class="page-header mb-4 text-gray-800"> Pengaturan Layanan</h2>

	<div class="row">
		<div class="col-lg-6 col-sm-12">
			<form action="#" method="POST">
			<table class="table ">
				<thead><tr>
							<th> No</th>
							<th> Nama Layanan </th>
							<th> Status</th>
						</tr>
				</thead>
				<tbody>
					<?php foreach($layanan as $lo){
						echo '<tr><td>'.$lo->id_layanan.'</td>'.
								'<td><input class="form-control mb-2 mr-sm-2" name="nama-'.$lo->id_layanan.'" type="text" value="'.$lo->nama_layanan.'"></td>';								
								
								if($lo->status_layanan==1)
								echo '<td><input id="status-'.$lo->id_layanan.'" class="form-control" name="status-'.$lo->id_layanan.'" type="checkbox" value="1" data-toggle="toggle" data-onstyle="primary" data-width="50" checked></td>';
								else
								echo '<td><input id="status-'.$lo->id_layanan.'" class="form-control" name="status-'.$lo->id_layanan.'" type="checkbox" value="1" data-toggle="toggle" data-onstyle="primary" data-width="50"></td>';

						echo '</tr>';
					}
					?>
					
				</tbody>
			</table>
			
			<button class="btn btn-success float-right" name="submit" type="submit" value="save">Simpan</button>
			</form>
		</div>
	</div>
