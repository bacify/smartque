 <?php 
 date_default_timezone_set('Asia/Jakarta');

 ?>
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 	<link rel="shortcut icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon">
   <link rel="icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon">
 

	<!-- <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/trumbowyg.min.css"/>
	-->
 	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/jquery-3.4.0.min.js"></script>
 	 


</head>
<body>

	 <?php 
          

            echo $header; ?>
		
				<?php
				  /*
				   * Variabel $contentnya diambil dari libraries template.php
				   * (application/libraries/template.php)
				   * */
				  echo $content; ?>
			
	
		<?php 
		  /*
		   * Variabel $footernya diambil dari libraries template.php
		   * (application/libraries/template.php)
		   * */
		  echo $footer; ?>
		







</body>
</html>