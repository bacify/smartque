<table >
	<tr><td>No antrian</td><td> 01</td></tr>
	<tr><td>R1</td><td id="c1"> 00</td></tr>
	<tr><td>R2</td><td id="c2"> 00</td></tr>
	<tr><td>R3</td><td id="c3"> 00</td></tr>
	<tr><td>R4</td><td id="c4"> 01</td></tr>
</table>



<script type="text/javascript">
	/**
 * AJAX long-polling
 *
 * 1. sends a request to the server (without a timestamp parameter)
 * 2. waits for an answer from server.php (which can take forever)
 * 3. if server.php responds (whenever), put data_from_file into #response
 * 4. and call the function again
 *
 * @param timestamp
 */
function getContent(timestamp)
{
    var queryString = {'timestamp' : timestamp};

    $.ajax(
        {
            type: 'GET',
            url: '<?php echo base_url();?>home/pulldata',
            data: queryString,
            success: function(data){
                // put result data into "obj"
                var obj = jQuery.parseJSON(data);
                // put the data_from_file into #response
               /* $('#response').html(obj.data_from_file);*/
                // call the function again, this time with the timestamp we just got from server.php
                $('#c1').html(obj.c1);
                $('#c2').html(obj.c2);
                $('#c3').html(obj.c3);
                $('#c4').html(obj.c4);
                getContent(obj.timestamp);
            }
        }
    );
}

// initialize jQuery
$(function() {
    getContent();
});

</script>