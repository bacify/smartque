<div class="file-loading">
    <input id="inputfile" name="inputfile" type="file" multiple accept="image">
</div>
<script>
$("#inputfile").fileinput({
    theme: "explorer-fas",
    uploadUrl: "<?php echo base_url();?>administrator/fileupload",
    allowedFileExtensions: ['jpg', 'png', 'gif','mp4',],
    overwriteInitial: false,
    initialPreviewAsData: true,
    initialPreview: [
        <?php 
        foreach($media as $m){
            echo '"'.base_url().'assets/uploads/'.$m->filename.'",';
        }
        ?>
        
    ],
    initialPreviewConfig: [
        <?php 
        foreach($media as $m){
            if($m->ext=='.mp4'){
                echo '{type: "video", caption: "'.$m->filename.'", filetype: "'.$m->file_type.'", size: '.$m->size.', downloadUrl : "'.base_url().'assets/uploads/'.$m->filename.'", url : "'.base_url().'administrator/deletefile/'.$m->id_media.'"},';
            }else
            echo '{caption: "'.$m->filename.'", filetype: "'.$m->file_type.'", size: '.$m->size.', downloadUrl : "'.base_url().'assets/uploads/'.$m->filename.'", url : "'.base_url().'administrator/deletefile/'.$m->id_media.'"},';
        }
        ?>
        
    ],
    //initialPreviewDownloadUrl: 'https://picsum.photos/1920/1080?image={key}' // the key will be dynamically replaced 
    //,
    preferIconicPreview: false, // this will force thumbnails to display icons for following file extensions
         previewFileIconSettings: { // configure your icon file extensions
        'doc': '<i class="fas fa-file-word text-primary"></i>',
        'xls': '<i class="fas fa-file-excel text-success"></i>',
        'ppt': '<i class="fas fa-file-powerpoint text-danger"></i>',
        'pdf': '<i class="fas fa-file-pdf text-danger"></i>',
        'zip': '<i class="fas fa-file-archive text-muted"></i>',
        'htm': '<i class="fas fa-file-code text-info"></i>',
        'txt': '<i class="fas fa-file-text text-info"></i>',
        'mov': '<i class="fas fa-file-video text-warning"></i>',
        'mp3': '<i class="fas fa-file-audio text-warning"></i>',
        // note for these file types below no extension determination logic 
        // has been configured (the keys itself will be used as extensions)
          
    },
    previewFileExtSettings: { // configure the logic for determining icon file extensions
        'doc': function(ext) {
            return ext.match(/(doc|docx)$/i);
        },
        'xls': function(ext) {
            return ext.match(/(xls|xlsx)$/i);
        },
        'ppt': function(ext) {
            return ext.match(/(ppt|pptx)$/i);
        },
        'zip': function(ext) {
            return ext.match(/(zip|rar|tar|gzip|gz|7z)$/i);
        },
        'htm': function(ext) {
            return ext.match(/(htm|html)$/i);
        },
        'txt': function(ext) {
            return ext.match(/(txt|ini|csv|java|php|js|css)$/i);
        },
        'mov': function(ext) {
            return ext.match(/(avi|mpg|mkv|mov|mp4|3gp|webm|wmv)$/i);
        },
        'mp3': function(ext) {
            return ext.match(/(mp3|wav)$/i);
        }
    }
});
</script>