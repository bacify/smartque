<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maincore{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct()
	{
	    $this->CI =& get_instance();
	    $this->CI->load->model('m_smartq');
	}
	public function index()
	{
		$this->load->view('Direct Class Not Allowed');
	}

	/*status login*/
	public function getloginstatus(){

	}
	/*Buatstatus login*/
	public function createloginstatus(){

	}
	/*Buatstatus login*/
	public function updateloginstatus(){

	}

	/*informasi Instansi*/
	public function getinformation(){		


	}
	/*informasi Instansi*/
	public function updateinformation(){		


	}

	/*informasi pengumuman (running text)*/
	public function getbanner(){

	}
	public function createbanner(){

	}
	public function updatebanner(){

	}
	public function deletebanner(){

	}

	/*informasi antrian pada tiket*/
	public function getticketqueue(){

	}
	public function updateticketqueue(){

	}
	public function resetticketqueue(){

	}

	/*informasi antrian yang sedang berjalan*/
	public function getcurrentqueue(){

	}
	public function updatecurrentqueue(){

	}

	/*informasi antrian yang sedang dilayani*/
	public function getclientqueue(){

	}
	public function updateclientqueue(){

	}

	/*informasi receptionnis*/
	public function getreceptionist(){

	}
	public function createreceptionist(){

	}
	public function updatereceptionist(){

	}
	public function deletereceptionist(){

	}

	/*	informasi gambar*/
	public function getimage(){

	}
	public function updateimage(){

	}
	public function deleteimage(){

	}
	public function addimage(){

	}

	/*pengaturan program*/
	public function getsetting(){

	}
	public function updatesetting(){

	}

	/*informasi statistik harian*/
	public function getstat(){

	}
	public function insertstat(){

	}

	/*informasi API*/
	public function getapi(){

	}
	public function createapi(){

	}
	public function updateapi(){

	}
	public function deleteapi(){

	}
}


   
