-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 17 Agu 2019 pada 11.02
-- Versi server: 10.3.16-MariaDB
-- Versi PHP: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smartq`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `activity_counter`
--

CREATE TABLE `activity_counter` (
  `id_activity` int(255) NOT NULL,
  `no_tiket` int(255) DEFAULT NULL,
  `resepsionis_id` int(11) DEFAULT NULL,
  `time_of_activity` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `login_details`
--

CREATE TABLE `login_details` (
  `login_detail_id` int(11) NOT NULL,
  `loket_id` int(11) NOT NULL,
  `last_activity` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_type` int(255) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `login_details`
--

INSERT INTO `login_details` (`login_detail_id`, `loket_id`, `last_activity`, `is_type`) VALUES
(34, 1, '2019-08-03 09:18:07', 0),
(35, 1, '2019-08-09 07:35:25', 0),
(36, 1, '2019-08-10 05:56:57', 0),
(37, 2, '2019-08-10 06:39:57', 0),
(38, 1, '2019-08-11 18:32:38', 0),
(39, 1, '2019-08-12 11:06:24', 0),
(40, 1, '2019-08-12 12:30:02', 0),
(41, 1, '2019-08-12 19:16:14', 0),
(42, 1, '2019-08-16 18:23:33', 0),
(43, 1, '2019-08-17 04:02:25', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `smart_setting`
--

CREATE TABLE `smart_setting` (
  `id` int(11) NOT NULL,
  `setting` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `smart_setting`
--

INSERT INTO `smart_setting` (`id`, `setting`, `value`) VALUES
(1, 'queue_status', '1'),
(2, 'queue_mode', '1'),
(3, 'nama_instansi', 'RSUD RA. BASOENI     '),
(4, 'alamat_instansi', 'Jalan Raya Gedeg No. 17 Mojokerto, Jawa Timur 61351'),
(5, 'telp_instansi', '(0321) 364752 '),
(6, 'jenis_display', '2'),
(7, 'display_list', '0'),
(8, 'slideshow', '35,36'),
(9, 'video', '34'),
(10, 'key_activation', 'WhAJzKDrgtHm6'),
(11, 'email', 'admin@whitespace.com'),
(12, 'password', 'ac43724f16e9241d990427ab7c8f4228');

-- --------------------------------------------------------

--
-- Struktur dari tabel `s_call_status`
--

CREATE TABLE `s_call_status` (
  `id_call` int(255) NOT NULL,
  `loket_id` int(11) DEFAULT NULL,
  `counter` varchar(11) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` int(255) DEFAULT 0,
  `calling` int(255) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `s_call_status`
--

INSERT INTO `s_call_status` (`id_call`, `loket_id`, `counter`, `updated`, `status`, `calling`) VALUES
(1, 1, 'A30', '2019-08-17 03:55:29', 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `s_layanan`
--

CREATE TABLE `s_layanan` (
  `id_layanan` int(255) NOT NULL,
  `nama_layanan` varchar(255) DEFAULT NULL,
  `tiket_alias` varchar(255) DEFAULT NULL,
  `status_layanan` int(255) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `s_layanan`
--

INSERT INTO `s_layanan` (`id_layanan`, `nama_layanan`, `tiket_alias`, `status_layanan`) VALUES
(1, 'layanan1', 'A', 1),
(2, 'layanan2', 'B', 1),
(3, 'layanan3', 'C', 0),
(4, 'layanan4', 'D', 0),
(5, 'layanan5', 'E', 0),
(6, 'layanan6', 'F', 0),
(7, 'layanan7', 'G', 0),
(8, 'layanan8', 'H', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `s_layanan_counter`
--

CREATE TABLE `s_layanan_counter` (
  `id_counter` int(255) NOT NULL,
  `layanan_id` int(255) DEFAULT NULL,
  `counter` int(255) DEFAULT 1,
  `updated` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `s_layanan_counter`
--

INSERT INTO `s_layanan_counter` (`id_counter`, `layanan_id`, `counter`, `updated`) VALUES
(1, 1, 622, '2019-08-16 18:23:10'),
(2, 2, 1, '2019-08-11 17:36:51'),
(3, 3, 1, '2019-08-03 08:43:53'),
(4, 4, 1, '2019-08-03 08:43:53'),
(5, 5, 1, '2019-08-03 08:43:53'),
(6, 6, 1, '2019-08-03 08:43:53'),
(7, 7, 1, '2019-08-03 08:43:53'),
(8, 8, 1, '2019-08-03 08:43:53');

-- --------------------------------------------------------

--
-- Struktur dari tabel `s_loket_alias`
--

CREATE TABLE `s_loket_alias` (
  `id_alias` int(255) NOT NULL,
  `loket_id` int(255) DEFAULT NULL,
  `alias_name` varchar(255) DEFAULT NULL,
  `status` int(255) DEFAULT 0,
  `layanan_id` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `s_loket_alias`
--

INSERT INTO `s_loket_alias` (`id_alias`, `loket_id`, `alias_name`, `status`, `layanan_id`) VALUES
(1, 1, '1', 1, 1),
(2, 2, '2', 1, 2),
(3, 3, '3', 0, 1),
(4, 4, '4', 0, 1),
(5, 5, '5', 0, 1),
(6, 6, '6', 0, 1),
(7, 7, '7', 0, 1),
(8, 8, '8', 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `s_loket_status`
--

CREATE TABLE `s_loket_status` (
  `id_counter` int(255) NOT NULL,
  `loket_id` int(255) DEFAULT NULL,
  `counter` varchar(255) DEFAULT '1',
  `updated` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `s_loket_status`
--

INSERT INTO `s_loket_status` (`id_counter`, `loket_id`, `counter`, `updated`) VALUES
(1, 1, 'A31', '2019-08-17 03:36:39'),
(2, 2, '-', '2019-07-30 17:31:06'),
(3, 3, '-', '2019-07-30 17:31:06'),
(4, 4, '-', '2019-07-13 07:24:20'),
(5, 5, '-', '2019-07-13 07:24:20'),
(6, 6, '-', '2019-07-13 07:24:20'),
(7, 7, '-', '2019-07-13 07:24:20'),
(8, 8, '-', '2019-07-13 07:24:20');

-- --------------------------------------------------------

--
-- Struktur dari tabel `s_media`
--

CREATE TABLE `s_media` (
  `id_media` int(255) NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `size` double DEFAULT NULL,
  `width` double(255,0) DEFAULT NULL,
  `ext` varchar(255) DEFAULT NULL,
  `file_type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `s_media`
--

INSERT INTO `s_media` (`id_media`, `filename`, `size`, `width`, `ext`, `file_type`) VALUES
(34, 'Ini_Cerita_Aneh_di_Waktu_Malam_Tentang_Lagu_Lama!_-_Kartun_Lucu.mp4', 4918.21, NULL, '.mp4', 'video/mp4'),
(35, '1__2_.jpg', 147.05, 1024, '.jpg', 'image/jpeg'),
(36, '2__2_.jpg', 100.2, 1024, '.jpg', 'image/jpeg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `s_tiket_counter`
--

CREATE TABLE `s_tiket_counter` (
  `id_counter` int(255) NOT NULL,
  `layanan_id` int(255) DEFAULT NULL,
  `counter` int(255) DEFAULT 0,
  `updated` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `s_tiket_counter`
--

INSERT INTO `s_tiket_counter` (`id_counter`, `layanan_id`, `counter`, `updated`) VALUES
(1, 1, 0, '2019-08-02 14:02:02'),
(2, 2, 0, '2019-08-02 14:02:02'),
(3, 3, 0, '2019-08-02 14:02:02'),
(4, 4, 0, '2019-08-02 14:02:02'),
(5, 5, 0, '2019-08-02 14:02:02'),
(6, 6, 0, '2019-08-02 14:02:02'),
(7, 7, 0, '2019-08-02 14:02:02'),
(8, 8, 0, '2019-08-02 14:02:02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tiket_queue`
--

CREATE TABLE `tiket_queue` (
  `idtiket` int(255) NOT NULL,
  `no_tiket` varchar(255) DEFAULT NULL,
  `layanan_id` int(255) DEFAULT NULL,
  `queue_start` timestamp NULL DEFAULT current_timestamp(),
  `queue_end` timestamp NULL DEFAULT NULL,
  `service_start` timestamp NULL DEFAULT NULL,
  `service_end` timestamp NULL DEFAULT NULL,
  `queue_status` int(255) DEFAULT 0,
  `updated` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `tiket_queue`
--

INSERT INTO `tiket_queue` (`idtiket`, `no_tiket`, `layanan_id`, `queue_start`, `queue_end`, `service_start`, `service_end`, `queue_status`, `updated`) VALUES
(69391, 'A1', 1, '2019-08-08 20:36:51', NULL, NULL, NULL, 4, '2019-08-10 05:40:37'),
(69392, 'A1', 1, '2019-08-10 05:40:42', NULL, NULL, NULL, 4, '2019-08-11 17:36:51'),
(69393, 'A2', 1, '2019-08-10 05:40:46', NULL, NULL, NULL, 4, '2019-08-11 17:36:50'),
(69394, 'A3', 1, '2019-08-10 05:41:06', NULL, NULL, NULL, 4, '2019-08-11 17:36:50'),
(69395, 'B1', 2, '2019-08-10 05:57:43', NULL, NULL, NULL, 4, '2019-08-11 17:36:50'),
(69396, 'B2', 2, '2019-08-10 05:57:46', NULL, NULL, NULL, 4, '2019-08-11 17:36:50'),
(69397, 'B3', 2, '2019-08-10 05:57:49', NULL, NULL, NULL, 4, '2019-08-11 17:36:50'),
(69398, 'A1', 1, '2019-08-11 17:58:43', NULL, NULL, NULL, 4, '2019-08-12 17:31:42'),
(69399, 'A2', 1, '2019-08-11 17:58:46', NULL, NULL, NULL, 4, '2019-08-12 17:31:42'),
(69400, 'A3', 1, '2019-08-11 17:58:50', NULL, NULL, NULL, 4, '2019-08-12 17:31:42'),
(69401, 'A4', 1, '2019-08-12 11:51:01', NULL, NULL, NULL, 4, '2019-08-12 17:31:42'),
(69402, 'A5', 1, '2019-08-12 11:51:05', NULL, NULL, NULL, 4, '2019-08-12 17:31:42'),
(69403, 'A1', 1, '2019-08-12 19:00:51', NULL, NULL, NULL, 4, '2019-08-14 06:17:47'),
(69404, 'A2', 1, '2019-08-12 19:00:56', NULL, NULL, NULL, 4, '2019-08-14 06:17:46'),
(69405, 'A3', 1, '2019-08-12 19:00:59', NULL, NULL, NULL, 4, '2019-08-14 06:17:46'),
(69406, 'A4', 1, '2019-08-12 19:01:02', NULL, NULL, NULL, 4, '2019-08-14 06:17:46'),
(69407, 'A1', 1, '2019-08-16 18:19:15', NULL, NULL, NULL, 3, '2019-08-16 18:37:17'),
(69408, 'A2', 1, '2019-08-16 18:19:19', NULL, NULL, NULL, 3, '2019-08-16 18:37:31'),
(69409, 'A3', 1, '2019-08-16 18:19:22', NULL, NULL, NULL, 3, '2019-08-16 18:37:46'),
(69410, 'A4', 1, '2019-08-16 18:19:26', NULL, NULL, NULL, 3, '2019-08-16 18:38:00'),
(69411, 'A10', 1, '2019-08-16 18:20:32', '2019-08-16 18:38:14', '2019-08-16 18:38:14', '2019-08-16 18:38:19', 2, '2019-08-16 18:38:19'),
(69412, 'A11', 1, '2019-08-16 18:20:36', NULL, NULL, NULL, 3, '2019-08-16 18:38:33'),
(69413, 'A12', 1, '2019-08-16 18:20:49', NULL, NULL, NULL, 3, '2019-08-16 18:42:30'),
(69414, 'A20', 1, '2019-08-16 18:21:00', NULL, NULL, NULL, 3, '2019-08-17 03:34:38'),
(69415, 'A21', 1, '2019-08-16 18:21:04', NULL, NULL, NULL, 3, '2019-08-17 03:35:57'),
(69416, 'A30', 1, '2019-08-16 18:21:14', NULL, NULL, NULL, 3, '2019-08-17 03:36:38'),
(69417, 'A31', 1, '2019-08-16 18:21:17', NULL, NULL, NULL, 0, '2019-08-16 18:21:17'),
(69418, 'A100', 1, '2019-08-16 18:21:27', NULL, NULL, NULL, 0, '2019-08-16 18:21:27'),
(69419, 'A101', 1, '2019-08-16 18:21:31', NULL, NULL, NULL, 0, '2019-08-16 18:21:31'),
(69420, 'A102', 1, '2019-08-16 18:21:34', NULL, NULL, NULL, 0, '2019-08-16 18:21:34'),
(69421, 'A110', 1, '2019-08-16 18:21:45', NULL, NULL, NULL, 0, '2019-08-16 18:21:45'),
(69422, 'A111', 1, '2019-08-16 18:21:49', NULL, NULL, NULL, 0, '2019-08-16 18:21:49'),
(69423, 'A112', 1, '2019-08-16 18:21:52', NULL, NULL, NULL, 0, '2019-08-16 18:21:52'),
(69424, 'A120', 1, '2019-08-16 18:22:05', NULL, NULL, NULL, 0, '2019-08-16 18:22:05'),
(69425, 'A121', 1, '2019-08-16 18:22:08', NULL, NULL, NULL, 0, '2019-08-16 18:22:08'),
(69426, 'A122', 1, '2019-08-16 18:22:12', NULL, NULL, NULL, 0, '2019-08-16 18:22:12'),
(69427, 'A160', 1, '2019-08-16 18:22:22', NULL, NULL, NULL, 0, '2019-08-16 18:22:22'),
(69428, 'A161', 1, '2019-08-16 18:22:25', NULL, NULL, NULL, 0, '2019-08-16 18:22:25'),
(69429, 'A200', 1, '2019-08-16 18:22:32', NULL, NULL, NULL, 0, '2019-08-16 18:22:32'),
(69430, 'A600', 1, '2019-08-16 18:22:40', NULL, NULL, NULL, 0, '2019-08-16 18:22:40'),
(69431, 'A601', 1, '2019-08-16 18:22:43', NULL, NULL, NULL, 0, '2019-08-16 18:22:43'),
(69432, 'A610', 1, '2019-08-16 18:22:52', NULL, NULL, NULL, 0, '2019-08-16 18:22:52'),
(69433, 'A611', 1, '2019-08-16 18:22:55', NULL, NULL, NULL, 0, '2019-08-16 18:22:55'),
(69434, 'A612', 1, '2019-08-16 18:22:58', NULL, NULL, NULL, 0, '2019-08-16 18:22:58'),
(69435, 'A620', 1, '2019-08-16 18:23:07', NULL, NULL, NULL, 0, '2019-08-16 18:23:07'),
(69436, 'A621', 1, '2019-08-16 18:23:10', NULL, NULL, NULL, 0, '2019-08-16 18:23:10');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `activity_counter`
--
ALTER TABLE `activity_counter`
  ADD PRIMARY KEY (`id_activity`) USING BTREE;

--
-- Indeks untuk tabel `login_details`
--
ALTER TABLE `login_details`
  ADD PRIMARY KEY (`login_detail_id`) USING BTREE;

--
-- Indeks untuk tabel `smart_setting`
--
ALTER TABLE `smart_setting`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `s_call_status`
--
ALTER TABLE `s_call_status`
  ADD PRIMARY KEY (`id_call`) USING BTREE;

--
-- Indeks untuk tabel `s_layanan`
--
ALTER TABLE `s_layanan`
  ADD PRIMARY KEY (`id_layanan`) USING BTREE;

--
-- Indeks untuk tabel `s_layanan_counter`
--
ALTER TABLE `s_layanan_counter`
  ADD PRIMARY KEY (`id_counter`) USING BTREE;

--
-- Indeks untuk tabel `s_loket_alias`
--
ALTER TABLE `s_loket_alias`
  ADD PRIMARY KEY (`id_alias`) USING BTREE;

--
-- Indeks untuk tabel `s_loket_status`
--
ALTER TABLE `s_loket_status`
  ADD PRIMARY KEY (`id_counter`) USING BTREE;

--
-- Indeks untuk tabel `s_media`
--
ALTER TABLE `s_media`
  ADD PRIMARY KEY (`id_media`) USING BTREE;

--
-- Indeks untuk tabel `s_tiket_counter`
--
ALTER TABLE `s_tiket_counter`
  ADD PRIMARY KEY (`id_counter`) USING BTREE;

--
-- Indeks untuk tabel `tiket_queue`
--
ALTER TABLE `tiket_queue`
  ADD PRIMARY KEY (`idtiket`) USING BTREE;

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `activity_counter`
--
ALTER TABLE `activity_counter`
  MODIFY `id_activity` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `login_details`
--
ALTER TABLE `login_details`
  MODIFY `login_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT untuk tabel `smart_setting`
--
ALTER TABLE `smart_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `s_call_status`
--
ALTER TABLE `s_call_status`
  MODIFY `id_call` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `s_layanan`
--
ALTER TABLE `s_layanan`
  MODIFY `id_layanan` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `s_layanan_counter`
--
ALTER TABLE `s_layanan_counter`
  MODIFY `id_counter` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `s_loket_alias`
--
ALTER TABLE `s_loket_alias`
  MODIFY `id_alias` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `s_loket_status`
--
ALTER TABLE `s_loket_status`
  MODIFY `id_counter` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `s_media`
--
ALTER TABLE `s_media`
  MODIFY `id_media` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT untuk tabel `s_tiket_counter`
--
ALTER TABLE `s_tiket_counter`
  MODIFY `id_counter` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `tiket_queue`
--
ALTER TABLE `tiket_queue`
  MODIFY `idtiket` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69437;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
